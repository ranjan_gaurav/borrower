jQuery(function () {
			jQuery("#user_records").dataTable();
			jQuery('#user_records_list').dataTable({
			  "bPaginate": true,
			  "bLengthChange": false,
			  "bFilter": true,
			  "bSort": true,
			  "bInfo": false,
			  "bAutoWidth": true
			});
			jQuery('#customer_records_list').dataTable({
			  "bPaginate": false,
			  "bLengthChange": false,
			  "bFilter": false,
			  "bSort": true,
			  "bInfo": false,
			  "bAutoWidth": true
			});
		   });

jQuery("#username").keyup(function (e) { //user types username on inputfiled
   var username = $(this).val();
   var userid = $("#id" ).val();
   if(username.length >=6)
   {
    //get the string typed by user
   $.post('includes/check_username.php', {'username':username,'userid':userid},
   function(data) 
   { 
  //alert(data);
  if(data=="User name already exists.")
  {
     document.getElementById('username').value='';
	 $("#user-result").html(data);
  }else {
	  $("#user-result").html('User name is available.');
	  }
    //dump the data received from PHP page
   
   });
   }
   
});

function check_username()
{
	var username = $("#username" ).val();
	var userid = $("#id" ).val();
	
	//alert(userid);
   if(username.length >=6)
   {
    //get the string typed by user
   $.post('includes/check_username.php', {'username':username,'userid':userid}, 
   function(data) 
   { 
  //alert(data);
  if(data=="User name already exists.")
  {
     document.getElementById('username').value='';
	 alert('User name already exists..');
  } else {
     alert('User name is available.');
  }
  // $("#user-result").html(data); //dump the data received from PHP page
   
   });
   } else {
    alert('Please enter a 6 digits user id.');
   }
}

jQuery(document).ready(function(){
								
	jQuery.validator.addMethod("onlyInteger", function(value, element) { 
        return this.optional(element) || /^([0-9]{1,15})$/i.test(value);
    }, "Value should be integer and not more than 15 digits"); 	
	
	jQuery.validator.addMethod("onlyIntegerZip", function(value, element) { 
        return this.optional(element) || /^([0-9]{1,6})$/i.test(value);
    }, "Value should be integer and not more than 6 digits"); 	
								
								
	    jQuery("#personal_details_processor").validate({
			rules: {     
				first_name: {required: true},
				last_name: {required: true},
				phone: {number:true, onlyInteger:true},
				email: {email: true},
				username: {required: true},
				accepted: {required: true},
				password_new: {
					minlength: 5
				},
				password_new_re: {
					minlength: 5,
					equalTo: "#password_new"
				}
			},
			messages: {
				first_name: {
                required:"Please Enter First Name.",
                },
				last_name: {
                required:"Please Enter Last Name.",
                },
				email: {
                email:"Please Enter Valid Email Id."
                },
				username: {
				required: "Please Enter User Name."
				},
				accepted: {
				required: "Please accept terms & conditions."
				},
				password_new: {
					minlength: "Your password must be at least 5 characters long"
				},
				password_new_re: {
					minlength: "Your password must be at least 5 characters long",
					equalTo: "Please enter the same password as above"
				}
			},
			tooltip_options: {
				first_name: {placement:'bottom',html:true},
				last_name: {placement:'bottom',html:true},
				phone: {placement:'bottom',html:true},
				email: {placement:'bottom',html:true},
				username: {placement:'bottom',html:true},
				accepted: {placement:'bottom',html:true},
				password_new: {placement:'bottom',html:true},
				password_new_re: {placement:'bottom',html:true}
			}
		});
	
	    jQuery("#add_loan_application").validate({
			rules: {     
				borrower_name: {required: true},
				ammount: {required: true, number:true}
			},
			messages: {
				borrower_name: {
                required:"Please Enter Borrower Name.",
                },
				ammount: {
					required: "Please Enter Ammount Required."
				}
			},
			tooltip_options: {
				borrower_name: {placement:'bottom',html:true},
				ammount: {placement:'bottom',html:true}
			}
		});
	
		jQuery("#import_csv").validate({
		rules: {
			file_csv: {
				required: true, 
			}
		},
		messages: {
			file_csv: {
				required: 'Please upload a csv file!' // <-- 'accept' rule not used
			}
		},
		tooltip_options: {
		file_csv: {placement:'bottom',html:true}
		}
	});
		
		jQuery("#contact_us").validate({
			rules: {     
				name: {required: true},
				subject: {required: true},
				email: {required: true, email: true},
				message: {required: true}
			},
			messages: {
				name: {
                required:"Please Enter Name.",
                },
				subject: {
                required:"Please Enter Subject.",
                },
				message: {
					required: "Please Enter Message."
				},
				email: {
                required: "Please Enter Email Id",
                email:"Please Enter Valid Email Id."
                }
			},
			tooltip_options: {
				name: {placement:'bottom',html:true},
				subject: {placement:'bottom',html:true},
				message: {placement:'bottom',html:true},
				email: {placement:'bottom',html:true}
			}
		});
		
		jQuery("#add_property").validate({
			rules: {     
				address: {required: true},
				state: {required: true},
				zip: {required: true, number:true, onlyIntegerZip:true}
			},
			messages: {
				address: {
                required:"Please enter street address",
                },
				state: {
                required:"Please select state",
                },
				zip: {
                required:"Please enter valid zip code"
                }
			},
			tooltip_options: {
				address: {placement:'bottom',html:true},
				state: {placement:'bottom',html:true},
				phone: {placement:'bottom',html:true}
			}
		});
	
	});