var isChecked = 0;
var isCheckedCorrect_information = 0;
var isCheckedTrack_loan = 0;
//var isFirstTimeUserLogin = true;

var isCheckedClicked = 0;
var isCheckedCorrect_information_clicked = 0;
var isCheckedTrack_loan_clicked = 0;
var isFirstTimeUser = 0;

//ajax notification_stting
$(document).on('click', "#notification_stting", function(e){
	
	//alert(123);
	e.preventDefault();
	$(".fa-spinner").show();
	
	//send data by ajax
	
	$.ajax({
      type: "POST",
      url: base_url+'Api/update_notification',
      data:$('#notification_stting_form').serializeArray(),
      beforeSend : function(){
		  $('#loader').show();
	  },
      success: function(data) {
		$(".fa-spinner").hide();
		 //alert(data['temp']);
		if(data['temp'] == true){
			window.location.href = base_url+'admin/users/dashboard'; 
		} else{
			
			location.reload(true);
			//alert(23333);
			//window.location.href = base_url+'admin/users/notification_setting'; 
		}
		 $('#loader').hide();
      },
      error: function(){
    	  $('#loader').hide();
      },
      complete: function(){
    	  $('#loader').hide();
      }
    });
});
/* 
showUser = function () {
	    var FirstName = $('#firstname').val();
	    var LastName = $('#lastname').val();
	    var contact = $('#contact').val();
		var email = $('#email').val();
		var username = $('#username').val();
		var password = $('#password').val();
		var password_conf = $('#password_conf').val();
		var track_loan = $('#track_loan').val();
		var correct_information = $('#correct_information').val();
		var accepted = $('#accepted').val();
		 
		 var  url=  base_url+'Api/update_profile';
		 var datastring = 'FirstName='+FirstName+'&LastName'+LastName;
		//alert(datastring);
		//alert(skills);
		$.ajax({
	        type: 'POST',
	        url:url,
	        data: {
	            FirstName: FirstName,
	            LastName: LastName,
	            contact: contact,
				email:email,
				username:username,
				password:password,
				password_conf:password_conf,
				track_loan:track_loan,
				correct_information:correct_information,
				accepted:accepted
				
	        },
			
	        success: function (res) {
				window.location.href = base_url+'admin/users/notification_setting';
		 //  console.log(res);return false;
	           $("#main").html(res);
				
			 //alert(value);
				
	        },
	        dataType: 'html'
	    });
		
		
	} */

//ajax profile update12242342342342342
 $(document).on('click', "#update_profile", function(e){
	 //console.log( $('#accepted[type="checkbox"]').prop("checked") );
	e.preventDefault();
	//alert($("#personal_details_processor").valid())
	
	if($("#personal_details_processor").valid()){
		
		var userData = {};
		userData.firstname = $.trim($("#firstname").val());
		userData.lastname = $.trim($("#lastname").val());
		userData.contact = $.trim($("#contact").val());
		userData.email = $.trim($("#email").val());
		userData.username = $.trim($("#username").val());
		userData.password = $.trim($("#password").val());
		userData.password_conf = $.trim($("#password_conf").val());
				
		if(isCheckedClicked==1){
				userData.isTermsAccepted = isChecked;
		}else{
			if($('#accepted[type="checkbox"]').prop("checked")==false){
					userData.isTermsAccepted = 0;
			}else{
					userData.isTermsAccepted = 1;
			}
			
		}
		
		if(isCheckedCorrect_information_clicked==1){
				userData.isAllContactCorrect = isCheckedCorrect_information;
		}else{
			
			if($('#correct_information[type="checkbox"]').prop("checked")==false){
					userData.isAllContactCorrect = 0;
			}else{
					userData.isAllContactCorrect = 1;
			}
		}
		
		if(isCheckedTrack_loan_clicked==1){
				userData.haveSmartPhone = isCheckedTrack_loan;
		}else{
			
			if($('#track_loan[type="checkbox"]').prop("checked")==false){
					userData.haveSmartPhone = 0;
			}else{
					userData.haveSmartPhone = 1;
			}
		}

	
		 //userData.isFirstTimeUser = 0;
		//alert(userData.haveSmartPhone);
		console.log(userData)
		
		
	 $.ajax({
		 
			  type: "POST",
			  url: base_url+'Api/update_profile',
			  data:{userData:userData},
			  beforeSend : function(){
				  $('#loader').show();
			  },
			  success:function(data) {
				  console.log(data)
				$(".fa-spinner").hide();
				// alert(data['temp']);
				//alert(data['status'])
				if(data['status'] == 1 && data['temp']==true){
					//data['temp']  = false;
					window.location.href = base_url+'admin/users/notification_setting';
				} else{
					
					 location.reload(true);
				}
				 $('#loader').hide();
			  },
			  error: function(e){
				  console.log(e);
				  $('#loader').hide();
			  },
			  complete: function(){
				  $('#loader').hide();
			  }
			});  
	}
	//$(".fa-spinner").show();
	///validateProfileForm();
	//send data by ajax
	/* $.ajax({
      type: "POST",
      url: base_url+'Api/update_profile',
      data:$('#personal_details_processor').serializeArray(),
      success:function(data) {
		$(".fa-spinner").hide();
		// alert(data['temp']);
		if(data['status'] == 1 && data['temp']){
			window.location.href = base_url+'admin/users/notification_setting'; 
		} else{
			window.location.href = base_url+'admin/users/profile'; 
		}
      }
    });*/

}); 


// ajax login
$(document).on('click', "#submit_login", function(e){
	e.preventDefault();
	//$(".fa-spinner").show();
	
	//validate
	$username = $("#user_name_login").val();
	$password = $("#password").val();
	/* if(!$username){
		$(".login_error").html("Username Required!");
	}else if(!$password){
		$(".login_error").html("Password Required!");
		//alert("Password Required!");
	} */
	if($username!="" && $password!=""){
		//send data by ajax
		 $('#loader').show();
			$.ajax({
			  type: "POST",
			  url: base_url+'Api/login',
			  data: $('#login_form').serializeArray(),
			  success: function(data) {
				  $('#loader').hide();
				  console.log(data)
				$(".fa-spinner").hide(); // hide loader
				$status = data['status']; 
				if($status == 1){
					$isFirstTimeUser = data['temp'];
					if($isFirstTimeUser == 1 ){
						 window.location.href = base_url+'admin/users/profile'; 
					}else{
						 window.location.href = base_url+'admin/users/dashboard'; 
					}
				}else{ // if error
					$('.login_error').html(data['message']);
				}
			  },
			  error: function(){
				  $('#loader').hide();
			  },
			  complete: function(){
				  $('#loader').hide();
			  }
			});
	}else if($username=="" && $password!=""){
		$(".login_error").html("Username Required!");
	}else if($username!="" && $password==""){
		$(".login_error").html("Password Required!");
	}else{
		$(".login_error").html("Please enter your credentials.");
	}
	
	
});


function validateProfileForm(){
	var userData = {};
	userData.fname = $.trim($("#firstname").val());
	userData.lname = $.trim($("#lastname").val());
	userData.contact = $.trim($("#contact").val());
	userData.email = $.trim($("#email").val());
	userData.username = $.trim($("#username").val());
	userData.password = $.trim($("#password").val());
	userData.password_conf = $.trim($("#password_conf").val());
	userData.accepted = isChecked;
	console.log(userData)
	
}
$(document).ready(function(){
	$('#accepted[type="checkbox"]').click(function(){
				isCheckedClicked = 1;
				if($(this).prop("checked") == true){
					//alert("Checkbox is checked.");
					isChecked = 1;
				}
				else if($(this).prop("checked") == false){
					//alert("Checkbox is unchecked.");
					isChecked = 0;
				}
			});
			
	$('#correct_information[type="checkbox"]').click(function(){
				isCheckedCorrect_information_clicked = 1;
				if($(this).prop("checked") == true){
					//alert("Checkbox is checked.");
					isCheckedCorrect_information = 1;
				}
				else if($(this).prop("checked") == false){
					//alert("Checkbox is unchecked.");
					isCheckedCorrect_information = 0;
				}
			});
$('#track_loan[type="checkbox"]').click(function(){
				isCheckedTrack_loan_clicked = 1;
				if($(this).prop("checked") == true){
					//alert("Checkbox is checked.");
					isCheckedTrack_loan = 1;
				}
				else if($(this).prop("checked") == false){
					//alert("Checkbox is unchecked.");
					isCheckedTrack_loan = 0;
				}
			});			
$("#personal_details_processor").validate({
			rules: {
				firstname: "required",
				lastname: "required",
				contact: "required",
				username: {
					required: true,
					minlength: 2
				},
				password: {
					required: true
					/* minlength: 6, */
					/* pwcheck: true */
				},
				password_conf: {
					required: true,
					/* minlength: 6, */
					equalTo: "#password"
				},
				email: {
					required: true,
					email: true
				},
				isTermsAccepted: "required"
			},
			messages: {
				firstname: "Please enter your firstnames",
				lastname: "Please enter your lastname",
				username: {
					required: "Please enter a username",
					minlength: "Your username must consist of at least 2 characters"
				},
				password: {
					required: "Please provide a password"/*,
					minlength: "Your password must be at least 6 characters long" ,
					pwcheck: "Password must contain at least 1 Number , 1 UpperCase letter , 1 Special character!" */
				},
				confirm_password: {
					required: "Please provide a password" ,/*
					minlength: "Your password must be at least 6 characters long", */
					equalTo: "Please enter the same password as above"
				},
				email: "Please enter a valid email address",
				agree: "Please accept our policy",
				isTermsAccepted:"Please Accept Terms and Conditions",
			},
			 errorPlacement: function(error, element) { 
				 if (element.attr("name") == "isTermsAccepted" ) { 
					$(error).insertAfter('label.label-accepted');
				
				}

                 if (element.attr("name") == "password" ) { 
					$(error).insertAfter('input.input-password');
				
				}

                 if (element.attr("name") == "password_conf" ) { 
					$(error).insertAfter('input.input-repassword');
				
				}

                 if (element.attr("name") == "username" ) { 
					$(error).insertAfter('input.input-username');
				
				}				
             
				}, 
			
			
		});
		$.validator.addMethod("pwcheck", function(value) {
      return /[\@\#\$\%\^\&\*\(\)\_\+\!]/.test(value) && /[a-z]/.test(value) && /[0-9]/.test(value) && /[A-Z]/.test(value)
 // has a digit
});
		
		
			
  });
$(document).on('click', "#forgotPwd", function(e){
	//alert(1)
	$("#login_form").hide();
	$("#forgot_pwd_form").show();
	
	$(".forgot-pwd").hide();
	$(".login-btn").show();
	
	$("#login-header h2").html("Forgot Password?");
	$("#login-header p").html("");
	$("#login-header p").css("padding","0");
	
});

$(document).on('click', "#login-btn-footer", function(e){
	//alert(1)
	$("#login_form").show();
	$("#forgot_pwd_form").hide();
	
	$(".forgot-pwd").show();
	$(".login-btn").hide();
	
	$("#login-header h2").html("Log In");
	$("#login-header p").html("Enter your login credentials.");
	$("#login-header p").css("padding","1em 0 1.25em 0");
	
});

$(document).on('click', "#cancelBtn", function(e){
	//alert(1)
	window.location.href = base_url+'admin/users/dashboard'; 
	
});


$(document).on('click', ".close", function(e){
	//alert(1)
	$(".alert-success").hide();
	
});