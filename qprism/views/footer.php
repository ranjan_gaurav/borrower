  <!-- Footer -->
<footer id="footer">
  <ul class="icons">
    <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
    <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
    <!--<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon fa-github"><span class="label">Github</span></a></li>
						<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>-->
    <li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
  </ul>
  <ul class="copyright">
    <li>2016 &copy; All rights reserved.</li>
    <li><a href="http://www.quatrro.com/" target="_blank">Quatrro Global Services</a></li>
  </ul>
</footer>
</div>
<!-- Scripts -->
<script src=" <?php echo base_url("assets/js/jquery.min.js"); ?>" ></script>
<script>
	var base_url = '<?php echo base_url(); ?>';
</script>
<script src="<?php  echo base_url("assets/js/jquery.dropotron.min.js"); ?>" ></script>
<script src="<?php  //echo base_url("assets/js/jquery.scrollgress.min.js"); ?>" ></script>
<script src="<?php  echo base_url("assets/js/skel.min.js"); ?>" ></script>
<script src="<?php  echo base_url("assets/js/util.js"); ?>" ></script>
<!--[if lte IE 8]><script src="" ></script><![endif]-->
<script src="<?php  echo base_url("assets/js/main.js"); ?>" ></script>
<script src="<?php echo base_url("assets/js/global.js"); ?>" ></script>
<link href="<?php //echo base_url("assets/datatables/dataTables.bootstrap.css"); ?>" rel="stylesheet" type="text/css" />
<script src="<?php //echo base_url("assets/datatables/jquery.dataTables.js"); ?>" type="text/javascript"); ?>" ></script>
<script src="<?php //echo base_url("assets/datatables/dataTables.bootstrap.js"); ?>" type="text/javascript"); ?>" ></script>
<script src="<?php //echo base_url("assets/js/jquery.validate.js"); ?>" type="text/javascript"); ?>" ></script>
<script src="<?php //echo base_url("assets/js/custom-rules.js"); ?>" type="text/javascript"); ?>" ></script>
<script src="<?php echo base_url("assets/js/jquery.validate.js"); ?>" type="text/javascript"); ?>" ></script>
</body>
</html>
<script>
$( document ).ready(function() {
	
	getLoanStatus();
	
	function getLoanStatus(){
		 $.ajax({
			 
				  type: "GET",
				  url: base_url+'Api/getLoanStatus?loanNo='+loanNo,
				  data:"",
				  beforeSend : function(){
					  //$('#loader').show();
				  },
				  success:function(data) { 
					  //$('#loader').hide();
					  console.log(data)
					  if(data.status){

						var min = 0;
						var max= 5;
						  if(data.data.trans.length>0){
						 for(var i=min;i<max;i++){
							if('S'+i == data.data.step_processed || 's'+i == data.data.step_processed){
								//alert(1)
								var id = "#s"+i;
								$(id).addClass("yellow"); //green
								var nextIndex = i+1;
								if(nextIndex<max){
									var id = "#s"+nextIndex;
									$(id).addClass("gray"); //yellow
									for(j=nextIndex+1;j<max;j++){
										//alert(2)
										var id = "#s"+j;
										//console.log(id)
										$(id).addClass("gray"); //gray
									}
								}
								break;
							}else{
								//alert(data.data.step_processed)
								var id = "#s"+i;
								$(id).addClass("green");  //green
							}
						  } }
						 else{
							  for(var i=min;i<max;i++){
							    var id = "#s"+i;
								$(id).addClass("gray");
							  }
						  }

						 if( data.data.trans.length > 0){
								var str_0="";
								var str_1="";
								var str_2="";
								var str_3="";
								var str_4="";
						 	for(var i=0;i<data.data.trans.length;i++){
								/* var ele = "#accordion-"+i+" h6";
								console.log(data.data.trans[i].canned_msg)
								$(ele).html( data.data.trans[i].canned_msg ); */
								
						 		if('S0' == data.data.trans[i].step_processed || 's0' == data.data.trans[i].step_processed){
									//var ele = ".s"+i+" h6";
									//$(ele).html( data.data.trans[i].canned_msg );
									
									
										str_0 = str_0 + '<div class="col-xs-6" style="float:left;width:50%;">'+
													'<span>'+data.data.trans[i].canned_msg+'</span>'+
												'</div>'+
												'<div class="col-xs-6" style="float:left;width:50%;">'+
													 '<input type="file" name="user_pic_'+data.data.trans[i].id+'" id="user_pic'+i+'" style="font-size: 13px;">'+
												'</div>'+
										
											
											'<div class="col-xs-6" style="float:left;width:50%;    padding-top: 12px;">'+
												' <input name="remind" id="remind"  value="1" type="checkbox">'+
												' <label for="remind">Remind me later</label>'+
											'</div>';
									
								}
								if('S1' == data.data.trans[i].step_processed || 's1' == data.data.trans[i].step_processed){
									
										str_1 = str_1 + '<div class="col-xs-6" style="float:left;width:50%;">'+
													'<span>'+data.data.trans[i].canned_msg+'</span>'+
												'</div>'+
												'<div class="col-xs-6" style="float:left;width:50%;">'+
													 '<input type="file" name='+data.data.trans[i].id+' id="user_pic'+i+'" style="font-size: 13px;" value="12">'+
												'</div>'+
										
											
											'<div class="col-xs-6" style="float:left;width:50%;    padding-top: 12px;">'+
												' <input name="remind" id="remind"  value="1" type="checkbox">'+
												' <label for="remind">Remind me later</label>'+
											'</div>';
									
								}
								if('S2' == data.data.trans[i].step_processed || 's2' == data.data.trans[i].step_processed){
									
										str_2 = str_2 + '<div class="col-xs-6" style="float:left;width:50%;">'+
													'<span>'+data.data.trans[i].canned_msg+'</span>'+
												'</div>'+
												'<div class="col-xs-6" style="float:left;width:50%;">'+
													 '<input type="file" name='+data.data.trans[i].id+' id="user_pic'+i+'" style="font-size: 13px;">'+
												'</div>'+
										
											
											'<div class="col-xs-6" style="float:left;width:50%;    padding-top: 12px;">'+
												' <input name="remind" id="remind"  value="1" type="checkbox">'+
												' <label for="remind">Remind me later</label>'+
											'</div>';
									
								}
								if('S3' == data.data.trans[i].step_processed || 's3' == data.data.trans[i].step_processed){
									
										str_3 = str_3 + '<div class="col-xs-6" style="float:left;width:50%;">'+
													'<span>'+data.data.trans[i].canned_msg+'</span>'+
												'</div>'+
												'<div class="col-xs-6" style="float:left;width:50%;">'+
													 '<input type="file" name="user_pic_'+data.data.trans[i].id+'" id="user_pic'+i+'" style="font-size: 13px;">'+
												'</div>'+
										
											
											'<div class="col-xs-6" style="float:left;width:50%;    padding-top: 12px;">'+
												' <input name="remind" id="remind"  value="1" type="checkbox">'+
												' <label for="remind">Remind me later</label>'+
											'</div>';
									
								}
								
								if('S4' == data.data.trans[i].step_processed || 's4' == data.data.trans[i].step_processed){
									//alert(data.data.trans[i].canned_msg)
										str_4 = str_4 + '<div class="col-xs-6" style="float:left;width:50%;">'+
													'<span>'+data.data.trans[i].canned_msg+'</span>'+
												'</div>'+
												'<div class="col-xs-6" style="float:left;width:50%;">'+
													 '<input type="file" name="user_pic_'+data.data.trans[i].id+'" id="user_pic'+i+'" style="font-size: 13px;">'+
												'</div>'+
										
											
											'<div class="col-xs-6" style="float:left;width:50%;    padding-top: 12px;">'+
												' <input name="remind" id="remind"  value="1" type="checkbox">'+
												' <label for="remind">Remind me later</label>'+
											'</div>';
									
									
								}
									
						 		}
								if(str_0!=""){
									//add submit 
									str_0 = str_0 +	'<div class="col-xs-6" style="float:left;width:100%;text-align: center;margin-bottom:15px;">'+
												'<input type="submit" value="Submit" name="submit" class="btnSubmit" style="margin:0 8px;border-radius:2px;"> '+
												'<input type="submit" value="Cancel" class="btnSubmit" style="margin:0 8px;border-radius:2px;"> '+
											'</div>';
								}
								if(str_1!=""){
									//add submit 
									str_1 = str_1 +	'<div class="col-xs-6" style="float:left;width:100%;text-align: center;margin-bottom:15px;">'+
												'<input type="submit" value="Submit" name="submit2" class="btnSubmit" style="margin:0 8px;border-radius:2px;"> '+
												'<input type="submit" value="Cancel" class="btnSubmit" style="margin:0 8px;border-radius:2px;"> '+
											'</div>';
								}
								if(str_2!=""){
									//add submit 
									str_2 = str_2 +	'<div class="col-xs-6" style="float:left;width:100%;text-align: center;margin-bottom:15px;">'+
												'<input type="submit" value="Submit" name="submit3" class="btnSubmit" style="margin:0 8px;border-radius:2px;"> '+
												'<input type="submit" value="Cancel" class="btnSubmit" style="margin:0 8px;border-radius:2px;"> '+
											'</div>';
								}
								if(str_3!=""){
									//add submit 
									str_3 = str_3 +	'<div class="col-xs-6" style="float:left;width:100%;text-align: center;margin-bottom:15px;">'+
												'<input type="submit" value="Submit" name="submit4" class="btnSubmit" style="margin:0 8px;border-radius:2px;"> '+
												'<input type="submit" value="Cancel" class="btnSubmit" style="margin:0 8px;border-radius:2px;"> '+
											'</div>';
								}
								if(str_4!=""){
									//add submit 
									str_4 = str_4 +	'<div class="col-xs-6" style="float:left;width:100%;text-align: center;margin-bottom:15px;">'+
												'<input type="submit" value="Submit" name="submit5" class="btnSubmit" style="margin:0 8px;border-radius:2px;"> '+
												'<input type="submit" value="Cancel" class="btnSubmit" style="margin:0 8px;border-radius:2px;"> '+
											'</div>';
								}
								
								$("#accordion-0 .uploads").append(str_0);	
								$("#accordion-1 .uploads").append(str_1);		
								$("#accordion-2 .uploads").append(str_2);	
								$("#accordion-3 .uploads").append(str_3);	
								$("#accordion-4 .uploads").append(str_4);
						 	}	
							
									
																
					  } 
						 
					  
				  },
				  error: function(e){
					  console.log(e);
					  //$('#loader').hide();
				  },
				  complete: function(){
					  //$('#loader').hide();
				  }
			}); 
	}
});




</script>