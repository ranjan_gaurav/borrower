<html>
<head>
<title>Track My Loan</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
<link rel="stylesheet" href="<?php echo base_url("assets/css/main.css"); ?>" />
<link rel="stylesheet" href="<?php echo base_url("assets/font-awesome/css/font-awesome.min.css"); ?>" />

<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
<style>
label.error {
    background: transparent;
    border-radius: 5px;
    color: #fff;
    position: relative;
    text-align: left;
    top: 3px;
    color: #f00;
}
label.error::before {
    background: none !important;
    content: "";
    height: 4px;
    left: 50%;
    margin-left: -4px;
    position: absolute;
    top: -4px;
    width: 8px;
}
</style>
<script type="text/javascript">
var loanNo = "";
</script>
</head>
<body>
<div id="page-wrapper">

  <!-- Header -->
<header id="header">
	<h1>
		<!-- <a href="index.php"> -->
		<!-- Track</a> My Loan -->
			<img alt="" src="<?php echo base_url(); ?>assets/images/home.png">
		<!--</a>-->
		<span style="position: relative; bottom: 14px; left: 10px;">Lender XYZ</span>
	</h1>
	<nav id="nav">
		<ul>
			<li><a href="<?php echo base_url(); ?>">Home </a></li>
			<?php if(is_logged_in()): ?>
			<?php $user_data = user_data(); ?> 
				<li>
					<a href="javascript:void(0)" class="icon fa-angle-down">Welcome <?php echo ucfirst(trim($user_data['firstname'])).' '.ucfirst(trim($user_data['lastname'])); ?> !</a>
					<ul>
						<li><a href="<?php echo base_url("admin/users/dashboard"); ?>">Dashboard</a></li>
						<li><a href="javascript:void(0)">My Account</a>
							<ul>
								<li><a href="<?php echo base_url("admin/users/profile"); ?>">Profile</a></li>
								<li><a href="<?php echo base_url("admin/users/notification_setting"); ?>">Account Settings</a></li>
								<!-- <li><a href="javascript:void(0)">Loan Applications</a></li>
								<li><a href="javascript:void(0)">Property Address</a></li>
								<li><a href="javascript:void(0)">Submit Docs</a></li> -->
							</ul></li>
						<li><a href="<?php echo base_url("admin/users/privacy_policy"); ?>">Privacy Policy</a></li>
						<li><a href="<?php echo base_url("admin/users/term_of_use"); ?>">Terms of use</a></li>
						<li><a href="<?php echo base_url("admin/users/contact_us"); ?>">Contact Us</a></li>
					</ul>
				</li>

				<li><a href="<?php echo base_url("logout"); ?>" class="button">Log Out</a></li>
			<?php else: ?>
				<li><a href="<?php echo base_url("login"); ?>" class="button">Log In </a></li> 
			<?php endif; ?>
		</ul>
	</nav>
</header>