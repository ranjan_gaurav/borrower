<section id="banner">
					<h2>Welcome to Track My Loan</h2>
	
						<ul class="actions">
						    <?php if(is_logged_in()): ?>
							   <li><a href="javascript:void(0)" class="button" id="cancelBtn">Learn More</a></li>
							   <li><a href="<?php echo base_url("logout"); ?>" class="button special">Log Out</a></li>
							<?php else: ?>
								<li><a href="<?php echo base_url("login"); ?>" class="button special">Log In </a></li> 
							<?php endif; ?>
						</ul>
</section>
<!-- Main -->

<!-- <section id="main" class="container">

	 <section class="box special">
		<header class="major">
			
			<p>Blandit varius ut praesent nascetur eu penatibus nisi risus faucibus nunc ornare<br />
			adipiscing nunc adipiscing. Condimentum turpis massa.</p>
		</header>
		
	</section>

	<section class="box special features">
		<div class="features-row">
			<section>
				<span class="icon major fa-bolt accent2"></span>
				<h3>Magna etiam</h3>
				<p>Integer volutpat ante et accumsan commophasellus sed aliquam feugiat lorem aliquet ut enim rutrum phasellus iaculis accumsan dolore magna aliquam veroeros.</p>
			</section>
			<section>
				<span class="icon major fa-area-chart accent3"></span>
				<h3>Ipsum dolor</h3>
				<p>Integer volutpat ante et accumsan commophasellus sed aliquam feugiat lorem aliquet ut enim rutrum phasellus iaculis accumsan dolore magna aliquam veroeros.</p>
			</section>
		</div>
		<div class="features-row">
			<section>
				<span class="icon major fa-cloud accent4"></span>
				<h3>Sed feugiat</h3>
				<p>Integer volutpat ante et accumsan commophasellus sed aliquam feugiat lorem aliquet ut enim rutrum phasellus iaculis accumsan dolore magna aliquam veroeros.</p>
			</section>
			<section>
				<span class="icon major fa-lock accent5"></span>
				<h3>Enim phasellus</h3>
				<p>Integer volutpat ante et accumsan commophasellus sed aliquam feugiat lorem aliquet ut enim rutrum phasellus iaculis accumsan dolore magna aliquam veroeros.</p>
			</section>
		</div>
	</section>-->

<!-- 	<div class="row">
		<div class="6u 12u(narrower)">

			<section class="box special">
				<span class="image featured"><img src="images/pic02.jpg" alt="" /></span>
				<h3>Sed lorem adipiscing</h3>
				<p>Integer volutpat ante et accumsan commophasellus sed aliquam feugiat lorem aliquet ut enim rutrum phasellus iaculis accumsan dolore magna aliquam veroeros.</p>
				<ul class="actions">
					<li><a href="#" class="button alt">Learn More</a></li>
				</ul>
			</section>

		</div>
		<div class="6u 12u(narrower)">

			<section class="box special">
				<span class="image featured"><img src="images/pic03.jpg" alt="" /></span>
				<h3>Accumsan integer</h3>
				<p>Integer volutpat ante et accumsan commophasellus sed aliquam feugiat lorem aliquet ut enim rutrum phasellus iaculis accumsan dolore magna aliquam veroeros.</p>
				<ul class="actions">
					<li><a href="#" class="button alt">Learn More</a></li>
				</ul>
			</section>

		</div>
	</div> -->

<!-- </section> -->
<!-- CTA -->
<section id="cta">

	<h2>Subscribe Newsletter</h2>
	

	<form>
		<div class="row uniform 50%">
			<div class="8u 12u(mobilep)">
				<input type="email" name="email" id="email" placeholder="Email Address" />
			</div>
			<div class="4u 12u(mobilep)">
				<input type="submit" value="Send Email" class="button alt fit small" />
			</div>
		</div>
	</form>

</section>