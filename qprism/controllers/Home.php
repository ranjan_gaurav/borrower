<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Front_Controller{
	
    function __construct() {
        parent::__construct();
    }


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['main_content'] = 'index';
		$this->setData($data);
	}	
	 
	public function login()
	{
		
		login('manoj', 'manoj');
		pr($_SESSION);
		$this->session->set_flashdata('message', array('status' => 'error', 'message' => 'Username and Password fields must be filled out.'));
		$this->load->view('welcome_message');
	}	
	
	public function logout()
	{
		echo "Log out successfully!";
		logout();
	}

	public function is_logged_in()
	{
		if(is_logged_in()){
			
		prd(user_data());
		}
	}
}
