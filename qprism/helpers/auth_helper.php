<?php

if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

if (! function_exists ( 'login' )) {
	function login($login, $password, $remember = false, $temp = false) {
		$thiss = & get_instance ();
		
		/* validate */
		
		// is empty
		if (empty ( $login ) || empty ( $password )) {
			$thiss->session->set_flashdata ( 'message', array (
					'status' => 'error',
					'message' => 'Username and Password fields must be filled out.' 
			) );
			return 3;
		}
		
		// getting user data by username, pass, status
		if (! $temp) {
			$userdata = $thiss->db->where ( array (
					'username' => $login,
					'password' => md5 ( $password ),
					'status' => 1 
			) )->get ( 'users' )->first_row ( 'array' );
		} else {
			
			$userdata = $thiss->db->where ( array (
					'temp_id' => $login,
					'temp_pwd' => md5 ( $password ),
					'status' => 1,
					'isFirstTimeUser' => 1 
			) )->get ( 'users' )->first_row ( 'array' );
			$today = strtotime ( date ( 'Y-m-d H:i:s' ) );
			$createdDate = $userdata ['creationDate'];
			$afterFifteenDays = date ( "Y-m-d", strtotime ( $createdDate . "+15 days" ) );
			$finalday = strtotime ( $afterFifteenDays ); // strtotime($userdata['creationDate']); print_r($createdDate); die();
		}
		
		// is username pass exist
		if ($userdata) {
			
			if (($userdata ['isFirstTimeUser'] == 1) && ($today >= $finalday)) {
				return 4;
			} 

			else {
				
				// The login was successfully validated, so setup the session
				$thiss->session->set_userdata ( 'user_id', $userdata ['id'] );
				$thiss->session->set_userdata ( 'loanNo', $userdata ['loanNo'] );
				// $thiss->session->set_flashdata('message', array('status' => 'success', 'message' => 'User successfully logined!'));
				return 1;
			}
		} 

		else {
			
			$thiss->session->set_flashdata ( 'message', array (
					'status' => 'error',
					'message' => 'Sorry! Username and Password not matching' 
			) );
			return 0;
		}
	}
}

if (! function_exists ( 'logout' )) {
	function logout() {
		$thiss = & get_instance ();
		
		$thiss->session->unset_userdata ( 'user_id' );
		$thiss->session->unset_userdata ( 'status' ); // any form status msg
		$thiss->session->unset_userdata ( 'loggedin_data' );
		return true;
	}
}

if (! function_exists ( 'is_logged_in' )) {
	function is_logged_in() {
		$thiss = & get_instance ();
		$user_data = $thiss->session->userdata ( 'user_id' );
		return $user_data ? true : false;
	}
}

if (! function_exists ( 'userdata' )) {
	function user_data($array = true) {
		$thiss = & get_instance ();
		$user_id = $thiss->session->userdata ( 'user_id' );
		$query = $thiss->db->where ( 'id', $user_id )->get ( 'users' );
		return $array ? $query->first_row ( 'array' ) : $query->first_row ();
	}
}

if (! function_exists ( 'user_id' )) {
	function user_id() {
		$thiss = & get_instance ();
		return $thiss->session->userdata ( 'user_id' );
	}
}

if (! function_exists ( 'laon_no' )) {
	function laon_no() {
		$thiss = & get_instance ();
		return $thiss->session->userdata ( 'loanNo' );
	}
}
