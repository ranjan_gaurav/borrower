<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('flash_message'))
{
  function flash_message($print =true)
    {
		$thiss = &get_instance();
       	$message = $thiss->session->flashdata('message'); 
		$message_type = ($message['status']=='success')?'success':'error';
		if($print){
		echo '<div class="alert alert-'.$message_type.'">
						
						'.$message["message"].'
					</div>';
		}else{
			return '<div class="alert alert-'.$message_type.'">
						
						'.$message["message"].'
					</div>';
		}
					/* <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>  */
    }
}

if (!function_exists('loan_data'))
{
  function loan_data($array = true)
    {
		$thiss = &get_instance();
		$user_id = user_id();
		$query = $thiss->db->where(array('loanNo'=>laon_no()))->get('canned_master');
		return $array?$query->first_row('array'):$query->first_row();
    }
}

if (!function_exists('status'))
{
  function status($id = 0)
    {
		$thiss = &get_instance();
		$user_id = user_id();
		$res = $thiss->db->where(array('id'=>$id))->get('m_status')->first_row();
		return $res?$res->status :"Not define!";
    }
}

if (!function_exists('loan_status'))
{
  	function loan_status() {
		$thiss = &get_instance();
		return $thiss->db->get('m_loan_status')->result();
    }
}
