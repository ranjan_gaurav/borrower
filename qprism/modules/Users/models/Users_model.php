<?php

class Users_model extends CI_Model {

    protected $table = "users";

    public function __construct() {
        parent::__construct();
        //date_default_timezone_set('Asia/Kolkata');
    }
	
	public function update_user($data) {
		$user_data = user_data();
		$this->db->where('id', $user_data['id']);
		$this->db->update('users', $data); 
		return true;
    }

	
	public function update_notification($data) {
		//echo 123; die();
		$user_data = user_data();
		//print_r($user_data);
		//die();
		$this->db->where('user_id', $user_data['id']);
		$this->db->update('notification', $data); 
		return true;
    }
	
    public function GetEmailIDs($user_id)
    {
    	$this->db->where('users.id',$user_id);
    	$this->db->select('canned_master.*');
    	$this->db->from('users');
    	$this->db->join('canned_master','users.loanNo =  canned_master.loanNo' );
    	$res = $this->db->get (); //echo $this->db->last_query(); die();
    	return $res->row();
    }
    
    public function UpdateStatus($data,$loan_data,$id)
    {
    	$this->db->where ('id',$id);
        //$this->db->where ( 'loanNo', $loan_data->loanNo);
    	//$this->db->where ( 'step_processed', 's1');
    	$update = $this->db->update('canned_transection',$data);
    	
    	
    	return true;
    
    }


}


