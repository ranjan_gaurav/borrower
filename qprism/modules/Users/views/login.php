<?php error_reporting(0);?>
<section id="main" class="container 75%">
<header id="login-header">
  <h2>Log In</h2>
  <p style="color:#fff;">Enter your login credentials.</p>    </header>
<div class="box">
<div class="6u 12u(narrower)" style="width:100%;">
		<i class="fa fa-spinner fa-pulse fa-3x fa-fw" style="font-size: 2em; display:none"></i>
		<label class="login_error" style="color:red;font-size:12px;text-align:center;"> <?php //echo $this->session->flashdata('message'); ?></label>
	  </div>
  <form id="login_form">
	<div class="row uniform 50%">
	  <div class="6u 12u(mobilep)">
		<input type="text" name="username" id="user_name_login" required value="" placeholder="Enter Username" />
	  </div>
	  <div class="6u 12u(mobilep)">
		<input type="password" name="password" id="password" required value="" placeholder="Enter Password" />
	  </div>
	  <div class="6u 12u(narrower) loginradio">
		<input type="radio" name="temp" value="0" id="temp" style="color:#e89980;" checked > <label for="temp" style="color:#e89980;"> Login with permanent username</label><br>
		<input type="radio" name="temp" value="1"  id="temp1" style="color:#e89980;"> <label for="temp1" style="color:#e89980;"> Login with temp username</label>
		
	  </div>
	  
	</div>
	<div id="loader" class="align-center">
						<img alt="" src="<?php echo base_url();?>assets/images/loader.gif">
						</div>
	<div class="row uniform">
	  <div class="12u">
		<ul class="actions  align-center" id="loginBtn">
		  <li>
			<input type="submit" value="Login" name="submit_login"  id="submit_login" class="button special small"/>
			
		  </li>
		  	
		</ul>

	  </div>
	</div>
  </form>
  <form id="forgot_pwd_form">
  	<div style="text-align: center; font-size: 1.5em; line-height: 1.5; padding: 0% 5% 5% 5%;">Please contact your Loan Officer / Loan Processor / Processing Manager</div>
  </form>
  		<div class="forgot-pwd">
				<a href="javascript:void(0)" id="forgotPwd">Forgot Password?</a>
		  </div>
		  <div class="login-btn">
				<a href="javascript:void(0)" id="login-btn-footer">Login</a>
		  </div>
</div>
</section>