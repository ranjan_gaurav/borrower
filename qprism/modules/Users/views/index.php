<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>table html</title>

<style>
body { margin:0; padding:0;}

.border { margin:20px 0;}
.border td { border:1px solid #ccc;}
.border thead { background:#f4f4f4;}
.border thead td { font-weight:bold;}
</style>

</head>
<body>
<center>
<table border="0" cellspacing="0" cellpadding="0" style="background-color:#fff; width:1100px;">
        <thead style="background-color:#177b57;">
                <tr style="color:#fff; font-weight:bold;">
                        <td style="padding:10px; border:1px solid #999; ">Reporting Officers to highlight officer's three strengths ? (max 250 words)</td>
                </tr>
        </thead>
        <tbody>
                <tr>
                        <td style="padding:10px; border:1px solid #999; height:100px" valign="Top">&nbsp;</td>
                        
                </tr>
        </tbody>
</table>

<table border="0" cellspacing="0" cellpadding="0" style="background-color:#fff; width:1100px; margin-top:30px;">
        <thead style="background-color:#177b57;">
                <tr style="color:#fff; font-weight:bold;">
                        <td style="padding:10px; border:1px solid #999; ">Reporting Officers to highlight officer's three areas of development ? (max 250 words)</td>
                </tr>
        </thead>
        <tbody>
                <tr>
                        <td style="padding:10px; border:1px solid #999; height:100px" valign="Top">&nbsp;</td>
                        
                </tr>
        </tbody>
</table>

<table border="0" cellspacing="0" cellpadding="0" style="background-color:#fff; width:1100px; margin-top:30px;">
        <thead style="background-color:#177b57;">
                <tr style="color:#fff; font-weight:bold;">
                        <td style="padding:10px; border:1px solid #999; ">Overall Remarks by Reviewing Officer (max 250 words)</td>
                </tr>
        </thead>
        <tbody>
                <tr>
                        <td style="padding:10px; border:1px solid #999; height:100px" valign="Top">&nbsp;</td>
                        
                </tr>
        </tbody>
</table>


</center>
</body>
</html>
