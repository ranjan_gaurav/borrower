 <?php 
 $user_data = user_data(false);  
 ?>
<section id="main" class="container 75%">
	<header>
		<h2>Contact Us</h2>
		<p>Tell us what are query, please use the form below</p>
	</header>
	
	<div class="box align-center">
		
		<?php if ($this->session->flashdata('message')) { ?>
			 <div class="alert alert-success align-center">
            <a href="#" style="float:right;" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('message'); ?>
        </div> <?php }?> 
		<form   method="post"   id="contact_us">
			<div class="row uniform 50%">
				<div class="12u">
					<input type="text" name="name" id="name" required value="<?php //echo ucfirst(trim($user_data->firstname)).' '.$user_data->lastname; ?>" placeholder="Name" aria-required="true" aria-invalid="false" class="valid">
				</div>
				<!-- <div class="6u 12u(mobilep)">
					<input type="email" name="email" id="email" required  value="<?php //echo $user_data->email; ?>" placeholder="Email">
				</div>-->
			</div>
			<div class="row uniform 50%">
				<div class="12u">
					<input type="text" name="subject" id="subject" required  value="" placeholder="Subject">
				</div>
			</div>
			<div class="row uniform 50%">
				<div class="12u">
					<textarea name="message" id="message" required  placeholder="Enter your message" rows="6"></textarea>
				</div>
			</div>
			
			<div class="row uniform">
				<div class="12u">
					<!-- <ul class="actions align-center">
						<li><input type="submit" class="button special fit" value="Send Message" name="contact_me"></li>
						<li><input type="submit" class="button special fit" value="Cancel" name="contact_me"></li>
					</ul> -->
					<ul class="actions align-center">
					<i class="fa fa-spinner fa-pulse fa-3x fa-fw" style="font-size: 2em; display:none"></i>
						<li><input type="submit" value="Send Message" name="contact_me" class="button special small "/></li>
						<li><a class="button small" href="javascript:void(0)" id="cancelBtn">Cancel</a></li>
					</ul>
				</div>
			</div>
		</form>
	</div>
</section>