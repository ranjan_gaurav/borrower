<?php // $_SESSION["temp"] = false; 
//print_r($_SESSION);
?>
<section id="main" class="container 75%">

    <header>

      <h2>Account/Notification Setting</h2>

    </header>

    <div class="box align-center">

		<?php //print_r($notification_setting); ?>

			<?php if ($this->session->flashdata('message')) { ?>
			 <div class="alert alert-success">
            <a href="#" style="float: right; color: #fff;" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('message'); ?>
        </div> <?php }?>

            <form enctype="multipart/form-data" method="post" action="" id="notification_stting_form" novalidate="novalidate">

			

        <div class="12u(narrower) align-center">

          <div class="row no-collapse 50% uniform">

            <div class="3u">

              <label>&nbsp;</label>

            </div>

            <div class="3u" style="padding-right: 28px;">

            <label>

             <i class="fa fa-envelope fa-2x" style="color:#e89980;"></i>  
           <small>Email</small> 
			</label>

            </div>

            <div class="3u" style="padding-right: 34px;">

              <label>
             <i class="fa fa-mobile fa-2x" style="color:#e89980;"></i>
             <small>Phone</small> 
              </label>

            </div>

			<div class="3u" style="padding-right: 28px;">

              <label>
             <i class="fa fa-microphone fa-2x" aria-hidden="true" style="color:#e89980;"></i></i>
             <small>Text</small> 
              </label>

            </div>

            <div class="3u">

              <label>Offers Update</label>

            </div> 

            <div class="3u">

              <input type="checkbox" id="type_offer_email" value="1" name="offersUpdateOnMail" <?php echo (isset($notification_setting['offersUpdateOnMail']) and $notification_setting['offersUpdateOnMail']==1)?'checked':1; ?> >

              <label for="type_offer_email">&nbsp;</label>

            </div>

            <div class="3u">

              <input type="checkbox" id="type_offer_phone" value="1" name="offersUpdateOnPhone" <?php echo (isset($notification_setting['offersUpdateOnPhone']) and $notification_setting['offersUpdateOnPhone']==1)?'checked':1; ?> >

              <label for="type_offer_phone">&nbsp;</label>

            </div>

			 <div class="3u">

              <input type="checkbox" id="type_offer_voice" value="1" name="offersUpdateOnVoice" <?php echo (isset($notification_setting['offersUpdateOnVoice']) and $notification_setting['offersUpdateOnVoice']==1)?'checked':1; ?> >

              <label for="type_offer_voice">&nbsp;</label>

            </div>

			<div class="3u">

              <label>Loan Update</label>

            </div>

            <div class="3u">

              <input type="checkbox" id="type_status_email" value="1" name="loanUpdateOnMail" <?php echo (isset($notification_setting['loanUpdateOnMail']) and $notification_setting['loanUpdateOnMail']==1)?'checked':1; ?>>

              <label for="type_status_email">&nbsp;</label>

            </div>

            <div class="3u">

             <input type="checkbox" id="type_status_phone" value="1" name="loanUpdateOnPhone" <?php echo (isset($notification_setting['loanUpdateOnPhone']) and $notification_setting['loanUpdateOnPhone']==1)?'checked':1; ?>>

              <label for="type_status_phone">&nbsp;</label>

            </div>

			<div class="3u">

             <input type="checkbox" id="type_status_voice" value="1" name="loanUpdateOnVoice" <?php echo (isset($notification_setting['loanUpdateOnVoice']) and $notification_setting['loanUpdateOnVoice']==1)?'checked':1; ?>>

              <label for="type_status_voice">&nbsp;</label>

            </div>

			<div class="3u">

              <label>Notification</label>

            </div>

            <div class="3u">

               <input type="checkbox" id="type_not_email" value="1" name="notificationOnMail" <?php echo (isset($notification_setting['notificationOnMail']) and $notification_setting['notificationOnMail']==1)?'checked':1; ?>>

              <label for="type_not_email">&nbsp;</label>

            </div>

            <div class="3u">

              <input type="checkbox" id="type_not_phone" value="1" name="notificationOnPhone" <?php echo (isset($notification_setting['notificationOnPhone']) and $notification_setting['notificationOnPhone']==1)?'checked':1; ?>>

              <label for="type_not_phone">&nbsp;</label>

            </div>

			<div class="3u">

              <input type="checkbox" id="type_not_voice" value="1" name="notificationOnVoice" <?php echo (isset($notification_setting['notificationOnVoice']) and $notification_setting['notificationOnVoice']==1)?'checked':1; ?>>

              <label for="type_not_voice">&nbsp;</label>

            </div>
            
            <input type="hidden" value="0" name="isFirstTimeUser">

          </div>

		  
<div id="loader" class="align-center">
						<img alt="" src="<?php echo base_url();?>assets/images/loader.gif">
						</div>
		  <div class="row uniform">

            <div class="12u">

              <ul class="actions align-center">

                <li>

                  <input type="submit" value="Update" name="addloan_details" id="notification_stting" class="button special small">

                </li>

                <li><a class="button small" id="cancelBtn" >Cancel</a></li>

              </ul>

            </div>

          </div>

        </div>

      </form>

    </div>

  </section>