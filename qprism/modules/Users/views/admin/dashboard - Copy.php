 <?php 
 $user_data = user_data(false);  
 $loan_data = loan_data(false);  
 $loan_status = loan_status();  
 
// print_r($loan_data->loanOfficerName); die();
//print_r($loan_data); die();
 ?>
 <section id="banner">
	<h2>Hello <?php echo ucfirst(trim($user_data->firstname)).' '.ucfirst(trim($user_data->lastname)); ?></h2>
	<p>Now stay updated with the current status of your loan.</p>
	<ul class="actions">
		<li><a href="<?php echo base_url("admin/users/dashboard"); ?>" class="button">Dashboard</a></li>
		<li><a href="<?php echo base_url("logout"); ?>" class="button special">Log Out</a></li>
	</ul>
</section>
<!-- Main -->
<section  id="main" class="container 75% dashboard_margin">
	<div class="box">
		<div class="row uniform 50%">
			<div class="6u 12u(mobilep)">
				<ul class="alt" style="text-align:center;">
					<li><strong style="color:#000000; font-weight:bold;">Loan Number</strong> : <span style="color:#444;"><?php echo isset($loan_data->loanNumber)?$loan_data->loanNumber:null; ?></span></li>
					<li><strong style="color:#000000; font-weight:bold;">Loan Amount </strong> : <span style="color:#444;"><?php echo isset($loan_data->loanAmount)?$loan_data->loanAmount:null; ?> </span></li>
					<li><strong style="color:#000000; font-weight:bold;">Status </strong> : <span style="color:#444;"><?php echo isset($loan_data->status)?status($loan_data->status):null; ?></span></li>
					</ul>
			</div>
			<div class="6u 12u(mobilep)">
				<ul class="alt" style="text-align:center;">
					<li><strong style="color:#000000; font-weight:bold;">Disbursement Date </strong> : <span style="color:#444;"><?php echo isset($loan_data->disbursementDate)?date("d-m-y", strtotime($loan_data->disbursementDate)):null; ?></span></li>
					<li><strong style="color:#000000; font-weight:bold;">Processor </strong> : <span style="color:#444;"><?php echo isset($loan_data->processor)?$loan_data->processor:null; ?></span></li>
					<li><strong style="color:#000000; font-weight:bold;">Branch </strong> : <span style="color:#444;"><?php echo isset($loan_data->branch)?$loan_data->branch:null; ?></span></li>
				
				</ul>
			</div>
		</div>
		<div id="text_new">
			<h2>Status</h2>
		</div>	 
		<div class="12u" style="text-align:center;">
			<div class="accordion">
				<?php $color_array = array('gray','green','yellow','gray','green',); ?>
				<?php foreach($loan_status as $key => $status): ?>
				<div class="accordion-section">
					<a class="accordion-section-title" href="#accordion-<?php echo $key; ?>" style="color:#000000; font-size:18px!important;">
						<?php echo $status->type; ?> 												
						<div class="sphere <?php echo $color_array[$key]; ?>"></div> 
					</a>
					<div id="accordion-<?php echo $key; ?>" class="accordion-section-content" style="display:none;">
					
						  <input type="text" value="Text" style="width:auto; float:left; height:28px;">
						  <input type="file" style="float:left;">
						  <br><br>
						<div style="float:left;">
								<input name="remind" id="remind"  value="1" type="checkbox">
								<label for="remind">Remind me later</label>
							</div>
						  <input type="submit" value="Submit" class="btnSubmit"  style="float:left;">							
											
					</div><!--end .accordion-section-content-->
				</div>
				<?php endforeach; ?>

			</div>
		</div>
	</div>
</section>

  <!-- CTA -->
  <section id="cta">
    <h2>Subscribe Newsletter</h2>
    <p>Blandit varius ut praesent nascetur eu penatibus nisi risus faucibus nunc.</p>
    <form>
      <div class="row uniform 50%">
        <div class="8u 12u(mobilep)">
          <input type="email" name="email" id="email" placeholder="Email Address" />
        </div>
        <div class="4u 12u(mobilep)">
          <input type="submit" value="Sign Up" class="fit" />
        </div>
      </div>
    </form>
  </section>
  <!-- Footer -->
