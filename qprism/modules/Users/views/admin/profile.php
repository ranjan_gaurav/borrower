<?php error_reporting(0);?>
<?php  //$_SESSION["temp"] = false; 
//print_r($_SESSION);
?>
<section id="main" class="container 75%">
	<header>
		<h2>Profile Details</h2>
		<p>Edit Below To Update Your Details.</p>
	</header>
	<div class="box">
		<?php echo isset($_SESSION['status'])?$_SESSION['status']:''; unset($_SESSION['status']);?>				
		<form enctype="multipart/form-data" method="post" action="" id="personal_details_processor">
			<?php $user_data = user_data();  //pr($_SESSION);?>
			<?php if ($this->session->flashdata('message')) { ?>
			 <div class="alert alert-success align-center">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('message'); ?>
        </div> <?php }?>
			<div class="row uniform 50%">
				<div class="6u 12u(mobilep)">
					<input type="text" name="firstname" id="firstname" required value="<?php echo $user_data['firstname']; ?>" placeholder="First Name" />
					<span style="color:red;"><?php echo isset($_SESSION['error_firstname'])?$_SESSION['error_firstname']:''; unset($_SESSION['error_firstname']);?></span>
				</div>
				<div class="6u 12u(mobilep)">
					<input type="text" name="lastname" id="lastname" value="<?php echo $user_data['lastname']; ?>" placeholder="Last Name" />
					<span style="color:red;"><?php echo isset($_SESSION['error_lastname'])?$_SESSION['error_lastname']:'';  unset($_SESSION['error_lastname']);?></span>
				</div>
			</div>
			
			<div class="row uniform 50%" style="display: none;">
				<div class="6u 12u(mobilep)">
					<input type="text" name="contact" id="contact" value="<?php echo $user_data['contact']; ?>" placeholder="Phone No" />
					<span style="color:red;"><?php echo isset($_SESSION['error_contact'])?$_SESSION['error_contact']:'';  unset($_SESSION['error_contact']);?></span>
				</div>
				
				<div class="6u 12u(mobilep)">
					<input type="email" name="email" id="email" value="<?php echo $user_data['email']; ?>" placeholder="Email Id" />
					<span style="color:red;"><?php echo isset($_SESSION['error_email'])?$_SESSION['error_email']:'';  unset($_SESSION['error_email']);?></span>
				</div>
			</div>
			
			<div class="row uniform 50%">
				<div class="6u 12u(mobilep)">
					<input class="input-username" type="text" name="username" id="username" value="<?php echo $user_data['username']; ?>" placeholder="Change User Name"  />
					<div id="user-result" style="padding-top:5px; color:#FF0000"></div>
				</div>
											</div>
			
			<div class="row uniform 50%">
				<div class="6u 12u(mobilep)">
					<input class="input-password" type="password" name="password" id="password" value="" placeholder="Change Password" />
					<span style="color:red;"><?php echo isset($_SESSION['error_password'])?$_SESSION['error_password']:'';  unset($_SESSION['error_password']);?></span>
				</div>
				
				<div class="6u 12u(mobilep)">
					
					<input class="input-repassword" type="password" name="password_conf" id="password_conf" value="" placeholder="Re Enter Password" />
					<span style="color:red;"><?php echo isset($_SESSION['error_password_conf'])?$_SESSION['error_password_conf']:'';  unset($_SESSION['error_password_conf']);?></span>
				</div>
			</div>
			
			<div class="row uniform 50%">
							<div>
								<input type="checkbox" name="haveSmartPhone" id="track_loan" <?php echo ($user_data['haveSmartPhone']==1)?'checked':1; ?>>
								<label for="track_loan">I have a smart phone and would like to track my loan.</label>
							</div>
						</div>
						
			<div class="row uniform 50%">
							<div>
							   <input type="checkbox" name="isAllContactCorrect" id="correct_information"  <?php echo ($user_data['isAllContactCorrect']==1)?'checked':1; ?>>
								<label for="correct_information">All contact/informations is correct including cell phone no.</label>
							</div>
						</div>	
						
						
						
			<div class="row uniform 50%">
							<div class="12u">
								
								<input type="checkbox" name="isTermsAccepted" id="accepted"   <?php echo ($user_data['isTermsAccepted']==1)?'checked': 1; ?>>
								<label class="label-accepted" for="accepted">I accept <a href="<?php echo base_url();?>admin/users/term_of_use" target="_blank">Terms of Use and Privacy Policy.</a></label>
							</div>
							<span style="color:red;"><?php echo isset($_SESSION['error_isTermsAccepted'])?$_SESSION['error_isTermsAccepted']:'';  unset($_SESSION['error_isTermsAccepted']);?></span>
						</div>	
					<div id="loader" class="align-center">
						<img alt="" src="<?php echo base_url();?>assets/images/loader.gif">
						</div>					
			<div class="row uniform">
				<div class="12u">
					<ul class="actions align-center">
					<i class="fa fa-spinner fa-pulse fa-3x fa-fw" style="font-size: 2em; display:none"></i>
						<li><input type="submit" value="Update" id ="update_profile" name="update_details" class="button special small "/></li>
						<li><a class="button small" href="javascript:void(0)" id="cancelBtn">Cancel</a></li>
					</ul>
				</div>
			</div>
		</form>
		
	</div>

</section>
