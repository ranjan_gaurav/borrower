 <?php 
 $user_data = user_data(false);  
 
 $loan_data = loan_data(false);  
 $loan_status = loan_status();  
 
// print_r($loan_data->loanOfficerName); die();
//print_r($loan_data); die();
 ?>
 <script>
loanNo = "<?php echo isset($loan_data->loanNo)?$loan_data->loanNo:null; ?>";
 </script>
 <section id="banner" onload="getLoanStatus()">
	<h2>Hello <?php echo ucfirst(trim($user_data->firstname)).' '.ucfirst(trim($user_data->lastname)); ?></h2>
	<p>Now stay updated with the current status of your loan.</p>
	<ul class="actions">
		<li><a href="<?php echo base_url("admin/users/dashboard"); ?>" class="button">Dashboard</a></li>
		<li><a href="<?php echo base_url("logout"); ?>" class="button special">Log Out</a></li>
	</ul>
</section>
<!-- Main -->
<section  id="main" class="container 75% dashboard_margin">	
	<div class="box"><?php if ($this->session->flashdata('message')) { ?>			 <div class="alert alert-success align-center">            <a href="#" style="float:right;" class="close" data-dismiss="alert" aria-label="close">&times;</a>            <strong>Success!</strong> <?php echo $this->session->flashdata('message'); ?>        </div> <?php }?> 
		<div class="row uniform 50%">						
			<div class="6u 12u(mobilep)">
				<ul class="alt" style="text-align:left;">
				<?php //print_r($loan_data);?>
					<li><strong style="color:#000000; font-weight:bold;">Loan Number</strong> : <span style="color:#444;"><?php echo isset($loan_data->loanNo)?$loan_data->loanNo:null; ?></span></li>
					<li><strong style="color:#000000; font-weight:bold;">Loan Officer </strong> : <span style="color:#444;"><?php echo isset($loan_data->agent)?$loan_data->agent:null; ?> </span></li>
					<li><strong style="color:#000000; font-weight:bold;">Email </strong> : <span style="color:#444;"><?php echo isset($loan_data->lo_email)?($loan_data->lo_email==""?"NA":$loan_data->lo_email):null; ?></span></li>
					<li style="display:none;"><strong style="color:#000000; font-weight:bold;">Loan Amount </strong> : <span style="color:#444;"><?php echo isset($loan_data->loan_amt)?$loan_data->loan_amt:null; ?></span></li>
					</ul>
			</div>
			<div class="6u 12u(mobilep)">
				<ul class="alt" style="text-align:left;">
					<li><strong style="color:#000000; font-weight:bold;">Branch </strong> : <span style="color:#444;"><?php echo isset($loan_data->branch_desc)?$loan_data->branch_desc:null; ?></span></li>
					<li><strong style="color:#000000; font-weight:bold;">Loan Processor </strong> : <span style="color:#444;"><?php echo isset($loan_data->processor)?$loan_data->processor:null; ?></span></li>
					<li><strong style="color:#000000; font-weight:bold;">Email </strong> : <span style="color:#444;"><?php echo isset($loan_data->lp_email)?($loan_data->lp_email==""?"NA":$loan_data->lp_email):null; ?></span></li>
					<li style="display:none;><strong style="color:#000000; font-weight:bold;">Purpose </strong> : <span style="color:#444;"><?php echo isset($loan_data->purpose)?$loan_data->purpose:null; ?></span></li>
				
				</ul>
			</div>
		</div>
		<div id="text_new">
			<h2>Status</h2>
		</div>
		  <form action="" method="post" enctype="multipart/form-data">	 
		<div class="12u" style="text-align:center;">
			<div class="accordion">
				<?php $color_array = array('gray','green','yellow','gray','green',); ?>
				<?php //foreach($loan_status as $key => $status): ?>
				<!--<div class="accordion-section">
					<a class="accordion-section-title" href="#accordion-<?php echo $key; ?>" style="color:#000000; font-size:18px!important;">
						<?php //echo $status->type; ?> 												
						<div class="sphere <?php //echo $color_array[$key]; ?>"></div> 
					</a>
					<div id="accordion-<?php //echo $key; ?>" class="accordion-section-content" style="display:none;">
					
						  <input type="text" value="Text" style="width:auto; float:left; height:28px;">
						  <input type="file" style="float:left;">
						  <br><br>
						<div style="float:left;">
								<input name="remind" id="remind"  value="1" type="checkbox">
								<label for="remind">Remind me later</label>
							</div>
						  <input type="submit" value="Submit" class="btnSubmit"  style="float:left;">							
											
					</div><!--end .accordion-section-content-->
				<!--</div>-->
				<?php //endforeach; ?>
			<div class="accordion-section">
					<a class="accordion-section-title" href="#accordion-0" style="color:#000000; font-size:18px!important;padding-bottom: 8px">
						Initial Application Review 
						<div id="s0" class="sphere"></div> 
					</a>
					<div id="accordion-0" class="accordion-section-content s0" style="display:none;">
						<h6 style="color:#000000 !important; font-size: 15px;">
						Loan Application Submitted and Under Review 
						</h6><div class="" style="text-align: left;font-size:13px;">														<div class="uploads">														</div>							</div>
						 <!-- <input type="text" value="Text" style="width:auto; float:left; height:28px;">
						  <input type="file" style="float:left;">
						  <br><br>
						<div style="float:left;">
								<input name="remind" id="remind"  value="1" type="checkbox">
								<label for="remind">Remind me later</label>
							</div>
						  <input type="submit" value="Submit" class="btnSubmit"  style="float:left;"> -->							
											
					</div>
			</div>
			<div class="accordion-section">
					<a class="accordion-section-title" href="#accordion-1" style="color:#000000; font-size:18px!important;padding-bottom: 8px">
						In Processing
						<div id="s1" class="sphere"></div> 
					</a>
					<div id="accordion-1" class="accordion-section-content s1" style="display:none;">
						<h6 style="color:#000000 !important;font-size: 15px;margin-bottom: 3%;">
						Loan Application is being Processed
						</h6>
						<!-- <input type="text" value="Text" style="width:auto; float:left; height:28px;">
						  <input type="file" style="float:left;">
						  <br><br>
						<div style="float:left;">
								<input name="remind" id="remind"  value="1" type="checkbox">
								<label for="remind">Remind me later</label>
							</div>
						  <input type="submit" value="Submit" class="btnSubmit"  style="float:left;"> 		-->
						<div class="" style="text-align: left;font-size:13px;">
							
							<div class="uploads">
								<!-- <div class="col-xs-6" style="float:left;width:50%;">
									<strong>A.</strong> Re-submit your Bank Statement
								</div>
								<div class="col-xs-6" style="float:left;width:50%;">
									 <input type="file" name="user_pic0" id="user_pic0" style="font-size: 13px;">
								</div>
						
							
							<div class="col-xs-6" style="float:left;width:50%;    padding-top: 12px;">
								 <input name="remind" id="remind"  value="1" type="checkbox">
								 <label for="remind">Remind me later</label>
							</div>
							
							</div>
							<div class="col-xs-6" style="float:left;width:50%;">
								<strong>B.</strong> W2 for June 2016 is missing
							</div>
							<div class="col-xs-6" style="float:left;width:50%;">
								  <input type="file" name="user_pic1" id="user_pic0" style="font-size: 13px;">
							</div>
							<div class="col-xs-6" style="float:left;width:50%;">
							</div>
							<div class="col-xs-6" style="float:left;width:50%;    padding-top: 12px;">
								<input name="remind2" id="remind2"  value="1" type="checkbox">
								 <label for="remind2">Remind me later</label>
							</div> 
							
							<div class="col-xs-6" style="float:left;width:100%;text-align: center;margin-bottom:15px;">
								<input type="submit" value="Submit" name="submit2" class="btnSubmit" style="margin:0 8px;border-radius:2px;"> 
								<input type="submit" value="Cancel" class="btnSubmit" style="margin:0 8px;border-radius:2px;"> 
							</div>-->
						</div>
											
					</div>
			</div>
			<div class="accordion-section">
					<a class="accordion-section-title" href="#accordion-2" style="color:#000000; font-size:18px!important;padding-bottom: 8px">
						<!-- Preliminary Approval with Conditions -->Underwriting Conditions
						<div id="s2" class="sphere"></div> 
					</a>
					<div id="accordion-2" class="accordion-section-content s2" style="display:none;">
						<h6 style="color:#000000 !important;font-size: 15px;margin-bottom: 3%;">
						Underwriting Decision with Conditions 
						</h6>
						<div class="" style="text-align: left;font-size:13px;">
							
							<div class="uploads">
								<!--<div class="col-xs-6" style="float:left;width:50%;">
									<strong>A.</strong> Re-submit your Bank Statement
								</div>
								<div class="col-xs-6" style="float:left;width:50%;">
									<input type="file" name="user_pic2" id="user_pic" style="font-size: 13px;">
								</div>
						
							
								<div class="col-xs-6" style="float:left;width:50%;    padding-top: 12px;">
									 <input name="remind" id="remind"  value="1" type="checkbox">
									 <label for="remind">Remind me later</label>
								</div>
							
							</div>
							<div class="col-xs-6" style="float:left;width:50%;">
								<strong>B.</strong> W2 for June 2016 is missing
							</div>
							<div class="col-xs-6" style="float:left;width:50%;">
								<input type="file" name="user_pic3" id="user_pic1" style="font-size: 13px;">
							</div>
							<div class="col-xs-6" style="float:left;width:50%;">
							</div>
							<div class="col-xs-6" style="float:left;width:50%;    padding-top: 12px;">
								<input name="remind2" id="remind2"  value="1" type="checkbox">
								 <label for="remind2">Remind me later</label>
							</div>
							 
							<div class="col-xs-6" style="float:left;width:100%;text-align: center;margin-bottom:15px;">
								<input type="submit" value="Submit" name="submit" class="btnSubmit" style="margin:0 8px;border-radius:2px;"> 
								<input type="submit" value="Cancel" class="btnSubmit" style="margin:0 8px;border-radius:2px;"> 
							</div>-->
						</div>						
											
					</div>
			</div>
			<div class="accordion-section">
					<a class="accordion-section-title" href="#accordion-3" style="color:#000000; font-size:18px!important;padding-bottom: 8px">
						Final Approval Received
						<div id="s3" class="sphere"></div> 
					</a>
					<div id="accordion-3" class="accordion-section-content s3" style="display:none;">
						<h6 style="color:#000000 !important;font-size: 15px;margin-bottom: 3%;">
						Loan Approved and Closing to be Scheduled
						
						</h6>
						<div class="" style="text-align: left;font-size:13px;">
							<div class="uploads">
							
								<!--<div class="col-xs-6" style="float:left;width:50%;">
									<strong>A.</strong> Re-submit your Bank Statement
								</div>
								<div class="col-xs-6" style="float:left;width:50%;">
										<input type="file" name="user_pic4" id="user_pic4" style="font-size: 13px;">
								</div>
						
							
							<div class="col-xs-6" style="float:left;width:50%;    padding-top: 12px;">
								 <input name="remind" id="remind"  value="1" type="checkbox">
								 <label for="remind">Remind me later</label>
							</div>
							
							</div>
							
							<div class="col-xs-6" style="float:left;width:50%;">
								<strong>B.</strong> W2 for June 2016 is missing
							</div>
							<div class="col-xs-6" style="float:left;width:50%;">
								 	<input type="file" name="user_pic5" id="user_pic5" style="font-size: 13px;">
							</div>
							<div class="col-xs-6" style="float:left;width:50%;">
							</div>
							<div class="col-xs-6" style="float:left;width:50%;    padding-top: 12px;">
								<input name="remind2" id="remind2"  value="1" type="checkbox">
								 <label for="remind2">Remind me later</label>
							</div>
							
							<div class="col-xs-6" style="float:left;width:100%;text-align: center;margin-bottom:15px;">
								<input type="submit" value="Submit" name="submit3" class="btnSubmit" style="margin:0 8px;border-radius:2px;"> 
								<input type="submit" value="Cancel" class="btnSubmit" style="margin:0 8px;border-radius:2px;"> 
							</div>-->
						</div>						
											
					</div>
			</div>
			<div class="accordion-section">
					<a class="accordion-section-title" href="#accordion-4" style="color:#000000; font-size:18px!important;padding-bottom: 8px">
						Closing Scheduled
						<div id="s4" class="sphere"></div> 
					</a>
					<div id="accordion-4" class="accordion-section-content s4" style="display:none;">
						<h6 style="color:#000000 !important;font-size: 15px;"></h6>						<div class="" style="text-align: left;font-size:13px;">							<div class="uploads">
						 <!-- <input type="text" value="Text" style="width:auto; float:left; height:28px;">
						  <input type="file" style="float:left;">
						  <br><br>
						<div style="float:left;">
								<input name="remind" id="remind"  value="1" type="checkbox">
								<label for="remind">Remind me later</label>
							</div>
						  <input type="submit" value="Submit" class="btnSubmit"  style="float:left;"> -->							
						</div>						</div>					
					</div>
			</div>
			</div>
		</div> </form>
	</div>
</section>

  <!-- CTA -->
  <!--<section id="cta">
    <h2>Subscribe Newsletter</h2>
    <p>Blandit varius ut praesent nascetur eu penatibus nisi risus faucibus nunc.</p>
    <form>
      <div class="row uniform 50%">
        <div class="8u 12u(mobilep)">
          <input type="email" name="email" id="email" placeholder="Email Address" />
        </div>
        <div class="4u 12u(mobilep)">
          <input type="submit" value="Sign Up" class="fit" />
        </div>
      </div>
    </form>
  </section>-->
  <!-- Footer -->
