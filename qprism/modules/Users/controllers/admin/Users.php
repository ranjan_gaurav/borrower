<?php
class Users extends Admin_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->model ( 'Users_model' );
		if (! is_logged_in ()) {
			redirect ( '/login' );
		}
	}
	function profile() {
		// echo base_url('/index.php/admin/users/profile'); die();
		$data = $this->input->post (); // print_r($data); //die();
		$data ['main_content'] = 'profile';
		
		$this->setData ( $data );
	}
	function notification_setting() {
		$data ['main_content'] = 'notification_setting';
		$user_id = $this->session->userdata ( 'user_id' );
		$data ['notification_setting'] = $this->db->where ( 'user_id', $user_id )->get ( 'notification' )->first_row ( 'array' );
		$this->setData ( $data );
	}
	function dashboard() {
		// error_reporting(0);
		$target_dir = "uploads/";
		// echo 123; die();
		
	    $loan_data = loan_data(false);  //print_r($loan_data); die();
		
		if (isset ( $_POST ['submit'] )) {
		   			$files = $_FILES; 

			$key =  array_keys($files); 
			$config = array ();
		    ini_set ( 'upload_max_filesize', '200M' );
			ini_set ( 'post_max_size', '200M' );
			ini_set ( 'max_input_time', 3000 );
			ini_set ( 'max_execution_time', 3000 );
			$config ['upload_path'] = $target_dir;
			$config ['allowed_types'] = '*';
			
			$this->load->library ( 'upload', $config );
			$this->upload->initialize ( $config );
			
			foreach($key as $val)
			{
			
			 $this->upload->do_upload ($val);
			
			}
			
			$val = array();
			foreach($files as $k=>$v)
			{
				
				if($v['name']!=""){
					
					$data['is_uploaded'] = 1;
					$update = $this->Users_model->UpdateStatus($data,$loan_data,$k);
					$val[] = $v['name'];
					
				}
				
			}
			
		
			
			    $this->session->set_flashdata ( 'message', 'Your file has been successfully uploaded' );
		 
		
				
				$user_id = $this->session->userdata ( 'user_id' );
				$mail = $this->Users_model->GetEmailIDs ( $user_id ); // print_r($data); die();
				$this->sendMail ( $val,$mail );
				
			    redirect('admin/users/dashboard');
		} 

		else if (isset ( $_POST ['submit2'] )) {

			
			 
			$files = $_FILES; 

			$key =  array_keys($files); 
			$config = array ();
		    ini_set ( 'upload_max_filesize', '200M' );
			ini_set ( 'post_max_size', '200M' );
			ini_set ( 'max_input_time', 3000 );
			ini_set ( 'max_execution_time', 3000 );
			$config ['upload_path'] = $target_dir;
			$config ['allowed_types'] = '*';
			
			$this->load->library ( 'upload', $config );
			$this->upload->initialize ( $config );
			
			foreach($key as $val)
			{
			
			 $this->upload->do_upload ($val);
			
			}
			
			$val = array();
			foreach($files as $k=>$v)
			{
				
				if($v['name']!=""){
					
					$data['is_uploaded'] = 1;
					$update = $this->Users_model->UpdateStatus($data,$loan_data,$k);
					$val[] = $v['name'];
					
				}
				
			}
			
		
			
			    $this->session->set_flashdata ( 'message', 'Your file has been successfully uploaded' );
		 
		
				
				$user_id = $this->session->userdata ( 'user_id' );
				$mail = $this->Users_model->GetEmailIDs ( $user_id ); // print_r($data); die();
				$this->sendMail ( $val,$mail );
				
			    redirect('admin/users/dashboard');
				
			
		} 

		else if (isset ( $_POST ['submit3'] )) {
			
					$files = $_FILES; 

			$key =  array_keys($files); 
			$config = array ();
		    ini_set ( 'upload_max_filesize', '200M' );
			ini_set ( 'post_max_size', '200M' );
			ini_set ( 'max_input_time', 3000 );
			ini_set ( 'max_execution_time', 3000 );
			$config ['upload_path'] = $target_dir;
			$config ['allowed_types'] = '*';
			
			$this->load->library ( 'upload', $config );
			$this->upload->initialize ( $config );
			
			foreach($key as $val)
			{
			
			 $this->upload->do_upload ($val);
			
			}
			
			$val = array();
			foreach($files as $k=>$v)
			{
				
				if($v['name']!=""){
					
					$data['is_uploaded'] = 1;
					$update = $this->Users_model->UpdateStatus($data,$loan_data,$k);
					$val[] = $v['name'];
					
				}
				
			}
			
		
			
			    $this->session->set_flashdata ( 'message', 'Your file has been successfully uploaded' );
		 
		
				
				$user_id = $this->session->userdata ( 'user_id' );
				$mail = $this->Users_model->GetEmailIDs ( $user_id ); // print_r($data); die();
				$this->sendMail ( $val,$mail );
				
			    redirect('admin/users/dashboard');
		}
		
		else if(isset ( $_POST ['submit4'] ))
		{
			$files = $_FILES;
			
			$key =  array_keys($files);
			$config = array ();
			ini_set ( 'upload_max_filesize', '200M' );
			ini_set ( 'post_max_size', '200M' );
			ini_set ( 'max_input_time', 3000 );
			ini_set ( 'max_execution_time', 3000 );
			$config ['upload_path'] = $target_dir;
			$config ['allowed_types'] = '*';
				
			$this->load->library ( 'upload', $config );
			$this->upload->initialize ( $config );
				
			foreach($key as $val)
			{
					
				$this->upload->do_upload ($val);
					
			}
				
			$val = array();
			foreach($files as $k=>$v)
			{
			
				if($v['name']!=""){
						
					$data['is_uploaded'] = 1;
					$update = $this->Users_model->UpdateStatus($data,$loan_data,$k);
					$val[] = $v['name'];
						
				}
			
			}
				
			
				
			$this->session->set_flashdata ( 'message', 'Your file has been successfully uploaded' );
				
			
			
			$user_id = $this->session->userdata ( 'user_id' );
			$mail = $this->Users_model->GetEmailIDs ( $user_id ); // print_r($data); die();
			$this->sendMail ( $val,$mail );
			
			redirect('admin/users/dashboard');
		}
		
		
		$data ['main_content'] = 'dashboard';
	//	redirect('admin/users/dashboard');
		$this->setData ( $data );
	}
	function contact_us() {
		
		$value = $this->input->post();
		$user_id = $this->session->userdata ( 'user_id' );
		$mail = $this->Users_model->GetEmailIDs ( $user_id );
		if($value)
		{
		$this->session->set_flashdata ( 'message', 'Thank You! Your message has been sent. We will contact you soon!' );
		$this->contact_mail ($mail,$value);
		}
		 // contact mail
		
		
		   // $this->session->set_flashdata ( 'message', 'Thank You! Your message has been sent. We will contact you soon!' );
			//redirect('admin/users/contact_us');
		
		
		//$this->session->set_flashdata ( 'message', 'Thank You! Your message has been sent. We will contact you soon!' );
		//redirect('admin/users/contact_us');
		$data ['main_content'] = 'contact_us';
		$this->setData ( $data );
	}
    function contact_mail($mail,$value) {
		error_reporting(0);
	//	print_r($value); die();
		//$subject = $value['subject'];
		
		$message  = "<b>Name: </b>". $value['name'] .'</br>';
		$message .= '<b>Subject: </b>'. $value['subject'] .'</br>';
		$message .= '<b>Message: </b>'. $value['message'] .'</br>';
		
		
		//print_r($subject);
		//print_r($message);  //die();
		
		$LOID = $mail->lo_email;
		$LPID = $mail->lp_email;
		$From = $mail->last_name;
		$TO = array (
				$LOID,
				$LPID 
		);
		$this->load->library ( 'email' );
		$this->email->set_mailtype('html');
		$this->email->from ($From );
		$this->email->to ( $TO );
		$this->email->subject ('Contact us query');
		$this->email->message ($message);
		$this->email->send ();
	}
	function privacy_policy() {
		$data ['privacy'] = $this->db->get ( 'privacy_policy' )->first_row ( 'array' );
		
		// print_r($data['privacy']); die();
		
		$data ['main_content'] = 'privacy_policy';
		$this->setData ( $data );
	}
	function term_of_use() {
		$data ['terms'] = $this->db->get ( 'terms_conditions' )->first_row ( 'array' );
		$data ['main_content'] = 'term_of_use';
		$this->setData ( $data );
	}
	function sendMail($val,$mail) {
		//print_r($val); die();
	//	echo 234; die();
		error_reporting ( 0 );
		$LOID = $mail->lo_email;
		$LPID = $mail->lp_email;
		$From = $mail->borrower_email;
		$TO = array (
				$LOID,
				$LPID 
		);
		$this->load->library ( 'email' );
		$this->email->from ( $From, 'TRACK MY LOAN' );
		$this->email->to ( $TO );
		$this->email->subject ( 'Report' );
		$this->email->message ( 'Hi , Please find this attachment' );
		
			
		 // print_r($v); die();
		//print_r($val);	 die();
		for($i=0;$i<sizeof($val);$i++){
			$this->email->attach ( base_url() . 'uploads/' . $val[$i]);
		}
	  // die();
		
		
	
	    //$this->email->attach ( base_url ( 'uploads' ) .'/'  . $file2);
		 $this->email->send ();
		$this->email->clear(); 
	}
}