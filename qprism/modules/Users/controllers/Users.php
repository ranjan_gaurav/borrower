<?php
class Users extends Front_Controller {

    function __construct() {
        parent::__construct();
    }

   public function login() {
	   if(is_logged_in()){
		   redirect('/admin/users/profile');
	   }
	   
	   $data['main_content'] = 'login';
	   $this->setData($data);
   }

   public function logout() {
	   if(logout()){
		   if(isset($_SESSION['temp'])){unset($_SESSION['temp']);}
		   redirect('/login');
	   }
   }
}