<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

/**
 * @author Anjani Kr. Gupta
 * @date: 29th July 2016
 * Description : This controller serve the request from mobile application
 */
class Api extends REST_Controller {
    public function __construct() {
        parent::__construct();
        error_reporting(0);
        $this->load->model('Users_model');
       // $this->load->model('Api_service');
    }
    /* public function index_get(){
    	$x = "test data";
    	$this->set_response(array('status' => '1','message' => 'test', 'jsonData'=> $x) , REST_Controller::HTTP_OK);
    } */
	public function index_get()
    {
		$this->set_response(array('status' => 1,'message' => 'success login', 'jsonData'=>array('user'=>"Anjani")) , REST_Controller::HTTP_OK);
	}
	
	public function update_profile_post()
    {
		
		$data = $this->input->post('userData'); //print_r($data);  die();//
		
		// password setting
		if($data['password']){
			$data['password'] = md5($data['password']);
			$data['password_conf'] = md5($data['password_conf']);
		}else{
			unset($data['password']);
		}
		
		$data['haveSmartPhone'] = ($data['haveSmartPhone'])?$data['haveSmartPhone']:0;
		$data['isAllContactCorrect'] = ($data['isAllContactCorrect'])?$data['isAllContactCorrect']:0;
		$data['isTermsAccepted'] = ($data['isTermsAccepted'])?$data['isTermsAccepted']:0;
		
		//print_r($data);
		
		
		unset($data['password_conf']);
		 
		/*if($this->update_profile_validation($data)){	
			//update data
			// $this->load->model('users/users_model');
			$this->load->model('users_model');
			$this->users_model->update_user($data);
			$temp = isset($_SESSION['temp'])?true:false;
			$this->set_response(array('status' => 1, 'message' => 'User profile successflly updated!', 'user_data'=>user_data(), 'temp' => $temp) , REST_Controller::HTTP_OK);
		}else{
			$this->set_response(array('status' => 0, 'message' => 'There is something wrong!') , REST_Controller::HTTP_OK);
		}*/
		$this->load->model('Users_model');
		$userdata = user_data();
		if($userdata['isFirstTimeUser']==1){
			$temp = true;
		}else{
			$temp = false;
		}
		$update = $this->Users_model->update_user($data);
		
		
		$this->set_response(array('status' => 1, 'message' => 'User profile successfully updated!', 'user_data'=>user_data(), 'temp' => $temp) , REST_Controller::HTTP_OK);
	} 
	private function update_profile_validation($data){
		$error = true; 
		if(!trim($data['fname'])){
			$_SESSION['error_firstname'] = 'First name required!';
			$error = false;
		}
		if(!trim($data['lname'])){
			$_SESSION['error_lastname'] = 'Lastname name required!';
			$error = false;
		}
		if(!trim($data['email'])){
			$_SESSION['error_email'] = 'Email required!';
			$error = false;
		}elseif (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
			$_SESSION['error_email'] = 'Invalid email!';
			$error = false;
		}
		if(!trim($data['contact'])){
			$_SESSION['error_contact'] = 'Contact required!';
			$error = false;
		}elseif(!is_numeric($data['contact'])) {
			$_SESSION['error_contact'] = 'Invalid number!';
			$error = false;
		}elseif((strlen($data['password']) > 15)) {
			$_SESSION['error_contact'] = 'Number length should less than 15!';
			$error = false;
		}
		if($data['password'] and (strlen($data['password']) <= 2)){
			$_SESSION['error_password'] = 'Number length should greater than 6!';
			$error = false;
		}elseif($data['password'] != $data['password_conf']) {
			$_SESSION['error_password'] = 'Both password not matching';
			$error = false;
		}		
		if(!$data['isTermsAccepted']){
			$_SESSION['error_isTermsAccepted'] = 'Terms Accepted required!';
			$error = false;
		}
		
		
		if($error){
			$_SESSION['status'] = '<span style="color:Green;">'."User profile successfully updated!".'</span>';
		}else{
			$_SESSION['status'] = '<span style="color:red;">'."There is something wrong!".'</span>';
		}
		
		return $error;
	}
	public function update_notification_post()
    {
		$user_id = $this->session->userdata('user_id');
		
		$data['offersUpdateOnMail'] = ($this->post('offersUpdateOnMail'))?$this->post('offersUpdateOnMail'):0;
		$data['offersUpdateOnPhone'] = ($this->post('offersUpdateOnPhone'))?$this->post('offersUpdateOnPhone'):0;
		$data['loanUpdateOnMail'] = ($this->post('loanUpdateOnMail'))?$this->post('loanUpdateOnMail'):0;
		$data['loanUpdateOnPhone'] = ($this->post('loanUpdateOnPhone'))?$this->post('loanUpdateOnPhone'):0;
		$data['notificationOnMail'] = ($this->post('notificationOnMail'))?$this->post('notificationOnMail'):0;
		$data['notificationOnPhone'] = ($this->post('notificationOnPhone'))?$this->post('notificationOnPhone'):0;
		$data['offersUpdateOnVoice'] = ($this->post('offersUpdateOnVoice'))?$this->post('offersUpdateOnVoice'):0;
		$data['loanUpdateOnVoice'] = ($this->post('loanUpdateOnVoice'))?$this->post('loanUpdateOnVoice'):0;
		$data['notificationOnVoice'] = ($this->post('notificationOnVoice'))?$this->post('notificationOnVoice'):0;
		$data['user_id'] = $user_id;
		$val['isFirstTimeUser'] = $this->post('isFirstTimeUser');
		$userdata = user_data();
		if($userdata['isFirstTimeUser'] == 1){
			$temp = true;
		}else{
			$temp = false;
		}
		$notification_setting = $this->db->where('user_id', $user_id)->get('notification')->first_row('array');
		if(!$notification_setting){
			$this->db->insert('notification', $data);			
		}else{
			$update = $this->db->where('user_id', $user_id)->update('notification', $data);
			$update = $this->db->where('id',$user_id)->update('users',$val);
			if($update)
			{
				$this->session->set_flashdata('message', 'Notification setting successfully saved');
			}
		}
		
		
		//$temp = isset($_SESSION['temp'])?true:false;
		$this->set_response(array('status' => 1, 'message' => 'Notification setting successfully saved!', 'temp'=>$temp) , REST_Controller::HTTP_OK);				
	}
	
	
    public function login_post()
    { 
    	if($_SERVER['REQUEST_METHOD']== "POST"){ 
	
    		$username = $this->post('username');
    		$password = $this->post('password');
    		$temp = ($this->post('temp'))?true:false;
			//set session for temp// 
			if($temp){
				$_SESSION['temp'] = true;
			}
			//print_r(login($username, $password, false, $temp)); die();
			if(login($username, $password, false, $temp)== 1){
				$this->set_response(array('status' => 1, 'message' => 'User logged in successfully!', 'user_data'=>user_data(), 'temp'=>$temp) , REST_Controller::HTTP_OK);
			}
			else if(login($username, $password, false, $temp) == 0)
			{
				if($temp == true){$msg = "or Your account is already activated , Signin with Permanent Username";} else{$msg ='';}
				$this->set_response(array('status' => 0, 'message' => 'Sorry! Username and Password do not match '.' '. $msg) , REST_Controller::HTTP_OK);
			}
			
			else if(login($username, $password, false, $temp) == 4)
			{
				$this->set_response(array('status' => 0, 'message' => 'Your temporary username and password has been expired') , REST_Controller::HTTP_OK);
			}
			
			else {
				$this->set_response(array('status' => 0, 'message' => 'Your account is already activated , Signin with Permanent Username') , REST_Controller::HTTP_OK);
			}
    	}else{
    		$this->set_response(array('status' => 0,'message' => 'This HTTP method is not allowed') , REST_Controller::HTTP_OK);
    	}
    }
    public function profile_update_post()
    { 
    	if($_SERVER['REQUEST_METHOD']== "POST"){ 
    		$username = $this->post('username');
    		$password = $this->post('password');
			
			if(login($username, $password)){
				$this->set_response(array('status' => 1, 'message' => 'User logged in successfully!', 'user_data'=>user_data()) , REST_Controller::HTTP_OK);
			}else{
				$this->set_response(array('status' => 0, 'message' => 'Username and password not matching!') , REST_Controller::HTTP_OK);
			}
    	}else{
    		$this->set_response(array('status' => 0,'message' => 'This HTTP method is not allowed') , REST_Controller::HTTP_OK);
    	}
    }
    
    public function syncloandata_post()
    {
    	
    	//$data['user_id'] = $this->input->post('userid');
    	$data['loanNo'] = $this->input->post('loanNumber');
    	$data['last_name'] = $this->input->post('last_name');
    	$data['agent'] = $this->input->post('agent');
    	$data['processor'] = $this->input->post('processor');
    	$data['processing_manager'] = $this->input->post('processing_manager');
    	$data['current_status'] = $this->input->post('current_status');
    	$data['step_processed'] = $this->input->post('step_processed');
    	$data['loan_canned_msg'] = $this->input->post('loan_canned_msg');
    	//$data['canned_msg'] = $this->input->post('trans_canned_msg');
    	$data['lp_email'] = $this->input->post('lp_Email');
    	$data['lp_phone'] = $this->input->post('lp_Phone');
    	$data['lo_email'] = $this->input->post('lo_email');
    	$data['lo_phone'] = $this->input->post('lo_phone');
    	$data['pm_email'] = $this->input->post('pm_email');
    	$data['pm_phone'] = $this->input->post('pm_phone');
    	$data['loan_amt'] = $this->input->post('loanAmount');
    	$data['purpose'] = $this->input->post('purpose');
    	$data['branch_desc'] = $this->input->post('branchDesc');
    	$data['region'] = $this->input->post('region');
    	//$data['msg_from'] = $this->input->post('msgFrom');
   //	print_r($data);die();
        //unset($data['canned_msg']);
    	$check = $this->Users_model->CheckLoanNo($data['loanNo']);
    	
    	if($check)
    	{
    		$insert = $this->Users_model->updateLoanNo($data);
    	}
    	else 
    	{
    	$insert = $this->Users_model->AddNewLoan($data);
    	}
    	if($insert)
    	{
    		$this->set_response(array('status' => 1, 'message' => $insert) , REST_Controller::HTTP_OK);
    	}
    	
    }
    
    public function Adduser_post()
    {
    	$data['temp_id'] = $this->input->post('temp_id');
    	$data['temp_pwd'] = md5($this->input->post('temp_password'));
    	$data['isFirstTimeUser'] = 1;
    	$data['status'] = 1;
    	
    	$insert = $this->Users_model->AddtempUser($data);
    	
    	if($insert)
    	{
    		$this->set_response(array('status' => 1, 'message' => $insert) , REST_Controller::HTTP_OK);
    	}
    }
    
    public function getLoanStatus_get(){
    	$loanNumber = $this->input->get('loanNo');
    	$getData = $this->Users_model->getLoanStatus($loanNumber);
    	if(sizeof($getData)>0){
    		$this->set_response(array('status' => 1, 'data' => $getData) , REST_Controller::HTTP_OK);
    	}else{
    		$this->set_response(array('status' => 0, 'message' => NULL) , REST_Controller::HTTP_OK);
    	}
    	
    }
    
	 
}
