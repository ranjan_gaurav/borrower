<?php
class Users_model extends CI_Model {
	protected $table = "users";
	public function __construct() {
		parent::__construct ();
		// date_default_timezone_set('Asia/Kolkata');
	}
	public function update_user($data) {
		$user_data = user_data ();
		// $isFirstTimeUser = $user_data['isFirstTimeUser']; //print_r($user_data); die();
		$this->db->where ( 'id', $user_data ['id'] );
		$update = $this->db->update ( 'users', $data );
		if ($update) {
			
			$this->session->set_flashdata ( 'message', 'Your Profile has been Successfully updated' );
		}
		// echo $this->db->last_query(); die();
		return true;
	}
	public function update_notification($data) {
		// echo 123; die();
		$user_data = user_data ();
		// print_r($data); //die();
		$this->db->where ( 'user_id', $user_data ['id'] );
		$this->db->update ( 'notification', $data ); // echo $this->db->last_query(); die();
		return $data;
	}
	public function AddNewLoan($data) {
		try {
			$this->load->library ( 'form_validation' );
			$config = array (
					array (
							'field' => 'loanNumber',
							'label' => 'loanNumber',
							'rules' => 'trim|required' 
					) 
			)
			;
			
			$this->form_validation->set_rules ( $config );
			// $this->form_validation->set_message ( 'is_unique', 'The %s is already taken' );
			
			if ($this->form_validation->run () == false) {
				$errors_array = '';
				foreach ( $config as $row ) {
					$field = $row ['field'];
					$error = strip_tags ( form_error ( $field ) );
					if ($error)
						$errors_array .= $error . ', ';
				}
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => rtrim ( $errors_array, ', ' ) 
				);
			} else {
				// $countVote = $this->checkUserPoll($data['poll_id'], $data['user_id']);
				
				$this->db->set ( 'inserted_on', 'NOW()', FALSE );
				$this->db->insert ( 'canned_master', $data );
				// echo $this->db->last_query(); die();
				$insert_id = $this->db->insert_id ();
				
				if ($insert_id) {
					// $array['user_id_fk'] = array($arr);
					
					$json = json_decode ( $this->input->post ( 'trans_canned_msg' ) ); // print_r($json); die();
					if (count ( $json ) > 0) {
						$c = 0;
						foreach ( $json as $val ) {
							$data_to_add = array (
									
									// 'loan_m_id' => $insert_id,
									'loanNo' => $data ['loanNo'],
									// 'canned_msg' => $data['loan_canned_msg'],
									'step_processed' => $data ['step_processed'],
									'msg_from' => $this->input->post ( 'msgFrom' ),
									'canned_msg' => $val 
							);
							
							$this->db->set ( 'inserted_on', 'NOW()', FALSE );
							$insert = $this->db->insert ( 'canned_transection', $data_to_add );
							$c ++;
						}
					}
					/*
					 * $data_to_add = array (
					 *
					 * //'loan_m_id' => $insert_id,
					 * 'loanNo' => $data['loanNo'],
					 * //'canned_msg' => $data['loan_canned_msg'],
					 * 'step_processed' => $data['step_processed'],
					 * 'msg_from' => $this->input->post('msgFrom'),
					 * 'canned_msg' => $json
					 * );
					 */
					
					// $this->db->set ( 'inserted_on', 'NOW()', FALSE );
					// $insert = $this->db->insert ( 'canned_transection', $data_to_add );
					// print_r($json); die();
					
					// for($i = 0 ; )
					
					$message = array (
							'status' => true,
							'response_code' => '1',
							'message' => "Data Successfully Added" 
					);
				}
			}
		} 

		catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
		}
		
		return $message;
	}
	public function CheckLoanNo($LoanNo) {
		$this->db->where ( 'loanNo', $LoanNo );
		$res = $this->db->get ( 'canned_master' );
		$no = $res->num_rows ();
		if ($no == 1) {
			return true;
		}
	}
	public function updateLoanNo($data) {
		try {
			$this->load->library ( 'form_validation' );
			$config = array (
					array (
							'field' => 'loanNumber',
							'label' => 'loanNumber',
							'rules' => 'trim|required' 
					) 
			)
			;
			
			$this->form_validation->set_rules ( $config );
			// $this->form_validation->set_message ( 'is_unique', 'The %s is already taken' );
			
			if ($this->form_validation->run () == false) {
				$errors_array = '';
				foreach ( $config as $row ) {
					$field = $row ['field'];
					$error = strip_tags ( form_error ( $field ) );
					if ($error)
						$errors_array .= $error . ', ';
				}
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => rtrim ( $errors_array, ', ' ) 
				);
			} else {
				// $countVote = $this->checkUserPoll($data['poll_id'], $data['user_id']);
				
				$this->db->where ( 'loanNo', $data ['loanNo'] );
				$update = $this->db->update ( 'canned_master', $data );
				
				if ($update) {
					// $array['user_id_fk'] = array($arr);
					$loanMNo = $this->GetLoanID ( $data ['loanNo'] );
					
					$json = json_decode ( $this->input->post ( 'trans_canned_msg' ) ); // print_r($json); die();
					if (count ( $json ) > 0) {
						$c = 0;
						foreach ( $json as $val ) {
							$data_to_add = array (
									
									// 'loan_m_id' => $insert_id,
									'loanNo' => $data ['loanNo'],
									// 'canned_msg' => $data['loan_canned_msg'],
									'step_processed' => $data ['step_processed'],
									'msg_from' => $this->input->post ( 'msgFrom' ),
									'canned_msg' => $val 
							);
							
							$this->db->set ( 'inserted_on', 'NOW()', FALSE );
							$insert = $this->db->insert ( 'canned_transection', $data_to_add );
							$c ++;
						}
					}
					
					$message = array (
							'status' => true,
							'response_code' => '1',
							'message' => "Data Successfully Updated" 
					);
					/*
					 * $data_to_add = array (
					 *
					 * //'loan_m_id' => $loanMNo,
					 * 'loanNo' => $data['loanNo'],
					 * //'canned_msg' => $data['loan_canned_msg'],
					 * 'step_processed' => $data['step_processed'],
					 * 'msg_from' =>$this->input->post('msgFrom')
					 * );
					 * $this->db->set ( 'inserted_on', 'NOW()', FALSE );
					 * $insert = $this->db->insert ( 'canned_transection', $data_to_add );
					 *
					 * if($insert){
					 *
					 *
					 * }
					 */
				}
			}
		} 

		catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
		}
		
		return $message;
	}
	public function GetLoanID($loanNumber) {
		$this->db->where ( 'loanNo', $loanNumber );
		$res = $this->db->get ( 'canned_master' );
		$no = $res->row ()->id;
		return $no;
		// print_r($no); die();
	}
	public function AddtempUser($data) {
		try {
			$this->load->library ( 'form_validation' );
			$config = array (
					array (
							'field' => 'temp_id',
							'label' => 'temp_id',
							'rules' => 'trim|required|is_unique[users.temp_id]' 
					),
					array (
							'field' => 'temp_password',
							'label' => 'temp_password',
							'rules' => 'trim|required' 
					) 
			)
			;
			
			$this->form_validation->set_rules ( $config );
			$this->form_validation->set_message ( 'is_unique', 'The %s is already taken' );
			
			if ($this->form_validation->run () == false) {
				$errors_array = '';
				foreach ( $config as $row ) {
					$field = $row ['field'];
					$error = strip_tags ( form_error ( $field ) );
					if ($error)
						$errors_array .= $error . ', ';
				}
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => rtrim ( $errors_array, ', ' ) 
				);
			} else {
				// $countVote = $this->checkUserPoll($data['poll_id'], $data['user_id']);
				
				// $this->db->set ( 'inserted_on', 'NOW()', FALSE );
				$insert = $this->db->insert ( 'users', $data );
				
				if ($insert) {
					// $array['user_id_fk'] = array($arr);
					
					$message = array (
							'status' => true,
							'response_code' => '1',
							'message' => "User Successfully Added" 
					);
				}
			}
		} 

		catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
		}
		
		return $message;
	}
	public function getLoanStatus($loanNo) {
		$loan = array ();
		$query = "SELECT * FROM canned_master cm WHERE cm.loanNo=" . $this->db->escape ( $loanNo );
		$result = $this->db->query ( $query );
		if ($result->num_rows () > 0) {
			$data = $result->result () [0];
			$loan ['loanNo'] = $data->loanNo;
			$loan ['last_name'] = $data->last_name;
			$loan ['borrower_email'] = $data->borrower_email;
			$loan ['borrower_phone'] = $data->borrower_phone;
			$loan ['agent'] = $data->agent;
			$loan ['lo_email'] = $data->lo_email;
			$loan ['lo_phone'] = $data->lo_phone;
			$loan ['processor'] = $data->processor;
			
			$loan ['lp_email'] = $data->lp_email;
			$loan ['lp_phone'] = $data->lp_phone;
			$loan ['processing_manager'] = $data->processing_manager;
			$loan ['pm_email'] = $data->pm_email;
			$loan ['pm_phone'] = $data->pm_phone;
			$loan ['current_status'] = $data->current_status;
			$loan ['loan_canned_msg'] = $data->loan_canned_msg;
			$loan ['loan_amt'] = $data->loan_amt;
			
			$loan ['step_processed'] = $data->step_processed;
			$loan ['purpose'] = $data->purpose;
			$loan ['branch_desc'] = $data->branch_desc;
			$loan ['region'] = $data->region;
			$loan ['inserted_on'] = $data->inserted_on;
			$loan ['trans'] = $this->getTransactionData ( $loanNo );
		}
		return $loan;
	}
	public function getTransactionData($loanNo) {
		$response = array ();
		$loan = array ();
		$query = "SELECT * FROM canned_transection ct WHERE ct.loanNo=" . $this->db->escape ( $loanNo )." AND is_uploaded=0";
		$result = $this->db->query ( $query );
		return $result->result ();
	}
	
	
}




