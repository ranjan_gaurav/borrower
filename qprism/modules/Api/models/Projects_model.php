<?php

class Projects_model extends CI_Model {

    protected $projectTable = "projects";

    function __construct() {
        parent::__construct();
        //date_default_timezone_set('Asia/Kolkata');
    }
	
	public function get_validation_rules(){
		return array(
				array(
						'field' => 'project_id',
						'label' => 'Project ID',
						'rules' => 'required|numeric'
				),
				array(
						'field' => 'project_name',
						'label' => 'Project Name',
						'rules' => 'required'
				),
				array(
						'field' => 'ro_id',
						'label' => 'R.O.',
						'rules' => 'required|numeric'
				),
				array(
						'field' => 'piu_id',
						'label' => 'P.I.U.',
						'rules' => 'required|numeric'
				),
				array(
						'field' => 'state_id',
						'label' => 'State22',
						'rules' => 'numeric'
				),
		);		
	}
	
	public  function assignment_validation_rules(){
		return array(
				array(
						'field' => 'project_id',
						'label' => 'Project ID',
						'rules' => 'required|numeric'
				),
				array(
						'field' => 'state_id',
						'label' => 'State',
						'rules' => 'required|numeric'
				),
				array(
						'field' => 'kpi_id',
						'label' => 'K.P.I.',
						'rules' => 'required|numeric'
				),
				array(
						'field' => 'ro_id',
						'label' => 'R.O.',
						'rules' => 'required|numeric'
				),
				array(
						'field' => 'piu_id',
						'label' => 'P.I.U.',
						'rules' => 'required|numeric'
				),
				array(
						'field' => 'project_id',
						'label' => 'Project',
						'rules' => 'required|numeric'
				),
				array(
						'field' => 'project_value',
						'label' => 'Project Target',
						'rules' => 'required|numeric'
				)
		);		
	}	
	
	public  function achievement_validation_rules(){
		return array(
				array(
						'field' => 'project_id',
						'label' => 'Project ID',
						'rules' => 'required|numeric'
				),
				array(
						'field' => 'kpi_id',
						'label' => 'K.P.I.',
						'rules' => 'required|numeric'
				),
				array(
						'field' => 'ro_id',
						'label' => 'R.O.',
						'rules' => 'required|numeric'
				),
				array(
						'field' => 'piu_id',
						'label' => 'P.I.U.',
						'rules' => 'required|numeric'
				),
				array(
						'field' => 'achive_value',
						'label' => 'Achive Value',
						'rules' => 'required|numeric'
				),
				array(
						'field' => 'startdate',
						'label' => 'Start Date',
						'rules' => 'required'
				),
				array(
						'field' => 'enddate',
						'label' => 'End Date',
						'rules' => 'required'
				)
		);		
	}
	
	
	
	public function all_pro_count() {
		return $this->db->get('projects')->num_rows();
	}
	public function all_projects($limit, $offset) {
		$this->db->select('p.*, ro.new_ro_name, piu.piu_name as new_piu_name');
		$this->db->from('projects as p');
		$this->db->join('ro_lists as ro', 'ro.new_ro_id = p.ro_id', 'left');
		$this->db->join('piu_list as piu', 'piu.piu_id = p.piu_id', 'left');
		$this->db->order_by('p.pro_id','desc');
		$res = $this->db->limit($limit, $offset)->get()->result();
		// prd($res);
		return $res;
	}	
	
	public function all_filter_pro_count($ro_id, $piu_id){
		if($ro_id){
			$this->db->where(array('p.ro_id' =>$ro_id));
		}
		if($piu_id){
			$this->db->where(array('p.piu_id'=> $piu_id))	;
		}		
		return $this->db->get('projects as p')->num_rows();
	}

	public function projects_by_ro_piu($ro_id, $piu_id, $limit, $offset){
		$this->db->select('p.*, ro.new_ro_name, piu.piu_name as new_piu_name');
		$this->db->from('projects as p');
		$this->db->join('ro_lists as ro', 'ro.new_ro_id = p.ro_id', 'left');
		$this->db->join('piu_list as piu', 'piu.piu_id = p.piu_id', 'left');
		if($ro_id){
			$this->db->where(array('p.ro_id' =>$ro_id));
		}
		if($piu_id){
			$this->db->where(array('p.piu_id'=> $piu_id))	;
		}		
		return $this->db->limit($limit, $offset)->get()->result();
	}	
	
	public function project_byid($id){
		$this->db->select('p.*, ro.new_ro_name, piu.piu_name as new_piu_name');
		$this->db->from('projects as p');
		$this->db->join('ro_lists as ro', ' p.ro_id = ro.new_ro_id', 'left');
		$this->db->join('piu_list as piu', 'piu.piu_id = p.piu_id', 'left');		
		$this->db->where('p.pro_id=', $id);	
		return $this->db->get()->first_row('array');
	}

	
	public function create_project() {
		$this->db->insert('projects', $_POST);
	    return $this->db->insert_id();		
	}	
	
	public function project_update($id){
		return $this->db->where('pro_id=',$id)->update('projects', $_POST); 
	}
	public function project_delete($id){
		return $this->db->where('pro_id=',$id)->delete('projects'); 
	}
	
	public function byuser_pro_count($user_id = false, $project_id = false,  $ro_id = false, $piu_id = false){
		if($user_id){
			$this->db->where(array('p.user_id' =>$user_id));
		}		
		if($project_id){
			$this->db->where(array('p.project_id' =>$project_id));
		}		
		if($kpi_id){
			$this->db->where(array('p.kpi_id' =>$kpi_id));
		}		
		if($ro_id){
			$this->db->where(array('p.ro_id' =>$ro_id));
		}
		if($piu_id){
			$this->db->where(array('p.piu_id'=> $piu_id));
		}
	
		return $this->db->get('project_assigned as p')->num_rows();
	}
	
	public function all_project_byuser($limit, $offset, $user_id = false, $project_id = false, $kpi_id = false, $ro_id = false, $piu_id = false){
		$this->db->select('*');
		$this->db->from('project_assigned as pa');
		if($user_id){
			$this->db->where('pa.user_id = ', $user_id);
		}
		if($project_id){
			$this->db->where('pa.project_id = ', $project_id);
		}		
		if($kpi_id){
			$this->db->where(array('pa.kpi_id' =>$kpi_id));
		}
		if($ro_id){
			$this->db->where('pa.ro_id = ', $ro_id);
		}	
		if($piu_id){
			$this->db->where('pa.piu_id = ', $piu_id);
		}		
		$res = $this->db->order_by('pa.id', 'desc')->limit($limit, $offset)->get()->result();
		return $res;
	}
	
	public function project_assigned_byid($id){
		$this->db->select('pa.id, pa.user_id, p.project_id, pa.kpi_id,  p.project_name, pa.project_value, ro.new_ro_id as ro_id,ro.new_ro_name as ro_name, piu.piu_id, piu.piu_name, u.username, u.user_email, s.state_id');
		$this->db->from('users as u');
		$this->db->join('project_assigned as pa', 'u.id = pa.user_id', 'left');
		$this->db->join('projects as p', 'p.project_id = pa.project_id', 'left');
		$this->db->join('ro_lists as ro', 'ro.new_ro_id = pa.ro_id', 'left');
		$this->db->join('piu_list as piu', 'piu.piu_id = pa.piu_id', 'left');
		$this->db->join('states as s', 's.state_id = pa.state_id', 'left');
		$this->db->where('pa.id = ', $id);	
		$this->db->group_by('pa.id');
		$res = $this->db->get()->first_row('array');
		return $res;
	}
	
	public function project_assignment_update($id){
		$data = $_POST;		
		$this->db->where('project_id=',$data['project_id'])->update('projects',array('state_id'=>$data['state_id'])); 
		return $this->db->where('id',$id)->update('project_assigned', $data); 
	}	
	
	public function assign_project_insert(){
		$this->db->insert('project_assigned', $_POST);
	    return $this->db->insert_id();	
	}
	
	public function piu_options($ro_id) {		
		$result =  $this->db->select('piu_id, piu_name')->from('piu_list')->where("ro_id = ", $ro_id)->group_by('piu_id')->get()->result_array();	
		$options = array_column($result, 'piu_id', 'piu_name');
		$html = '<option value="">Select P.I.U.</option>';
		foreach($options as $key => $value){
			$html .= '<option value="'.$value.'">'.$key.'</option>';
		}
		return $html;
	}
	
	
	public function project_achievement_count($project_id = false, $month = false, $kpi_id = false, $ro_id = false, $piu_id = false){
	
		if($project_id){
			$this->db->where(array('project_id' =>$project_id));
		}	
		if($month){
			$this->db->where("Month(startdate) = $month");
		}		
		if($kpi_id){
			$this->db->where(array('kpi_id' =>$kpi_id));
		}		
		if($ro_id){
			$this->db->where(array('ro_id' =>$ro_id));
		}
		if($piu_id){
			$this->db->where(array('piu_id'=> $piu_id));
		}
		return $this->db->get('project_achievements')->num_rows();
	}
	
	public function all_project_achievement($limit, $offset, $project_id = false, $month = false, $kpi_id = false, $ro_id = false, $piu_id = false) {
		if($project_id){
			$this->db->where('project_id = ', $project_id);
		}	
		if($month){
			$this->db->where("Month(startdate) = $month");
		}		
		if($kpi_id){
			$this->db->where(array('kpi_id' =>$kpi_id));
		}
		if($ro_id){
			$this->db->where('ro_id = ', $ro_id);
		}	
		if($piu_id){
			$this->db->where('piu_id = ', $piu_id);
		}	
		return $this->db->order_by('achievement_id','desc')->limit($limit, $offset)->get('project_achievements')->result();
	}	
	
	public function insert_project_achievement() {
		$data = $_POST;
		$data['startdate']= date('Y-m-d', strtotime($data['startdate']));
		$data['enddate']= date('Y-m-d', strtotime($data['enddate']));
		$this->db->insert('project_achievements', $data);
	    return $this->db->insert_id();		
	}	
	
	public function project_achievement_byid($id){
		$this->db->where('achievement_id = ', $id);	
		$this->db->group_by('achievement_id', 'desc');
		$res = $this->db->get('project_achievements')->first_row('array');
		return $res;
	}	
	
	public function update_project_achievement($id){
		$data = $_POST;
		$data['startdate']= date('Y-m-d', strtotime($data['startdate']));
		$data['enddate']= date('Y-m-d', strtotime($data['enddate']));
		$this->db->where('achievement_id = ', $id);	
		return $this->db->update('project_achievements', $data); 
	}
	
	
	public function user_project_achievement_count($user_id = false, $project_id = false,  $ro_id = false, $piu_id = false) {
		if($user_id){
			$this->db->where(array('pas.user_id' =>$user_id));
		}		
		if($project_id){
			$this->db->where(array('pc.project_id' =>$project_id));
		}		
		if($kpi_id){
			$this->db->where(array('pc.kpi_id' =>$kpi_id));
		}		
		if($ro_id){
			$this->db->where(array('pc.ro_id' =>$ro_id));
		}
		if($piu_id){
			$this->db->where(array('pc.piu_id'=> $piu_id));
		}
	
		$this->db->join('project_assigned as pas', "pas.project_id = pc.project_id", 'right');	
		return $this->db->get('project_achievements as pc')->num_rows();
	}
	
	
	public function all_user_project_achievement($limit, $offset, $user_id = false, $project_id = false, $kpi_id = false, $ro_id = false, $piu_id = false)  {

		if($user_id){
			$this->db->where('pas.user_id = ', $user_id);
		}
		if($project_id){
			$this->db->where('pc.project_id = ', $project_id);
		}		
		if($kpi_id){
			$this->db->where(array('pc.kpi_id' =>$kpi_id));
		}
		if($ro_id){
			$this->db->where('pc.ro_id = ', $ro_id);
		}	
		if($piu_id){
			$this->db->where('pc.piu_id = ', $piu_id);
		}	
		$this->db->select('pc.*, pas.user_id')->join('project_assigned as pas', "pas.project_id = pc.project_id", 'right');
		return $this->db->order_by('achievement_id','desc')->limit($limit, $offset)->get('project_achievements as pc')->result();
	}	
	
	public function project_achievement_delete($id){
		return $this->db->where('achievement_id=',$id)->delete('project_achievements'); 
	}
	public function getListSubmittedTarget($filters = array())
	{
		$this->db->select('pda.*, u.username,u.user_type');
		$this->db->from('project_data_approval as pda');
		$this->db->join('users as u', 'u.id = pda.sender_id', 'right');
		$this->db->where('pda.data_type',$filters['data_type']);
		if(isset($filters['user_id'])):
		$this->db->where('pda.sender_id',$filters['user_id']);
		endif;
		$this->db->limit($filters['limit'], $filters['offset']);
		$query = $this->db->get();
		//echo $this->db->last_query(); 
		return $query->result();
		
	}
	
	public function insert_project_summary($data) {
		$this->db->set($data);
		$this->db->insert('project_summary', $data);
	    return $this->db->insert_id();		
	}
	public function project_summary_update($user_id, $summary_files) {
		return $this->db->where('user_id=',$user_id)->update('project_summary', array('summary_files'=>$summary_files)); 		
	}	
	public function count_project_summary_list($user_id){
		if($user_id){
			$this->db->where('user_id = ', $user_id);
		}
		return $this->db->get('project_summary')->num_rows();
	}
	public function project_summary_list($limit, $offset, $user_id =false) {
		if($user_id){
			$this->db->where('user_id = ', $user_id);
		}
	    return $this->db->limit($limit, $offset)->get('project_summary')->result();	
	}	

	public function project_summary_delete($user_id) {
		return $this->db->where('user_id=',$user_id)->delete('project_summary'); 
	}	

}


