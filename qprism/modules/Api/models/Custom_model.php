<?php

/*

 */

class Custom_model extends CI_Model {

    var $projectTable = "projects";
    var $userTable = "users";
    var $fieldTable = "field_users";
    var $hqTable = "hq_users";
    var $projectAssigned = "project_assigned";
    var $kpiAssigned = "kpi_assigned";
    var $projectMonthlyTarget = 'project_monthly_target';
    var $states = "states";
    var $tempTargetTable = "temp_project_assigned";
    var $tempKpiAssignTable = "temp_kpi_assigned";
    var $notification = "activity_notifications";
    var $approval_notification = "project_data_approval";

    function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Kolkata');
    }

    function projectList($data = array()) {
        //echo "<pre>"; print_r($data); die;
        $sql = "select users.username, projects.pro_id,projects.project_id,projects.project_name,projects.ro_id,projects.ro_name,projects.piu_id,projects.piu_name,project_assigned.user_id ,project_assigned.project_value,project_assigned.user_id from projects inner join project_assigned on projects.project_id=project_assigned.project_id join users on users.id = project_assigned.user_id";

        if (isset($data['ro_id']) && ($data['ro_id'] != 0)) {
            $sql.= " where projects.ro_id=" . $data['ro_id'];
        }
        if (isset($data['piu_id']) && !isset($data['ro_id'])) {
            $sql.= " where projects.piu_id=" . $data['piu_id'];
        }
        if (isset($data['ro_id']) && (($data['ro_id'] != 0)) && isset($data['piu_id'])) {
            $sql.= " where projects.ro_id=" . $data['ro_id'] . "AND projects.piu_id=" . $data['piu_id'];
        }
        if (isset($data['user_id']) && ($data['user_id'] != 0)) {
            $sql.= " where project_assigned.user_id=" . $data['user_id'];
        }
        if (isset($data['project_id']) && $data['project_id'] != 0) {
            $sql.= " AND project_assigned.project_id=" . $data['project_id'];
        }
        $sql.= " group by project_assigned.project_id";


        $user_data = $this->db->query($sql)->result();
        return $user_data;
    }

    function allProjects($ro_id, $piu_id = null) {
        //$this->db->select(array('project_id','project_name'));
        if (isset($ro_id) && ($ro_id != 0)) {
            $this->db->where('ro_id', $ro_id);
        }
        if (isset($piu_id) && empty($ro_id)) {
            $this->db->where('piu_id', $piu_id);
        }
        $query = $this->db->get($this->projectTable);
        //echo $this->db->last_query(); die;
        return $query->result();
    }

    function roList() {
        $this->db->select(array('ro_id', 'ro_name'))
                ->group_by('ro_id')
                ->order_by('ro_name', 'ASC');
        $query = $this->db->get($this->projectTable);
        return $query->result();

        // $sql = "select ro_id, ro_name from projects group by ro_id order by ro_name ASC";
        // $user_data = $this->db->query($sql)->result();
        // return $user_data;
    }

    function piuList() {

        $sql = "select piu_id, piu_name from projects where piu_name != '' group by piu_id order by piu_name ASC";
        $user_data = $this->db->query($sql)->result();
        return $user_data;
    }

    function get($id) {
        $this->db->where('pro_id', $id);
        $this->db->from($this->projectTable);
        $result = $this->db->get();
        return $result->row();
    }

    function update($id) {
        $data = $this->input->post();
        $this->db->where('pro_id', $id);
        $ro_list = $this->roList();
        $piu = $this->piuList();
        if ($ro_list[0]->ro_id == $data['ro_id']) {
            $data['ro_name'] = $ro_list[0]->ro_name;
        }
        //echo "<pre>"; print_r($piu); die;
        $query = $this->db->update($this->projectTable, $data);
        if ($query) {

            // $this->updateProjectAssignTable($data);

            return true;
        } else {
            return false;
        }
    }

    function getUsers() {
        $this->db->select(array('u.id', 'u.username'));
        $this->db->from($this->userTable . ' u');
        $this->db->join($this->fieldTable . ' f', 'f.user_id=u.id', 'left');
        $this->db->join($this->hqTable . ' h', 'h.user_id=u.id', 'left');
        $query = $this->db->get();
        //echo $this->db->last_query(); die;
        return $query->result();
    }

    function getResponsibility($kpi_id, $user_id) {
        $this->db->where('user_id', $user_id);
        $this->db->where('kpi_id', $kpi_id);
        $this->db->where("MONTH(modified_date) = MONTH(CURDATE())");
        $query = $this->db->get('kpi_special_responsibilities');
        if ($query->num_rows() === 1) {
            $data['data'] = $query->result_array();
            $data['status'] = 1;
        } else {
            $data['data'] = '';
            $data['status'] = 0;
        }
        return $data;
    }

    function getManualAchievement($user_id) {
        $data = array();
        $this->db->select(array('sum(achive_value) as total', 'kpi_id'));
        $this->db->from('kpi_special_responsibilities');
        $this->db->where('user_id', $user_id);
        $this->db->where('status', 1);
        $this->db->where("YEAR(modified_date) = YEAR(CURDATE())");
        $this->db->group_by('kpi_id');
        $query = $this->db->get();
        foreach ($query->result_array() as $item) {
            $data[$item['kpi_id']] = $item['total'];
        }
        return $data;
    }

    function updateResponsibilities($data, $type) {
        $values = array();

        $this->db->where('kpi_id', $data['kpi_id']);
        $this->db->where('user_id', $data['user_id']);
        $values['by_reporting_achive'] = $data['achive_value'];
        $values['by_reporting_description'] = $data['description'];
        $values['reporting_approval_time'] = $data['modified_date'];
        $values['by_reviewing_achive'] = $data['achive_value'];
        $values['by_reviewing_description'] = $data['description'];
        $values['reviewing_approval_time'] = $data['modified_date'];
        if ($type == 0) {
            $this->db->update('kpi_special_responsibilities', $data);
        } elseif ($type == 1) {
            $this->db->update('kpi_special_responsibilities', $values);
            $this->db->insert($this->notification, array('type' => 'edit_reporting_responsibility', 'reciever_id' => $data['user_id'], 'sender_id' => $this->session->userdata('userid'), 'resource_id' => $data['user_id']));
        } else {

            $this->db->update('kpi_special_responsibilities', array('by_reviewing_achive' => $data['achive_value'], 'by_reviewing_description' => $data['description'], 'reviewing_approval_time' => $data['modified_date']));
        }
    }

    function updateStatus($values, $id, $user_id, $type) {

        $reporting = $this->getReportingOfficerId($user_id);
        $reviewing = $this->getReportingOfficerId($reporting);
        if ($type == 1) {
            $this->db->insert($this->notification, array('type' => 'self_approve_responsibility', 'reciever_id' => $this->session->userdata('userid'), 'sender_id' => $this->session->userdata('userid'), 'resource_id' => $user_id));
            $this->db->insert($this->notification, array('type' => 'reporting_approve_responsibility', 'reciever_id' => $user_id, 'sender_id' => $this->session->userdata('userid'), 'resource_id' => $user_id));
            $this->db->insert($this->notification, array('type' => 'final_approve_responsibility', 'reciever_id' => $reviewing, 'sender_id' => $this->session->userdata('userid'), 'resource_id' => $user_id));
        } elseif ($type == 2) {
            $this->db->insert($this->notification, array('type' => 'self_approve_responsibility', 'reciever_id' => $this->session->userdata('userid'), 'sender_id' => $this->session->userdata('userid'), 'resource_id' => $user_id));
            $this->db->insert($this->notification, array('type' => 'final_approve_responsibility', 'reciever_id' => $user_id, 'sender_id' => $this->session->userdata('userid'), 'resource_id' => $user_id));
            $this->db->insert($this->notification, array('type' => 'final_approve_responsibility', 'reciever_id' => $reporting, 'sender_id' => $this->session->userdata('userid'), 'resource_id' => $user_id));
            $sql = "UPDATE `kpi_special_responsibilities` SET `status` = 1 , `achive_value` = `by_reviewing_achive` , `description` = `by_reviewing_description` where id = " . $id;
            $this->db->update('kpi_special_responsibilities', array('status' => 1, 'achive_value' => 'by_reviewing_achive', 'description' => 'by_reviewing_description'), array('id', $id));
        } else {
            $this->db->where('id', $id);
            $values['modified_date'] = date('Y-m-d H:i:s');
            $this->db->update('kpi_special_responsibilities', $values);
        }
    }

    function getReportingOfficerId($user_id) {
        $this->db->select('reporting_officer_id');
        $this->db->where('id', $user_id);
        $query = $this->db->get($this->userTable);
        $data = $query->result_array();
        if ($query->num_rows() === 1) {
            return $data[0]['reporting_officer_id'];
        } else {
            return '0';
        }
    }

    function getAssignProjectByPIU($userId, $piu_id) {
        $sql = "select projects.project_id,projects.project_name,"
                . "project_assigned.user_id ,project_assigned.project_value,"
                . "project_assigned.user_id from projects inner join project_assigned "
                . "on projects.project_id=project_assigned.project_id where "
                . "user_id=" . $userId . " AND project_assigned.piu_id=" . $piu_id . " "
                . "group by project_assigned.project_id";
        $projectsList = $this->db->query($sql)->result();
        return $projectsList;
    }

    function getWeightageByKpi($userId, $kpi_id) {
        $sql = "select kpi_achievements.kpi_id,kpi_achievements.kpi_type,kpi_achievements.captured_in_pmis ,kpi_achievements.kpi_unit as unit,(select kpi_assigned.value from kpi_assigned where kpi_assigned.user_id=" . $userId . " And kpi_assigned.kpi_id= kpi_achievements.kpi_id) as value from kpi_achievements where kpi_achievements.kpi_id =" . $kpi_id;
        // echo $sql;      
        return $this->db->query($sql)->result_array();
    }

    function getKpiList() {
        $this->db->select(array('kpi_id', 'captured_in_pmis'));
        $this->db->where('kpi_id < 38');
        // $this->db->where('kpi_type = 1');
        $query = $this->db->get('kpi_achievements');
        //echo $this->db->last_query();die;
        return $query->result();
    }

    function getPiuByRo($ro_id) {
        $this->db->select(array('piu_id', 'piu_name'));
        $this->db->where('ro_id', $ro_id)
                ->group_by('piu_id');
        $query = $this->db->get('projects');
        return $query->result();
    }

    function getCustomTargets() {
        $newdata = array();
        $arr1 = array('kpi_type' => 1, 'kpi_id' => 9, 'captured_in_pmis' => 'Total land acquired (3G)', 'unit' => 'Hectares',
            'value' => 15, 'total_value' => 267);
        $arr2 = array('kpi_type' => 1, 'kpi_id' => 10, 'captured_in_pmis' => 'Total number of tree clearance proposals approved',
            'unit' => 'Number', 'value' => 10, 'total_value' => 6);
        $arr3 = array('kpi_type' => 1, 'kpi_id' => 11, 'captured_in_pmis' => 'Total No. of forest clearance proposals approved', 'unit' => 'Number', 'value' => 10, 'total_value' => 3);
        $arr4 = array('kpi_type' => 1, 'kpi_id' => 15, 'captured_in_pmis' => 'Total No. of utility estimates approved', 'unit' => 'Number', 'value' => 10, 'total_value' => 45);
        $arr5 = array('kpi_type' => 1, 'kpi_id' => 18, 'captured_in_pmis' => 'Total length completed', 'unit' => 'Kms', 'value' => 20, 'total_value' => 65.78);
        $arr6 = array('kpi_type' => 1, 'kpi_id' => 22, 'captured_in_pmis' => 'No. of CODs achieved', 'unit' => 'Number', 'value' => 15, 'total_value' => 2);
        $arr7 = array('kpi_type' => 2, 'kpi_id' => 24, 'captured_in_pmis' => 'No. of contracts awarded for O&M and periodic maintenance', 'unit' => 'Number', 'value' => 10, 'total_value' => 2);
        $arr8 = array('kpi_type' => 1, 'kpi_id' => 27, 'captured_in_pmis' => 'Total length of kms tolled', 'unit' => 'Kms', 'value' => 10, 'total_value' => 37.2);

        $newdata[] = (object) $arr1;
        $newdata[] = (object) $arr2;
        $newdata[] = (object) $arr3;
        $newdata[] = (object) $arr4;
        $newdata[] = (object) $arr5;
        $newdata[] = (object) $arr6;
        $newdata[] = (object) $arr7;
        $newdata[] = (object) $arr8;
        return $newdata;
    }

    function getCustomAchivementRatio() {
        $array = array('9' => '120', '10' => '4', '11' => '1', '15' => '26', '18' => '48', '22' => '1', '24' => '1', '27' => '0');
        return $array;
    }

    function getProjectsByPiu($user_id, $piu_id, $ro_id, $kpi_id) {
        $sql = "select project_id,project_name,(select project_assigned.project_value from project_assigned where project_assigned.project_id = projects.project_id AND user_id =" . $user_id . " AND ro_id=" . $ro_id . " AND piu_id =" . $piu_id . " AND kpi_id =" . $kpi_id . ") as total_target from projects where piu_id=" . $piu_id . " AND ro_id=" . $ro_id . " group by projects.project_id";
        $projectsList = $this->db->query($sql)->result();
        //echo "<pre>"; print_r($projectsList); die;
        return $projectsList;
    }

    function projectKpiList($userId) {
        $sql = "select kpi_achievements.captured_in_pmis ,"
                . "temp_kpi_assigned.unit,temp_kpi_assigned.value,temp_kpi_assigned.kpi_id "
                . "from kpi_achievements inner join temp_kpi_assigned on "
                . "temp_kpi_assigned.kpi_id=kpi_achievements.kpi_id where "
                . "temp_kpi_assigned.user_id=" . $userId . ' order by kpi_achievements.kpi_id ASC';

        $user_data = $this->db->query($sql)->result_array();
        return $user_data;
    }

    function updateKpiData($data) {
        $status = $this->getTempKpiAssign($data);
        if ($status == true) {
            $this->updateTempKpiAssign($data);
        } else {
            $this->db->insert($this->tempKpiAssignTable, $data);
        }
        return true;
    }

    function updateTempKpiAssign($values) {
        $this->db->where('kpi_id', $data['kpi_id']);
        $this->db->where('user_id', $data['user_id']);
        $this->db->update($this->tempKpiAssignTable, $values);
    }

    function getTempKpiAssign($data) {
        //$data = $data[0];
        $this->db->where('kpi_id', $data['kpi_id']);
        $this->db->where('user_id', $data['user_id']);
        $query = $this->db->get($this->tempKpiAssignTable);
        $data = $query->result();
        if ($query->num_rows() === 1) {
            return true;
        } else {
            return false;
        }
    }

    function addTempTarget($data) {
        $status = $this->getTempTarget($data);
        if ($status == true) {
            $this->updateTempTarget($data);
        } else {
            $this->db->insert($this->tempTargetTable, $data);
        }
        return true;
    }

    function updateTempTarget($values, $id) {
        $this->db->where('project_id', $data['project_id']);
        $this->db->where('ro_id', $data['ro_id']);
        $this->db->where('piu_id', $data['piu_id']);
        $this->db->where('kpi_id', $data['kpi_id']);
        $values['modified_date'] = date('Y-m-d H:i:s');
        $this->db->update($this->tempTargetTable, $values);
    }

    function getTempTarget($data) {
        //$data = $data[0];
        $this->db->where('project_id', $data['project_id']);
        $this->db->where('ro_id', $data['ro_id']);
        $this->db->where('piu_id', $data['piu_id']);
        $this->db->where('kpi_id', $data['kpi_id']);
        $this->db->where('user_id', $data['user_id']);
        $query = $this->db->get($this->tempTargetTable);
        $data = $query->result();
        if ($query->num_rows() === 1) {
            return true;
        } else {
            return false;
        }
    }

    function getNewTargetData($user_id,$data_id = null) {
        $this->db->where('user_id', $user_id);
        $this->db->where('status', 0);
		if(isset($data_id))
		{
			$this->db->where('data_id', $data_id); 
		}
        $query = $this->db->get($this->tempTargetTable);
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
            return $data;
        } else {
            return false;
        }
    }

    function getNewKpiData($user_id) {
        $data = array();
        $this->db->select(array('kpa.kpi_id', 'kpa.user_id', 'kpa.status', 'kpa.unit', 'kpa.value', 'ka.captured_in_pmis', 'kpa.special_responsibility'));
        $this->db->from($this->tempKpiAssignTable . ' kpa');
        $this->db->join('kpi_achievements' . ' ka', 'ka.kpi_id=kpa.kpi_id', 'inner');
        $this->db->where('kpa.user_id', $user_id);
        $this->db->where('kpa.status', 0);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
            return $data;
        } else {
            return $data[] = array('0' => '');
        }
    }

    function addNotification($user_id, $notification_type,$object_id,$object_type,$data_id, $self = null) 
	{

        $data = array();
        $data['sender_id'] = $user_id;
        $data['resource_id'] = $user_id;
		$data['object_id'] = $object_id;
		$data['data_id'] = $data_id;
        $data['object_type'] = $object_type;
        if ($self == 'self') {
            $data['reciever_id'] = $user_id;
        } else {
            $data['reciever_id'] = $this->getReportingOfficerId($user_id);
        }
        $data['type'] = $notification_type;
        $data['modified_date'] = date('Y-m-d H:i:s');
        $item = $this->getTargetNotification($data);
        if ($item['status'] != true) {
            $this->db->insert($this->notification, $data);

            $notification_id = $this->db->insert_id();
        } else {
            $notification_id = $item['notification_id'];
        }
        return $notification_id;
    }

    function getTargetNotification($data) 
	{

        $values = array();
        $this->db->where('sender_id', $data['sender_id']);
        $this->db->where('reciever_id', $data['reciever_id']);
        $this->db->where('type', $data['type']);
		$this->db->where('object_type', $data['object_type']);
		$this->db->where('object_id', $data['object_id']);
        //$this->db->where('read',0);
        $query = $this->db->get($this->notification);
        $data = $query->result();
        // echo $this->db->last_query(); die;
        if ($query->num_rows() > 0) {
            $values['status'] = true;
            $values['notification_id'] = $query->row()->notification_id;
        } else {
            $values['status'] = false;
            $values['notification_id'] = '';
        }
        return $values;
    }

    function getKpiNotification($user_id) {
        $this->db->where('user_id', $user_id);
        // $this->db->where('edit_special_responsibility', $notification_id);
        //$this->db->order_by('notification_id DESC');
        $query = $this->db->get('kpi_special_responsibilities');
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
            return $data;
        } else {
            return false;
        }
    }

    function addApprovalNotification($user_id, $notification_type, $object_id, $object_type) 
    {
        $data = array();
        $data['sender_id'] = $user_id;
		$data['object_id'] = $object_id;
		$data['object_type'] = $object_type;
        $data['reciever_id'] = $this->getReportingOfficerId($user_id);
        $data['data_type'] = $notification_type;
        $data['modified_date'] = date('Y-m-d H:i:s');
		//echo "<pre>"; print_r($data); die;
        $item = $this->getApprovalNotification($data);
        if ($item['status'] != true) 
        {
            $this->db->insert($this->approval_notification, $data);
            $data_id = $this->db->insert_id();
            if($notification_type != 'target')
            {
			 $this->db->insert($this->notification, array('type' => 'self_achieve', 'reciever_id' => $data['sender_id'], 'sender_id' => $data['sender_id'],'object_id' => $data['object_id'],'object_type' => $data['object_type'], 'resource_id' => $data['sender_id'],'data_id'=> $data_id));
             $this->db->insert($this->notification, array('type' => 'monthly_achieve', 'reciever_id' => $data['reciever_id'], 'sender_id' => $data['sender_id'],'object_id' => $data['object_id'],'object_type' => $data['object_type'], 'resource_id' => $data['sender_id'],'data_id'=> $data_id));
            }
         } else {
            $data_id = $item['data_id'];    
        }
		
        return $data_id;
    }

    function getApprovalNotification($data) {
        ///  print_R($data);die;
        $values = array();
        $this->db->where('sender_id', $data['sender_id']);
        $this->db->where('data_type', $data['data_type']);
        $this->db->where('object_id', $data['object_id']);
        $this->db->where('object_type', $data['object_type']);
        $query = $this->db->get($this->approval_notification);
        $items = $query->result();
        if ($query->num_rows() > 0) 
		{
            $values['status'] = true;
            $values['data_id'] = $query->row()->data_id; 
			if($data['data_type'] == 'target')
			{
			  $this->db->delete('temp_kpi_assigned',array('user_id'=>$data['sender_id']));
            $this->db->delete('temp_project_assigned',array('user_id'=>$data['sender_id']));
            $this->db->update($this->approval_notification, array('reporting_officer_approval' => 0, 'reviewing_officer_approval' => 0), array('data_id'=>$values['data_id']));
	          $this->db->insert($this->notification, array('type' => 'target_second_self', 'reciever_id' => $data['sender_id'], 'sender_id' => $data['sender_id'],'object_id' => $data['object_id'],'object_type' => 'user', 'resource_id' => $data['sender_id']));
              $this->db->insert($this->notification, array('type' => 'target_second', 'reciever_id' => $data['reciever_id'], 'sender_id' => $data['sender_id'], 'object_id' => $data['object_id'],'object_type' => 'user', 'resource_id' => $data['sender_id']));
			}
          
        } else {
            $values['status'] = false;
            $values['data_id'] = '';
        }

        return $values;
    }

    function getApproveStatus($user_id,$type) {
        $this->db->where('sender_id', $user_id);
        $this->db->where('data_type', $type);
        $data = array();
        $query = $this->db->get($this->approval_notification);
        // echo $this->db->last_query(); die;
        if ($query->num_rows() > 0) {
            $data = $query->result();
            return $data;
        } else {
            return $data[] = array('0' => '');
        }
    }

    function updateApproveStatus($approve, $data_id, $user_type, $message) {
        $this->db->where('data_id', $data_id);
        $data = array();
        if ($user_type == 1) {
            $values['reporting_officer_approval'] = $approve;
            $values['reporting_officer_message'] = $message;
        } else {
            $values['reviewing_officer_approval'] = $approve;
            $values['reviewing_officer_message'] = $message;
        }
        $date = date('Y-m-d H:i:s');
        
		
        try {
            $item = $this->getTargetNotificationById($data_id);
            
            $data['sender_id'] = $this->session->userdata('userid');
            if ($approve == 2) {
                $data['type'] = 'decline_target';
                if ($user_type == 2) {
                    $data['reciever_id'] = $item[0]->sender_id;
                    $data['sender_id'] = $this->session->userdata('userid');
					$values['reviewing_approval_date'] = $date;
					
                } else {
                    $data['reciever_id'] = $item[0]->sender_id;
					$values['reporting_approval_date'] = $date;
                }
                $data['resource_id'] = $item[0]->sender_id;
				
                if(($item[0]->reporting_officer_approval !=2) || ($item[0]->reviewing_officer_approval !=2))
                {
					
                    $this->db->insert($this->notification, $data);
                }
            } elseif ($approve == 1 && $user_type == 1) {
				
                $data['type'] = 'approve_target';
                $data['reciever_id'] = $this->getReportingOfficerId($item[0]->reciever_id);
                $data['resource_id'] = $item[0]->sender_id;
                $this->db->insert($this->notification, array('type' => 'self_approve_target', 'reciever_id' => $this->session->userdata('userid'), 'sender_id' => $this->session->userdata('userid'), 'resource_id' => $item[0]->sender_id));
                $this->db->insert($this->notification, array('type' => 'reporting_approve_target', 'reciever_id' => $item[0]->sender_id, 'sender_id' => $this->session->userdata('userid'), 'resource_id' => $item[0]->sender_id));
                $this->db->insert($this->notification, $data);
				$this->db->update($this->approval_notification, array('reporting_approval_date' => $date), array('data_id' => $data_id));
            } else {
				
                $data['type'] = 'final_approve_target';
                $data['reciever_id'] = $item[0]->sender_id;
                $data['resource_id'] = $item[0]->sender_id;
                $this->updateActualTragetData($data_id);
                $this->updateActualKpiData($data_id);
                $this->db->insert($this->notification, array('type' => 'final_approve_revieving', 'reciever_id' => $this->getReportingOfficerId($item[0]->sender_id), 'sender_id' => $this->session->userdata('userid'), 'resource_id' => $item[0]->sender_id));
                $this->db->insert($this->notification, array('type' => 'self_approve_target', 'reciever_id' => $this->session->userdata('userid'), 'sender_id' => $this->session->userdata('userid'), 'resource_id' => $item[0]->sender_id));
                $this->db->insert($this->notification, $data);
				$this->db->update($this->approval_notification, array('reviewing_approval_date' => $date), array('data_id' => $data_id));
            }
            $data['modified_date'] = $values['modified_date'];
			$this->db->update($this->approval_notification, $values, array('data_id' => $data_id));

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    function getTargetNotificationById($id) {
        $this->db->where('data_id', $id);
        $query = $this->db->get($this->approval_notification);
        // echo $this->db->last_query(); die;
        if ($query->num_rows() > 0) {
            $data = $query->result();
            return $data;
        } else {
            return;
        }
    }

    function updateActualTragetData($data) {
        $where = array('user_id', 'project_id', 'kpi_id', 'project_value', 'piu_id', 'ro_id');
        $this->db->select($where);
        $this->db->where('data_id', $data);
        $this->db->where('status', 0);
        $query = $this->db->get($this->tempTargetTable);
        $array = $query->result_array();
        foreach ($array as $key => $target) {
            $target['status'] = 1;
            $status = $this->checkActualTarget($target);
            if ($status == 1) {
                $this->updateActualTarget($target);
            } else {
                $this->db->insert($this->projectAssigned, $target);
            }
        }

        return true;
    }

    function checkActualTarget($array) {

        $this->db->where('project_id', $array['project_id']);
        $this->db->where('user_id', $array['user_id']);
        $this->db->where('piu_id', $array['piu_id']);
        $this->db->where('ro_id', $array['ro_id']);
        $this->db->where('kpi_id', $array['kpi_id']);
        $query = $this->db->get($this->projectAssigned);
        $data = $query->result();
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function updateActualTarget($array) {
        $this->db->where('project_id', $array['project_id']);
        $this->db->where('user_id', $array['user_id']);
        $this->db->where('piu_id', $array['piu_id']);
        $this->db->where('ro_id', $array['ro_id']);
        $this->db->where('kpi_id', $array['kpi_id']);

        $this->db->update($this->projectAssigned, $array);
    }

    function updateActualKpiData($data) {
        $where = array('user_id', 'kpi_id', 'unit', 'value');
        $this->db->select($where);
        $this->db->where('data_id', $data);
        $this->db->where('status', 0);
        $query = $this->db->get($this->tempKpiAssignTable);
        $array = $query->result_array();
        foreach ($array as $key => $kpi) {
            $target['status'] = 1;
            $status = $this->checkActualKpi($kpi);
            if ($status == 1) {
                $this->updateActualKpi($kpi);
            } else {
                $this->db->insert($this->kpiAssigned, array('kpi_id' => $kpi['kpi_id'], 'unit' => $kpi['unit'], 'value' => $kpi['value'], 'user_id' => $kpi['user_id'], 'status' => 1));
            }
        }

        return true;
    }

    function checkActualKpi($array) {

        $this->db->where('user_id', $array['user_id']);
        $this->db->where('kpi_id', $array['kpi_id']);
        $query = $this->db->get($this->kpiAssigned);
        $data = $query->result();
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function updateActualKpi($array) {
        $this->db->where('user_id', $array['user_id']);
        $this->db->where('kpi_id', $array['kpi_id']);
        $array['status'] = 1;
        $this->db->update($this->kpiAssigned, $array);
    }

    function addAchivement($data) {

        $values = array();
        $array = array('7' => $data['kpi7'], '8' => $data['kpi8'], '9' => $data['kpi9'], '10' => $data['kpi10'], '11' => $data['kpi11'], '12' => $data['kpi12'], '13' => $data['kpi13'], '14' => $data['kpi14'], '15' => $data['kpi15'], '18' => $data['kpi18'], '19' => $data['kpi19'], '27' => $data['kpi27'], '28' => $data['kpi28']);
        foreach ($array as $k => $v) {
            $values['project_id'] = $data['project_id'];
            $values['piu_id'] = $data['piu_id'];
            $values['ro_id'] = $data['ro_id'];
            $values['project_name'] = $data['project_name'];
            $values['stage'] = $data['stage'];
            $values['length'] = $data['length'];
            $values['project_value'] = $v;
            $values['kpi_id'] = $k;
            $values['modified_date'] = date('Y-m-d H:i:s');
            $values['startdate'] = date('Y-m-d');
            $values['enddate'] = date('Y-m-d');
            //echo "<pre>"; print_r($values); 
            $query = $this->db->insert('project_achievements', $values);
        }
    }

    function addNewAchievement($datas) {
        $this->db->insert_batch('project_achievements', $datas);
    }

    function getMonthlyAchive($project_id, $ro_id, $kpi_id, $type, $data_id) {
        if ($type == 'notification') {
            $table = 'temp_monthly_achievements';
            $sql = "select achive_value, description from " . $table . " where project_id=" . $project_id . "  AND ro_id = " . $ro_id . "  AND kpi_id = " . $kpi_id . " AND data_id = " . $data_id . "  order by enddate ASC";
        } else {
            $table = 'project_achievements';
            $sql = "select achive_value from " . $table . " where project_id=" . $project_id . "  AND ro_id = " . $ro_id . "  AND kpi_id = " . $kpi_id . "  order by enddate ASC";
        }


        $user_data = $this->db->query($sql)->result_array();
        return $user_data;
    }

    function addTempMonthlyAchive($values, $monthData, $description) {
        //echo $description; die;
        $data = array();
        $j = 0;
        $data_id = $this->custom_model->addApprovalNotification($values['data']['user_id'], 'monthly_achieve',$values['data']['project_id'],'project_monthly_achieve');
        $this->db->delete('temp_monthly_achievements', array('data_id' => $data_id));
        foreach ($monthData as $item) {
            //$data['values'] = $values['data'];
            if ($item != '') {
                $i = $j + 4;
                $data[$j]['achive_value'] = $item;
                $data[$j]['startdate'] = date("Y-$i-1");
                $data[$j]['enddate'] = date("Y-$i-30");
                $data[$j]['project_id'] = $values['data']['project_id'];
                $data[$j]['ro_id'] = $values['data']['ro_id'];
                $data[$j]['user_id'] = $values['data']['user_id'];
                $data[$j]['kpi_id'] = $values['data']['kpi_id'];
                $data[$j]['data_id'] = $data_id;
                $data[$j]['description'] = $description;
                $j++;
            }
        }
        try {
            $this->db->insert_batch('temp_monthly_achievements', $data);
             return true;
        } catch (Exception $e) {
            return false;
        }
        //echo "<pre>"; print_r($data); die;
    }

    function getTempAchivement($user_id,$data_id) 
	{
        $data = array();
		$this->db->select('tmp.*, pr.project_name');
		$this->db->from('temp_monthly_achievements as tmp');
		$this->db->join('projects as pr', 'pr.project_id = tmp.project_id', 'right');
		$this->db->where('tmp.ro_id = pr.ro_id');
		$this->db->where('tmp.user_id',$user_id);
		$this->db->where('tmp.data_id',$data_id);
		$this->db->limit($filters['limit'], $filters['offset']);
		//echo $this->db->last_query(); die;
		$query = $this->db->get();
		
        // $this->db->where('user_id', $user_id);
        // $this->db->where('data_id', $data_id);
        // $query = $this->db->get('temp_monthly_achievements');
        //echo $this->db->last_query(); 
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
            return $data;
        } else {
            return $data[] = array('0' => '');
        }
    }

    function updateAchiveData($values) {
        $i = 4;
        foreach ($values['values'] as $k => $v) {
            if (isset($v)) {
                $this->db->where('data_id', $values['data_id']);
                $this->db->where('user_id', $values['user_id']);
                $this->db->where("MONTH(enddate) = $i");
                $this->db->update('temp_monthly_achievements', array('achive_value' => $v, 'modified_date' => date('Y-m-d H:i:s')));
                $i++;
            }
        }
    }

    function approveAchiveData($values) {
        $this->updateAchiveData($values);
        $reporting = $this->getReportingOfficerId($values['user_id']);
        $reviwieng = $this->getReportingOfficerId($reporting);
        if ($reporting == $this->session->userdata('userid')) {
            $data = array('reporting_officer_approval' => 1, 'modified_date' => date('Y-m-d H:i:s'));
            $this->db->insert($this->notification, array('type' => 'approve_achive', 'reciever_id' => $reviwieng, 'sender_id' => $this->session->userdata('userid'), 'resource_id' => $values['user_id']));
            $this->db->insert($this->notification, array('type' => 'reporting_approve_achive', 'reciever_id' => $values['user_id'], 'sender_id' => $this->session->userdata('userid'), 'resource_id' => $values['user_id']));
            $this->db->insert($this->notification, array('type' => 'self_approve_achieve', 'reciever_id' => $this->session->userdata('userid'), 'sender_id' => $this->session->userdata('userid'), 'resource_id' => $values['user_id']));
        } else {
            $data = array('reviewing_officer_approval' => 1, 'modified_date' => date('Y-m-d H:i:s'));
            $this->db->insert($this->notification, array('type' => 'self_approve_achieve', 'reciever_id' => $this->session->userdata('userid'), 'sender_id' => $this->session->userdata('userid'), 'resource_id' => $values['user_id']));
            $this->db->insert($this->notification, array('type' => 'final_approve_achieve', 'reciever_id' => $reporting, 'sender_id' => $this->session->userdata('userid'), 'resource_id' => $values['user_id']));
            $this->db->insert($this->notification, array('type' => 'final_approve_achieve', 'reciever_id' => $values['user_id'], 'sender_id' => $this->session->userdata('userid'), 'resource_id' => $values['user_id']));
            $sql = 'update project_achievements as pa join temp_monthly_achievements as tmp on tmp.project_id = pa.project_id AND  tmp.ro_id = pa.ro_id AND tmp.kpi_id=pa.kpi_id AND MONTH(pa.enddate) = MONTH(tmp.enddate) AND  tmp.data_id=' . $values['data_id'] . ' set pa.achive_value = tmp.achive_value;';
            $this->db->query($sql);
        }
        $this->db->where('data_id', $values['data_id']);
        $this->db->where('sender_id', $values['user_id']);
        $this->db->where('data_type', 'monthly_achieve');
        $this->db->where("MONTH(creation_date) = MONTH(CURDATE())");
        $this->db->update('project_data_approval', $data);
    }
	
    function kpi_date_for_user_summaru($user_id) 
	{
        $data = array();
		$this->db->select('ka.value as weightage, kah.captured_in_pmis as name, sum(pa.project_value) as target');
		
		
		$this->db->from('kpi_assigned as ka');
		$this->db->join('kpi_achievements as kah', 'kah.kpi_id = ka.kpi_id', 'left');
		$this->db->join('project_assigned as pa', 'pa.kpi_id = ka.kpi_id and pa.user_id = ka.user_id', 'right');
		//$this->db->join('project_achievements as achive', 'achive.kpi_id = ka.kpi_id and achive.project_id = pa.project_id and pa.piu_id = achive.piu_id', 'right');		
		$this->db->where('ka.user_id',$user_id);
		$this->db->group_by('pa.kpi_id');
		return $this->db->get()->result();
    }

}

?>