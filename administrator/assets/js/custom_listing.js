$(document).on("click", ".del_Listing", function () {
    var delID = $(this).attr('name');
    var deleteUrl = $("#P_deleteurl").val();
    bootbox.confirm("Are you sure to delete this ?", function (result) {
        if (result == true) {
            $.ajax({
                type: 'POST',
                url: deleteUrl,
                data: {delID: delID},
                success: function (res) {
                    var obj = JSON.parse(res);
                    if (obj['status'] == true) {
                        location.reload();
                    } else {
                        alert("Bad request");
                    }
                },
            });
        }
    });
});

function checkall(objForm){
	len = objForm.elements.length;
	var i=0;
	for( i=0 ; i<len ; i++) {
		if (objForm.elements[i].type=='checkbox') {
			objForm.elements[i].checked=objForm.check_all.checked;
		}
	}
}

function showEmail() {

  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
      document.getElementById("responce_container").innerHTML=xmlhttp.responseText;
    }
  }
  xmlhttp.open("GET","admin/SearchCandidates/invite?q=",true);
  xmlhttp.send();
}