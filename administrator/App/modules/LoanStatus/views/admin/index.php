<section>
    <div class="section-body">
        <div class="card">

            <!-- BEGIN CONTACT DETAILS HEADER -->
            <div class="card-head style-primary">
                <div class="tools pull-left">
                    <form class="navbar-search" role="search">
                        <div class="form-group">
                            <input type="text" class="form-control" name="contactSearch" placeholder="Enter your keyword">
                        </div>
                        <button type="submit" class="btn btn-icon-toggle ink-reaction"><i class="fa fa-search"></i></button>
                    </form>
                </div><!--end .tools -->
                
                 
   
                <div class="col-sm-6 col-md-6 pull-right">
                <a href="<?php echo base_url('admin/LoanStatus/create'); ?>" class="btn btn-primary pull-right addnewloan">Add New Loan</a>
            </div><!--end .tools -->
            </div><!--end .card-head -->
            <!-- END CONTACT DETAILS HEADER -->

            <!-- BEGIN CONTACT DETAILS -->
            <div class="card-tiles">
                <div class="hbox-md col-md-12">
                    <div class="hbox-column col-md-9">
                        <div class="row">


                            <!-- END CONTACTS NAV -->

                            <!-- BEGIN CONTACTS MAIN CONTENT -->
                            <div class="col-sm-12 col-md-12 col-lg-12">
                            <?php if ($this->session->flashdata('message')) { ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('message'); ?>
        </div>
    <?php } ?>
                                <?php if ($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php } ?>
                                <ul class="nav nav-tabs" data-toggle="tabs">
                                    <li class="active"><a href="#details">Loan Status</a></li>
                                    <!-- <li><a href="#notes">FJ QUESTION BANK</a></li>
                                    <li><a href="#note">MY QUESTIONS</a></li> -->
                                </ul>
                                <div class="tab-content">

                                    <!-- BEGIN CONTACTS NOTES -->
                                    <div class="tab-pane active" id="details">

                                        
                                                           <table id="datatable1" class="table table-striped table-hover">
                            <thead>
                                <tr>
                            <th><div class="checkbox checkbox-styled"><label><input name="check_all" type="checkbox" id="check_all" value="1" onclick="checkall(this.form)" ></label></div></th>
                            <th>steps</th>
                            <th>Type</th>
                            <th>CreationDate</th>
                           
                            </tr>
                            </thead>
                            <tbody>
                            
                             <?php foreach ($loan as $int): ?>
                                    <tr class="<?php echo ($int->status == 'active' ? 'gradeX' : 'gradeA'); ?>">
                                        <td>
                                            <div class="checkbox checkbox-styled"><label><input name="arr_ids[]" type="checkbox" id="arr_ids[]" value="<?php echo $int->id; ?>" ></label></div>
                                        </td>
                                        <td><a href=""><?php echo $int->steps ;?></a></td>
                                        <td><?php echo $int->type; ?></td>
                                         <td><?php echo $int->creationDate; ?></td>
                                        <td><a href="<?php echo base_url('admin/LoanStatus/edit') . '/' . $int->id; ?>"><i class="fa fa-edit"></i></a></td>
                                         
                                       
                                    </tr>
                                <?php endforeach; ?>
                             
                            </tbody>
                        </table>
                        <!--end .timeline -->
                                    </div><!--end #notes -->
                                    <!-- END CONTACTS NOTES -->

                                    <!-- END CONTACTS ACTIVITY -->

                                    <!-- BEGIN CONTACTS DETAILS -->
                                    <div class="tab-pane" id="notes">
                                          <ul class="timeline collapse-lg timeline-hairline no-shadow">
                                            <li class="timeline-inverted">
                                                <div class="timeline-circ style-accent"></div>
                                                <div class="timeline-entry">
                                                    <div class="card style-default-bright">
                                                        <div class="card-body small-padding">
                                                           <table id="datatable1" class="table table-striped table-hover">
                            <thead>
                                <tr>
                            <th><div class="checkbox checkbox-styled"><label><input name="check_all" type="checkbox" id="check_all" value="1" onclick="checkall(this.form)" ></label></div></th>
                            <th>Title</th>
                            <th>Type</th>
                            <th>No. of Questions</th>
                           
                            </tr>
                            </thead>
                            <tbody>
                             
                            </tbody>
                        </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul><!--end .dl-horizontal -->
                                    </div><!--end #details -->
                                    <!-- END CONTACTS DETAILS -->

                                </div><!--end .tab-content -->
                            </div><!--end .col -->
                            <!-- END CONTACTS MAIN CONTENT -->
                         <input type="hidden" id="P_deleteurl" value="<?php echo base_url('admin/ManageInterviews/delete'); ?>" />
                        </div><!--end .row -->
                    </div><!--end .hbox-column -->

                    <!-- BEGIN CONTACTS COMMON DETAILS -->
                    <!-- END CONTACTS COMMON DETAILS -->

                </div><!--end .hbox-md -->
            </div><!--end .card-tiles -->
            <!-- END CONTACT DETAILS -->

        </div><!--end .card -->
    </div><!--end .section-body -->
</section>



<script>

function delenquiry(id) {
    if (confirm("Are you sure?")) {
        $.ajax({
            url: base_url + 'admin/ManageInterviews/delete',
            type: 'post',
            data: {id:id},
            success: function () {
                alert('ajax success');
            },
            error: function () {
                alert('ajax failure');
            }
        });
    } else {
        alert(id + " not deleted");
    }
}
</script>