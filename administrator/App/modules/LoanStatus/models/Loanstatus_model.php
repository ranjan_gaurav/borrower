<?php

/*
 *
 */
class Loanstatus_model extends CI_Model {
	var $table = "m_loan_status";
	var $question = "questions";
	var $table_fields_values = "user_fields_values";
	var $original_path;
	var $resized_path;
	var $thumbs_path;
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'session' );
		$this->load->library ( 'pagination' );
		error_reporting(0);
	}
	function read($showPerpage, $offset) {
		$this->db->limit ( $showPerpage, $offset );
		// $this->db->where ( 'id', $jobid );
		$this->db->select ( 'm_loan_status.*' );
		$this->db->from ( $this->table );
	
		$query = $this->db->get ();
		// echo $this->db->last_query();die;
		return $query->result ();
	}
	function usersFeilds($showPerpage, $offset) {
		$this->db->limit ( $showPerpage, $offset );
		$this->db->from ( $this->usersFeilds );
		$query = $this->db->get ();
		return $query->result ();
	}
	function count_all() {
		$query = $this->db->get ( $this->table );
		return $query->num_rows ();
	}
	function loan_by_id($id) {
		$this->db->where ( 'id', $id );
		$query = $this->db->get ( $this->table );
		if ($query->num_rows () > 0) {
			return $query->row ();
		} else {
			return false;
		}
	}
	function get_question_bysetid($jobid) {
		$this->db->where ( 'questionset_id', $jobid );
		$query = $this->db->get ( $this->question );
		if ($query->num_rows () > 0) {
			return $query->result_array ();
		} else {
			return false;
		}
	}
	function Loan_update($id) {
		$data = array ();
		
		$data ['steps'] = $this->input->post ( 'step' );
		$data ['type'] = $this->input->post ( 'type' );
		
		$this->db->where ( 'id', $id );
		$query = $this->db->update ( $this->table, $data );
		if ($query) {
			
			// echo $this->db->last_query();die;
			
			return true;
		} else {
			return false;
		}
	}
	function AddLoanStatus() {
		$data ['steps'] = $this->input->post ( 'step', true );
		$data ['type'] = $this->input->post ( 'type', true );
		
		$query = $this->db->insert ( $this->table, $data );
		if ($query) {
		
			// echo $this->db->last_query();die;
			
			return true;
		} else {
			return false;
		}
	}
	function updateStatus() {
		if ($this->input->post ( 'Delete' ) != null) {
			$arr_ids = $this->input->post ( 'arr_ids' );
			if (! empty ( $arr_ids )) {
				$this->db->where_in ( 'id', $arr_ids );
				$this->db->delete ( $this->table );
			}
		} else {
			if ($this->input->post ( 'Deactivate' ) != null) {
				$status = "inactive";
			} else if ($this->input->post ( 'Activate' ) != null) {
				$status = "active";
			}
			$data = array (
					'status' => $status 
			);
			
			$arr_ids = $this->input->post ( 'arr_ids' );
			if (! empty ( $arr_ids )) {
				$this->db->where_in ( 'id', $arr_ids );
				$this->db->update ( $this->table, $data );
			}
		}
	}
	function get_questions($id) {
		$this->db->where ( 'questionset_id', $id );
		$this->db->from ( $this->question );
		$result = $this->db->get ();
		return $result->result ();
	}
	function fileupload() {
		$config = array ();
		$new_name = time () . $_FILES ["file"] ['name'];
		$config ['file_name'] = $new_name;
		$config ['upload_path'] = IMAGESPATH . 'audition/';
		$config ['allowed_types'] = '*';
		$this->load->library ( 'upload', $config );
		$this->upload->do_upload ( 'audition_video' );
		$Image = $this->upload->data ();
		// print_r($Image); die;
	}
	
	function delete()
	{
		
		
	}
	
	
}

?>