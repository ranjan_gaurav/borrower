<?php

/**
 * Created by PhpStorm.
 * User: Sanjay Yadav<yadav.sanjay@orangemantra.in>
 * Date: 20/1/2016
 * Time: 11:28 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class User extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('user_model');
        $this->load->helper('string');
    }

    /**
     * @method : list Users using GET Method
     * @method description: call to get list of Users.
     * @param : care_id
     * @data: Users Data
     */
    public function list_post() {
        $id = (int) $this->post('user_id');
        log_message('info', 'data=' . $id);
        $users = $this->user_model->getAllUsers($id);

        if (!empty($users)) {
            $this->set_response([
                'status' => true,
                'response_code' => '1',
                'message' => 'Success',
                'data' => $users
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->set_response([
                'status' => FALSE,
                'response_code' => '0',
                'message' => 'No Content'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function add_post() {
        foreach ((($this->post())) as $key => $value) {
            log_message('info', 'data=' . $key . ' =>' . $value);
        }
        $posted_data = $this->post();
        $randcode = random_string('numeric', 16);
        $random_code = substr($randcode, 0, 4);
        $new_reg_id = $this->input->post('reg_id', true);
        $device_id = array($new_reg_id);
        $data = array(
            'user_mobile' => $this->post('user_mobile'),
            'user_email' => $this->post('user_email'),
            'user_pass' => md5($this->post('user_pass')),
            'first_name' => $this->post('first_name') ? $this->post('first_name') : '',
            'user_dob' => $this->post('user_dob') ? $this->post('user_dob') : '',
            'user_gender' => $this->post('user_gender') ? $this->post('user_gender') : '',
            'random_code' => $random_code,
            'status' => '0'
        );
        $create = $this->user_model->create($data);
        $this->set_response($create, REST_Controller::HTTP_OK);
    }

    /**
     * @method : Update User using Post Method
     * @method description: Update.
     * @param : care_id, Data array
     * @data: User Data
     */
    public function update_post() {

        $id = $this->post('user_id');
        if ($id == '') {
            $message = [
                'response_code' => '0',
                'message' => 'Failed ! Please Send User ID.'
            ];
            $this->response($message, REST_Controller::HTTP_OK);
        }
        if (isset($id) && $id != '') {
            $update = $this->user_model->update($id, $this->post());
            $user_data = $this->user_model->GetUpdateddata($id);
            $user_data->photo_id = $user_data->photo_id ? base_url() . 'uploads/users/profile/' . $user_data->photo_id : null;
            if ($update['status']) {
                $status = REST_Controller::HTTP_OK;
                $update['data'] = $user_data;
            } else {
                $status = REST_Controller::HTTP_OK;
            }
            $this->set_response($update, $status);
        } else {

            $message = [
                'response_code' => '0',
                'message' => 'Id Not Found'
            ];
            $this->set_response($message, REST_Controller::HTTP_OK);
        }
    }

    /**
     * @method : Delete User using DELETE Method
     * @method description: delete  User.
     * @param : care__id
     * @data:
     */
    public function drop_delete() {
        $id = (int) $this->get('user_id');
        // Validate the id.
        if ($id <= '0') {
            $this->response(NULL, REST_Controller::HTTP_OK);
        }
        $result = $this->user_model->delete($id);
        if ($result) {
            $message = [
                'response_code' => '1',
                'message' => 'User Deleted'
            ];
            $this->set_response($message, REST_Controller::HTTP_OK);
        } else {
            $message = [
                'response_code' => '0',
                'message' => 'Bad request'
            ];
            $this->set_response($message, REST_Controller::HTTP_OK);
        }
    }

    static public function slugify($text) {
        $text = preg_replace('~[^\\pL\d]+~u', '_', $text);
        $text = trim($text, '-');
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = strtolower($text);
        $text = preg_replace('~[^-\w]+~', '', $text);
        if (empty($text)) {
            return 'n-a';
        }
        return $text;
    }

    function signin_post() {
        $user_id = $this->post('user_mobile', true);
        $password = $this->post('user_pass', true);
        $new_reg_id = $this->input->post('reg_id', true);
        $deviceID = $this->input->post('device_id', true);
        $deviceType = $this->input->post('device_type', true);

        $device_id = json_encode(array($new_reg_id));
        $userdata = $this->user_model->validate($user_id, md5($password));
        if ($userdata) {
            $CareDeviceCheck = $this->user_model->CheckCareDevice($deviceID, $userdata->id, $deviceType);
            if ($CareDeviceCheck > 0) {
                $deviceUpdate = $this->user_model->updateDeviceId($deviceID, $userdata->id, $new_reg_id);
            } else {
                $DeviceCheck = $this->user_model->CheckDevice($deviceID, $deviceType);
                if ($DeviceCheck > 0) {
                    $this->user_model->DeleteDeviceUser($deviceID);
                }
                $deviceUpdate = $this->user_model->InsertDeviceId($deviceID, $userdata->id, $new_reg_id, $deviceType);
            }
            $userid = $userdata->id;
            $photo_id = ($userdata->user_pic != '' && $userdata->user_pic != null ) ? base_url() . '/uploads/users/profile/' . $userdata->user_pic : "";
            if ($userdata->status == 0) {
                $message = [
                    'response_code' => '0',
                    'message' => 'Not validated!'
                ];
                $this->set_response($message, REST_Controller::HTTP_OK);
            } else {
                $Res = $userdata;
                if ($this->post('is_signup') == '1') {
                    $type = 'signup';
                } else {
                    $type = 'signin';
                }
                if($deviceType=='android'){
                    $this->user_model->androidnotification($userid, $type , $deviceID);
                }else{
                    $this->user_model->iosNotification($userid, $type , $deviceID);
                }
                
                $Res->user_pic = $photo_id;
                $removeKeys = array('user_pass', 'registered_date', 'activation_key', 'user_modification_date', 'random_code', 'reg_id', 'reg_id_ios');

                foreach ($removeKeys as $key) {
                    unset($Res->$key);
                }
                $message = [
                    'response_code' => '1',
                    'message' => 'Success',
                    'data' => $Res
                ];

                $this->set_response($message, REST_Controller::HTTP_OK);
            }
        } else {
            $message = [
                'response_code' => '0',
                'message' => 'Invalid Credentials'
            ];

            $this->set_response($message, REST_Controller::HTTP_OK);
        }
    }

    public function forgotpassword_post() {
        foreach ((($this->post())) as $key => $value) {
            log_message('info', 'data=' . $key . ' =>' . $value);
        }
        $data = array(
            'email_id' => $this->post('email_id'),
            'mobile' => $this->post('mobile')
        );
        $checkCredentails = $this->user_model->CheckUserCredentials($data);
        if ($checkCredentails) {
            $create = $this->user_model->validate_password($data);
            if ($create['status']) {
                $status = REST_Controller::HTTP_OK;
                $this->set_response($create, $status);
            } else {

                $message = [
                    'response_code' => '0',
                    'message' => $create['message']
                ];
                $this->set_response($message, REST_Controller::HTTP_OK);
            }
        } else {
            $message = [
                'response_code' => '0',
                'message' => 'Sorry! Invalid Credentails.Please update Your Email and Mobile in profile or Contact Administarator!'
            ];
            $this->set_response($message, REST_Controller::HTTP_OK);
        }
    }

    // change password 
    function changepassword_post() {
        $user_id = $this->post('user_id', true);
        $current_pass = $this->post('current_pass', true);
        $new_pass = $this->post('password', true);
        if ($user_id != '') {
            $userpasswrod = $this->user_model->valid_current_pass($user_id, md5($current_pass));
            if (!$userpasswrod) {
                $message = [
                    'response_code' => '0',
                    'message' => 'Invalid Current Password'
                ];
                $this->set_response($message, REST_Controller::HTTP_OK);
            } else {
                $this->user_model->updatepassword('users', $user_id, $new_pass);
                $message = [
                    'response_code' => '1',
                    'message' => 'Success',
                    'new_password' => $new_pass
                ];
                $this->set_response($message, REST_Controller::HTTP_OK);
            }
        } else {
            $message = [
                'response_code' => '0',
                'message' => 'Invalid Credentials'
            ];
            $this->set_response($message, REST_Controller::HTTP_OK);
        }
    }

    protected function changeprofilepic_post() {
        $user_id = (int) $this->post('care_id');
        if (isset($user_id) && $user_id != '' && $user_id != null) {
            $update = $this->user_model->updateprofile($user_id);
            if ($update) {
                $message = [
                    'response_code' => '1',
                    'message' => 'Success',
                    'photo_id' => $update
                ];
                $this->set_response($message, REST_Controller::HTTP_OK);
            } else {
                $message = [
                    'response_code' => '0',
                    'message' => 'Not Updated! Please Browse File to upload'
                ];
                $this->set_response($message, REST_Controller::HTTP_OK);
            }
        } else {
            $message = [
                'response_code' => '0',
                'message' => 'Invalid User'
            ];
            $this->set_response($message, REST_Controller::HTTP_OK);
        }
    }

    // logout 

    public function logout_post() {
        $user_id = $this->post('care_id', true);
        $reg_id = $this->input->post('android_device_id', true);
        $device = $this->input->post('device_id', true);

        if ($user_id != '' && $reg_id != '') {

            //$create = $this->user_model->updaterlogout('users', $user_id, $reg_id);
            $create = $this->user_model->Deviceupdaterlogout('user_devices', $user_id, $device);
            $message = [
                'response_code' => '1',
                'userid' => $user_id,
                'message' => 'Successfully Logout'
            ];
            $this->set_response($message, REST_Controller::HTTP_OK);
        } else {
            $message = [
                'response_code' => '0',
                'message' => 'Sorry! Invalid Userid. and reg id'
            ];
            $this->set_response($message, REST_Controller::HTTP_OK);
        }
    }

}
