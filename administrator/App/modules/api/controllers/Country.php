<?php
/**
 * Created by PhpStorm.
 * User: Parmod Bhardwaj<parmod@orangemantra.com>
 * Date: 10/13/2015
 * Time: 10:58 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
class Country extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('country_model');
    }

    public function list_get() {
        $id = strtoupper($this->get('code'));
        $country = $this->country_model->getData($id);

        if (!empty($country)) {
            $this->set_response([
                'status' => true,
                'message' => 'Success',
                'data' => $country
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->set_response([
                'status' => FALSE,
                'message' => 'Country Data could not be found'
            ], REST_Controller::HTTP_NO_CONTENT); // NOT_FOUND (404) being the HTTP response code
        }
    }
} 