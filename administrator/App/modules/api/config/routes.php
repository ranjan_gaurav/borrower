<?php

/*
 * Routing for Users
 */
$route['api/user'] = "User/list";
$route['api/user/add'] = "User/add";
$route['api/user/update'] = "User/update";
$route['api/user/drop/(:any)'] = "User/drop/$1";
$route['api/forgotpassword'] = "User/forgotpassword";
$route['api/changepassword'] = "User/changepassword";
$route['api/changepic'] = "User/changeprofilepic";
$route['api/user/signin'] = "User/signin";
$route['api/user/logout'] = "User/logout";
$route['api/user/verify_register'] = "User/verify_register";
$route['api/user/(:any)'] = "User/list/$1";



$route['api/notifications'] = "Notification/notifications";
$route['api/customnotifications'] = "Notification/customnotifications";
$route['api/notifications/(:any)'] = "Notification/notifications/$1";
$route['api/usernotifications/(:any)'] = "Notification/usernotifications/$1";
?>