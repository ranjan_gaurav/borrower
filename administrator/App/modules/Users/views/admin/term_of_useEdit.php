<script src="<?php echo base_url('assets/plugins/ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/ckfinder/ckfinder.js'); ?>"></script>
<section>
    <div class="contain-lg">

        <!-- BEGIN BASIC VALIDATION -->
        <div class="row">
            <div class="col-md-12">
                <form class="form form-validate floating-label" novalidate="novalidate" method="post">
                    <div class="card-head style-primary">
                        <div class="tools pull-left">
                            <header>Edit terms & condition</header>
                        </div>
                        <div class="tools">
                            <a class="btn btn-flat hidden-xs" href="<?php echo base_url('admin/Users/term_of_use'); ?>"><span class="glyphicon glyphicon-arrow-left"></span> &nbsp;Back</a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <?php if (@$error): ?>
                                <div class="alert">
                                    <button type="button" class="close" data-dismiss="alert">�</button>
                                    <?php echo $error; ?>
                                </div>
                            <?php endif; ?>
                          

                            <div class="form-group">
                                <textarea id="editor1" class="form-control control-5-rows" placeholder="Enter text ..." name="description"><?php echo $terms['description']; ?>
                                </textarea>
                               
                            </div><!--end .card-body -->

                            <script>
                                var editor = CKEDITOR.replace('editor1');
                                editor.config.extraAllowedContent = 'div(*)';
                                CKEDITOR.config.allowedContent = true;
                                CKEDITOR.disableAutoInline = true;
                                CKFinder.setupCKEditor(editor, '<?php echo base_url() ?>assets/plugins/ckfinder/');
                            </script>
                         
                        </div><!--end .card-body -->
                        <div class="card-actionbar">
                            <div class="card-actionbar-row">
                                <button type="submit" class="btn ink-reaction btn-raised btn-primary btn-loading-state" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading...">Update<div style="top: 26px; left: 32.5px;" class="ink"></div></button>
                            </div>
                        </div><!--end .card-actionbar -->
                    </div><!--end .card -->
                </form>
            </div><!--end .col -->
        </div><!--end .row -->
        <!-- END BASIC VALIDATION -->

    </div><!--end .section-body -->
</section>