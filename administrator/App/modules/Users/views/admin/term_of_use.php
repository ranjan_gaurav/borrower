<style>

#page-wrapper{background: #fff;    padding: 7% 8%; font-family: "Source Sans Pro", sans-serif;}

	p{

		font-family: "Source Sans Pro", sans-serif !important;

	}

.cms-page-container{padding:5% 8%; background: #fff;}

.cms-page-container h1 { border-bottom: 1px solid #ccc;color: #00337e !important; font-family: "Source Sans Pro", sans-serif ;font-size: 22px; margin: 0 0 15px; padding: 0 0 10px;}

.cms-page-container h2{ font-size:18px; color:#00337e !important; padding:5px 0px 10px; margin:0px;font-family:"Source Sans Pro", sans-serif ;}

.cms-page-container p {color: #333;font-family: "Source Sans Pro", sans-serif ;font-size: 14px;line-height: 1.6;margin: 0;padding: 0 0 15px;     text-transform: lowercase; text-align: justify}

.cms-page-container p span{ color:#3694fa;}

p:first-letter {

    text-transform: initial;

}

.cms-page-container ol, .cms-page-container ul {margin: 0; padding: 0 0 0 15px;}

.cms-page-container ol li, .cms-page-container ul li{ font-size:14px; color:#333;line-height: 1.6; padding-bottom:15px; margin:0px;font-family:"Source Sans Pro", sans-serif ;}

.cms-page-container ol li strong{color:#00337e;}

</style>





<div class="cms-page-container">
 <?php if ($this->session->flashdata('message')) { ?>
                            <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Success ! </strong> <?php echo $this->session->flashdata('message'); ?>
                            </div>
                        <?php } ?>
<a class="pull-right" href="<?php echo base_url('admin/users/term_of_useEdit') . '/' . $terms['id']?>"><i class="fa fa-edit"></i></a>
<?php echo $terms['description'];?>

</div>





