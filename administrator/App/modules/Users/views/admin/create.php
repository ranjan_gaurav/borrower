	<script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>
<section>
    <div class="section-body contain-lg">
        <div class="row">

            <!-- BEGIN ADD CONTACTS FORM -->
            <div class="col-md-12">
                <div class="card">
                    <div class="card-head style-primary">
                        <div class="tools pull-left">
                            <header>Add a New User</header>
                        </div>
                        <div class="tools">
                            <a class="btn btn-flat hidden-xs" href="<?php echo base_url('admin/users'); ?>"><span class="glyphicon glyphicon-arrow-left"></span> &nbsp;Back</a>
                        </div>
                    </div>
                    <form class="form" role="form form-validate" method="post" action="" id="userForm" enctype="multipart/form-data">

                        <!-- BEGIN DEFAULT FORM ITEMS -->
                        <?php if (@$error): ?>
                            <div class="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <?php echo $error; ?>
                            </div>
                        <?php endif; ?>
                        <div class="card-body style-primary form-inverse">
                            <div class="row">
                                <div class="col-xs-12">
                                    <!--end .row -->
                                </div><!--end .col -->
                            </div><!--end .row -->
                        </div><!--end .card-body -->
                        <!-- END DEFAULT FORM ITEMS -->

                        <!-- BEGIN FORM TABS -->
                        <div class="card-head style-primary">
                            <ul class="nav nav-tabs tabs-text-contrast tabs-accent" data-toggle="tabs">
                                <li class="active"><a href="#contact">CONTACT INFO</a></li>
                              <!--   <li><a href="#general">Notes</a></li> -->
                            </ul>
                        </div><!--end .card-head -->
                        <!-- END FORM TABS -->

                        <!-- BEGIN FORM TAB PANES -->
                        <div class="card-body tab-content">
                            <div class="tab-pane active" id="contact">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="user_mobile" value="<?php echo set_value('user_mobile'); ?>" name="user_mobile" required>
                                                    <label for="user_mobile">Mobile</label>
                                                </div>
                                            </div><!--end .col -->
                                         
                                        </div><!--end .row -->
                                        
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="user_email" value="<?php echo set_value('user_email'); ?>" name="username" required>
                                            <label for="user_email">Username</label>
                                        </div><!--end .form-group -->
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="user_pass" value="<?php echo set_value('user_pass'); ?>" name="user_pass" required>
                                                    <label for="user_pass">Password</label>
                                                </div>
                                            </div><!--end .col -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="user_cnf_pass" value="<?php echo set_value('user_cnf_pass'); ?>" name="user_cnf_pass" required>
                                                    <label for="user_cnf_pass">Confirm Password</label>
                                                </div>
                                            </div><!--end .col -->
                                        </div><!--end .row -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <select class="form-control" name="user_gender" id="user_gender" required>
                                                        <option value="">Select</option>
                                                        <option value="Male">Male</option>
                                                        <option value="Female">Female</option>
                                                    </select>
                                                    <label for="user_gender">Gender</label>
                                                </div>
                                            </div><!--end .col -->
                                        </div><!--end .row -->
                                      
                                    </div><!--end .col -->
                                   <!--end .col -->
                                </div><!--end .row -->
                            </div><!--end .tab-pane -->
                            <div class="tab-pane" id="general">
                                <div class="form-group">
                                    <textarea id="summernote" name="note" class="form-control control-4-rows" placeholder="Enter note ..." spellcheck="false"></textarea>
                                </div><!--end .form-group -->
                            </div><!--end .tab-pane -->
                        </div><!--end .card-body.tab-content -->
                        <!-- END FORM TAB PANES -->

                        <!-- BEGIN FORM FOOTER -->
                        <div class="card-actionbar">
                            <div class="card-actionbar-row">
                                <button type="submit" class="btn btn-flat btn-primary ink-reaction">ADD THIS PERSON</button>
                            </div><!--end .card-actionbar-row -->
                        </div><!--end .card-actionbar -->
                        <!-- END FORM FOOTER -->

                    </form>
                </div><!--end .card -->
            </div><!--end .col -->
            <!-- END ADD CONTACTS FORM -->

        </div><!--end .row -->
    </div><!--end .section-body -->
</section>

<script type="text/javascript">
function initialize() {
    var input = document.getElementById('user_city');
    var options = {componentRestrictions: {country: 'in'}};
                 
    new google.maps.places.Autocomplete(input, options);
}
             
google.maps.event.addDomListener(window, 'load', initialize);
</script>