<?php

/*

 */

class Users extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    function is_admin($id) {
        if (@$this->userCheck($id)->role === 1) {
            return true;
        } else {
            return false;
        }
    }

    function userCheck($id) {
        return $this->user_model->user_by_id($id);
    }

    function logout() {
        $this->session->sess_destroy();
        redirect(base_url('admin/home'));
    }

//Hidden Methods not allowed by url request

    function _member_area() {
        if (!$this->_is_logged_in()) {
            redirect('signin');
        }
    }

    function _is_logged_in() {
        if ($this->session->userdata('logged_in')) {
            return true;
        } else {
            return false;
        }
    }

    function userdata() {
        if ($this->_is_logged_in()) {
            return $this->user_model->user_by_id($this->session->userdata('userid'));
        } else {
            return false;
        }
    }

    function _is_admin() {
        if (@$this->userdata()->role === 1) {
            return true;
        } else {
            return false;
        }
    }

}

?>