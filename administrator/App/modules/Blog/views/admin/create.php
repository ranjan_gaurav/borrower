<script src="<?php echo base_url('assets/plugins/ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/ckfinder/ckfinder.js'); ?>"></script>
<section style="padding:0px;">
    <div class="contain-lg">
        <!-- BEGIN BASIC VALIDATION -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-body form-inverse">
                    <form class="form form-validate floating-label" novalidate="novalidate" method="post">
                        <div class="card-head style-primary">
                            <div class="tools pull-left">
                                <header>Add New</header>
                            </div>
                            <div class="tools">
                                <a class="btn btn-flat hidden-xs" href="<?php echo base_url('admin/blog'); ?>"><span class="glyphicon glyphicon-arrow-left"></span> &nbsp;Back</a>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <?php if (@$error): ?>
                                    <div class="alert alert-callout alert-warning" role="alert">
                                        <strong>Warning!</strong> <?php echo $error; ?>
                                    </div>
                                <?php endif; ?>
                                <div class="form-group">
                                    <textarea name="title" id="textarea1" class="form-control" rows="3" required></textarea>
                                    <label for="title">Blog Title</label>
                                </div>

                                <div class="form-group">
                                    <textarea id="editor1" class="form-control control-5-rows" placeholder="Enter text ..." name="description">
                                    </textarea>
                                </div><!--end .card-body -->

                                <script>
                                    var editor = CKEDITOR.replace('editor1');
                                    editor.config.extraAllowedContent = 'div(*)';
                                    CKEDITOR.config.allowedContent = true;
                                    CKEDITOR.disableAutoInline = true;
                                    CKFinder.setupCKEditor(editor, '<?php echo base_url() ?>theme/plugins/ckfinder/');
                                </script>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="meta_title" name="meta_title" required data-rule-minlength="2">
                                    <label for="meta_title">Meta Title</label>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="meta_description" name="meta_description" required data-rule-minlength="2">
                                    <label for="meta_description">Meta Description</label>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="meta_keyword" name="meta_keyword" required data-rule-minlength="2">
                                    <label for="meta_keyword">Meta Keyword</label>
                                </div>
                            </div><!--end .card-body -->
                            <div class="card-actionbar">
                                <div class="card-actionbar-row">
                                    <button type="submit" class="btn ink-reaction btn-raised btn-primary btn-loading-state" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading...">Create<div style="top: 26px; left: 32.5px;" class="ink"></div></button>
                                </div>
                            </div><!--end .card-actionbar -->
                        </div><!--end .card -->
                    </form>
                </div>
            </div><!--end .col -->
        </div><!--end .row -->
        <!-- END BASIC VALIDATION -->

    </div><!--end .section-body -->
</section>