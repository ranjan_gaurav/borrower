<?php

/*

 */

class Blog_model extends CI_Model {

    var $table = "blogs";

    function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Kolkata');
    }

    function getList($showPerpage, $offset) {
        $this->db->limit($showPerpage, $offset);
        $this->db->order_by('id', 'DESC');
        $this->db->from($this->table);
        $result = $this->db->get();
        return $result->result();
    }

    function updateStatus() {
        if ($this->input->post('Delete') != null) {
            $arr_ids = $this->input->post('arr_ids');
            if (!empty($arr_ids)) {
                $this->db->where_in('id', $arr_ids);
                $this->db->delete($this->table);
            }
        } else {
            if ($this->input->post('Deactivate') != null) {
                $status = "0";
            } else if ($this->input->post('Activate') != null) {
                $status = "1";
            }
            $data = array(
                'status' => $status
            );
            $arr_ids = $this->input->post('arr_ids');
            if (!empty($arr_ids)) {
                $this->db->where_in('id', $arr_ids);
                $this->db->update($this->table, $data);
            }
        }
    }

    function count_all() {
        $query = $this->db->get($this->table);
        return $query->num_rows();
    }

    function delete($id) {
        $this->db->where('id', $id);
        $query = $this->db->delete($this->table);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function create() {
        $data = $this->input->post();
        $data['user_id'] = $this->session->userdata('userid');
        date_default_timezone_set('Asia/Kolkata');
        $date = date('Y-m-d H:i:s');
        $data['creation_date'] = $date;
        $data['modification_date'] = $date;
        $query = $this->db->insert($this->table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    function update($id) {
        $data = $this->input->post();
        $this->db->where('id', $id);
        date_default_timezone_set('Asia/Kolkata');
        $date = date('Y-m-d H:i:s');
        $data['modification_date'] = $date;
        $query = $this->db->update($this->table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    function get($id) {
        $this->db->where('id', $id);
        $this->db->from($this->table);
        $result = $this->db->get();
        return $result->row();
    }

}

?>