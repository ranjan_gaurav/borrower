<?php

/*

 */

class Blog extends Admin_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('blog_model', 'blog');
        $this->load->library("session");
        $this->load->library('pagination');
    }

    function index() {
        check_auth();
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        if ($this->input->post() != null) {
            $this->blog->updateStatus();
        }
        $config = array();
        $config["base_url"] = base_url() . "admin/blog/index";
        $config['total_rows'] = $this->blog->count_all();
        $config['per_page'] = (site_limit()) ? site_limit() : DEFAULT_LIMIT_PER_PAGE;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["lists"] = $this->blog->getList($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['main_content'] = 'list';
        $data['meta_title'] = "Blog List";
        $data['meta_keyword'] = "This is an meta keyword";
        $data['meta_description'] = "This is an meta description";
        $includeJs = array(
            'assets/js/libs/jquery/jquery-1.11.2.min.js',
            'assets/js/libs/DataTables/jquery.dataTables.min.js',
            'assets/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js',
            'assets/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js',
            'assets/js/libs/nanoscroller/jquery.nanoscroller.min.js',
            'assets/js/core/source/App.js',
             'assets/js/core/demo/DemoTableDynamic.js',
            );
        $includeCss = array(
            'assets/css/theme-default/libs/DataTables/jquery.dataTables.css?1423553989',
            'assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css?1423553990',
            'assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css?1423553990',
            );
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $this->setData($data);
    }

    function delete() {
        $id = $this->input->post('delID');
        if (isset($id) and $id != '') {
            $check = $this->blog->delete($id);
            if ($check) {
                $data['status'] = true;
                $data['message'] = "Deleted Successfully";
                echo json_encode($data);
            } else {
                $data['status'] = false;
                $data['message'] = "Something Went wrong";
                echo json_encode($data);
            }
        } else {
            $data['status'] = false;
            $data['message'] = "Something Went wrong";
            echo json_encode($data);
        }
    }

    public function create() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        $includeJs = array('assets/js/libs/jquery-validation/dist/jquery.validate.min.js',
            'assets/js/libs/jquery-validation/dist/additional-methods.min.js',
            'assets/js/core/source/App.js'
        );
        $includeCss = array('theme/plugins/jquery-validation/demo/css/screen.css');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
            $this->setData($data);
        } else {
            $this->blog->create();
            redirect(base_url('admin/blog'));
        }
    }

    public function edit($id) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        $includeJs = array('assets/js/libs/jquery-validation/dist/jquery.validate.min.js',
            'assets/js/libs/jquery-validation/dist/additional-methods.min.js',
            'assets/js/core/source/App.js'
        );
        $includeCss = array('theme/plugins/jquery-validation/demo/css/screen.css');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['content'] = $this->blog->get($id);
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
            $this->setData($data);
        } else {
            $this->blog->update($id);
            redirect(base_url('admin/blog'));
        }
    }

}

?>