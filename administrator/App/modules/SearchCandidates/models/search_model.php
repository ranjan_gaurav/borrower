<?php

/*
 *
 */
class search_model extends CI_Model {
	var $table = "users";
	var $jobs = "jobs";
	var $qualification = "qualifications";
	var $locations = "locations";
	var $usersFeilds = "user_fields";
	var $table_fields_values = "user_fields_values";
	var $original_path;
	var $resized_path;
	var $thumbs_path;
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'session' );
		$this->load->library ( 'pagination' );
	}
	function read($showPerpage, $offset) {
		$this->db->limit ( $showPerpage, $offset );
		$this->db->select ( 'qualifications.qualification_name,jobs.*' );
		$this->db->from ( $this->table );
		$this->db->join ( 'qualifications', 'qualifications.qualification_id = jobs.qualification_id', 'left' );
		$query = $this->db->get ();
		// echo $this->db->last_query();die;
		return $query->result ();
	}
	
	function selectjobs($showPerpage, $offset) {
	    $this->db->limit ( $showPerpage, $offset );
		$this->db->select ( 'qualifications.qualification_name,jobs.*' );
		$this->db->from ( $this->jobs );
		$this->db->join ( 'qualifications', 'qualifications.qualification_id = jobs.qualification_id', 'left' );
		$query = $this->db->get ();
		// echo $this->db->last_query();die;
		return $query->result ();
	}
	
	function usersFeilds($showPerpage, $offset) {
		$this->db->limit ( $showPerpage, $offset );
		$this->db->from ( $this->usersFeilds );
		$query = $this->db->get ();
		return $query->result ();
	}
	function count_all() {
		$query = $this->db->get ( $this->table );
		return $query->num_rows ();
	}
	function jobs_by_id($jobid) {
		$this->db->where ( 'id', $jobid );
		$query = $this->db->get ( $this->table );
		if ($query->num_rows () > 0) {
			return $query->row ();
		} else {
			return false;
		}
	}
	function job_update($id) {
		$data = array ();
		$data ['job_id'] = $this->input->post ( 'jobid' );
		$data ['job_title'] = $this->input->post ( 'title' );
		$data ['job_description'] = $this->input->post ( 'jobdescription' );
		$data ['no_of_vacancies'] = $this->input->post ( 'vacancies' );
		$data ['age_limit'] = $this->input->post ( 'age' );
		$data ['qualification_id'] = $this->input->post ( 'qualification' );
		$data ['experience'] = $this->input->post ( 'experience' );
		$data ['salary'] = $this->input->post ( 'salary' );
		$data ['job_location'] = $this->input->post ( 'location' );
		$data ['job_last_date'] = date ( 'Y-m-d', strtotime ( $this->input->post ( 'lastdate' ) ) );
		$data ['avaliability'] = $this->input->post ( 'notice' );
		
		$this->db->where ( 'id', $id );
		$query = $this->db->update ( $this->table, $data );
		if ($query) {
			return true;
		} else {
			return false;
		}
	}
	function search_candidates($showPerpage, $offset) {
		$user_city = $this->input->post ( 'locations', true ); //implode(',',$_POST['locations']);//$this->input->post ( 'location', true );
		$experience = $this->input->post ( 'experience', true );
		$qualification = implode(',', $_POST['qualification']);
	    $salary_expected = $this->input->post('salary',true);
	    $gender = $this->input->post('user_gender',true);
	    
	    if($user_city == '' && $experience == '' && $qualification =='' && $salary_expected == '' && $gender == '')
	    {
	    	//echo 123;
	    	//exit;
	        $this->db->limit ( $showPerpage, $offset );
	    	$this->db->select('users.*,qualifications.qualification_name as name,qualifications.qualification_id');
	    	$this->db->join('qualifications','users.qualification = qualifications.qualification_id','inner');
	    	//$this->db->join('locations','users.locations = locations.location_id','inner');
	    	$this->db->from ( $this->table );
	    	$query = $this->db->get();
	    	//echo $this->db->last_query();die;
	    }
	    else 
	    {
	    	
	    //print_r($user_city);
	   // exit;
	    if($user_city == ''){$user_city = 'null';}else {$user_city = $this->input->post ( 'locations', true );}if($experience == ''){$experience = 'null';}else {$experience = $this->input->post ( 'experience', true );}
	    if($qualification == ''){$qualification ='null';}else{$qualification = implode(',', $_POST['qualification']);}
	    if($salary_expected == ''){$salary_expected = 'null';}else{ $salary_expected = $this->input->post('salary',true);} 
	    if($gender == ''){$gender = 'null';}else{$gender = $this->input->post('user_gender',true);}
	    $this->db->limit ( $showPerpage, $offset );
	    $this->db->select('users.*,qualifications.qualification_name as name,qualifications.qualification_id');
	    $this->db->join('qualifications','users.qualification = qualifications.qualification_id','inner');
	  //  $this->db->join('locations','users.locations = locations.location_id','inner');
	    $this->db->where ("user_gender = '$gender' or (qualifications.qualification_id in($qualification))  or (locations in('$user_city'))
	    or (experience like concat('%' '$experience' '%')) or (salary_expected like concat('%' '$salary_expected' '%'))");
	    $this->db->from ( $this->table );
	    $query = $this->db->get();
	    }
	    //echo $this->db->last_query();die;
	    return $query->result ();
	   
	}
	function updateStatus() {
		if ($this->input->post ( 'Delete' ) != null) {
			$arr_ids = $this->input->post ( 'arr_ids' );
			if (! empty ( $arr_ids )) {
				$this->db->where_in ( 'id', $arr_ids );
				$this->db->delete ( $this->table );
			}
		} else {
			if ($this->input->post ( 'Deactivate' ) != null) {
				$status = "inactive";
			} else if ($this->input->post ( 'Activate' ) != null) {
				$status = "active";
			}
			$data = array (
					'status' => $status 
			);
			
			$arr_ids = $this->input->post ( 'arr_ids' );
			if (! empty ( $arr_ids )) {
				$this->db->where_in ( 'id', $arr_ids );
				$this->db->update ( $this->table, $data );
			}
		}
	}
	function get_qualifications() {
		$this->db->from ( $this->qualification );
		$result = $this->db->get ();
		return $result->result ();
	}
	
	
	function get_locations()
	{
		$this->db->from($this->locations);
		$result = $this->db->get ();
		return $result->result ();
		
		
	}
}

?>