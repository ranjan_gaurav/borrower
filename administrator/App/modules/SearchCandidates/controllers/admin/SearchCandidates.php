<?php

/*
 *
 */
class SearchCandidates extends Admin_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->model ( 'search_model' );
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'session' );
	}
	function index() {
		$this->load->view('create');
	}
	function editjob($id = null) {
		check_auth ();
		if (! $this->session->userdata ( 'logged_in' )) {
			redirect ( base_url ( 'signin' ) );
		}
		$includeJs = array (
				'assets/js/libs/jquery-validation/dist/jquery.validate.min.js',
				'assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js',
				'assets/js/core/demo/DemoFormComponents.js' 
		);
		$includeCss = array (
				'assets/css/theme-default/libs/bootstrap-datepicker/datepicker3.css' 
		);
		$data ['includes_for_layout_css'] = add_includes ( 'css', $includeCss );
		$data ['includes_for_layout_js'] = add_includes ( 'js', $includeJs, 'footer' );
		$data ['qualification'] = $this->search_model->get_qualifications ();
		$this->load->library ( 'form_validation' );
		$this->form_validation->set_rules ( 'title', 'title', 'trim|required' );
		$data ['job'] = $this->search_model->jobs_by_id ( $id );
		if ($this->form_validation->run () == false) {
			$data ['error'] = validation_errors ();
			$data ['main_content'] = 'edit';
		} else {
			$Update = $this->search_model->job_update ( $id );
			if ($Update) {
				$this->session->set_flashdata ( 'message', 'Jobs updated Successfully.' );
			} else {
				$this->session->set_flashdata ( 'error', 'Something Went Wrong' );
			}
			redirect ( 'admin/jobs' );
		}
		$this->setData ( $data );
	}
	function searchlist() {
		error_reporting ( 0 );
		
		if (isset ( $_POST ['submit'] )) {
			
			$data = $this->input->post ( 'arr_ids' );
			if ($data != '') {
				$this->session->set_userdata ( 'email', $data );
				
				redirect ( 'admin/SearchCandidates/selectJobs' );
			} 

			else {
				
				$this->session->set_flashdata ( 'error', 'Please select at least one Candidate' );
				$this->search ();
			}
		} 

		else {
			
			$this->search ();
		}
		
		// $this->setData ( $data );
	}
	function create() {
		
		check_auth ();
		if (! $this->session->userdata ( 'logged_in' )) {
			redirect ( base_url ( 'signin' ) );
		}
		$includeJs = array (
				'assets/js/libs/jquery-validation/dist/jquery.validate.min.js',
				'assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js',
				'assets/js/core/demo/DemoFormComponents.js' 
		);
		$includeCss = array (
				'assets/css/theme-default/libs/bootstrap-datepicker/datepicker3.css' 
		);
		$data ['includes_for_layout_css'] = add_includes ( 'css', $includeCss );
		$data ['includes_for_layout_js'] = add_includes ( 'js', $includeJs, 'footer' );
		$this->load->library ( 'form_validation' );
		$this->form_validation->set_rules ( 'location', 'location', 'trim|required' );
		$data ['qualification'] = $this->search_model->get_qualifications ();
		$data ['locations'] = $this->search_model->get_locations ();
		if ($this->form_validation->run () == false) {
			$data ['error'] = validation_errors ();
			$data ['main_content'] = 'create';
		} else {
		}
		$this->setData ( $data );
	}
	function search() {
		$config = array ();
		$config ['total_rows'] = $this->search_model->count_all ();
		$config ['per_page'] = (site_limit ()) ? site_limit () : DEFAULT_LIMIT_PER_PAGE;
		$this->pagination->initialize ( $config );
		$page = ($this->uri->segment ( 4 )) ? $this->uri->segment ( 4 ) : 0;
		// $data["jobs"] = $this->Job_model->read($config["per_page"], $page);
		$data ['result'] = $this->search_model->search_candidates ( $config ["per_page"], $page );
		$data ["links"] = $this->pagination->create_links ();
		$includeJs = array (
				'assets/js/libs/jquery/jquery-1.11.2.min.js',
				'assets/js/libs/DataTables/jquery.dataTables.min.js',
				'assets/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js',
				'assets/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js',
				'assets/js/libs/nanoscroller/jquery.nanoscroller.min.js',
				'assets/js/core/source/App.js',
				'assets/js/core/demo/DemoTableDynamic.js' 
		);
		$includeCss = array (
				'assets/css/theme-default/libs/DataTables/jquery.dataTables.css?1423553989',
				'assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css?1423553990',
				'assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css?1423553990' 
		);
		$data ['includes_for_layout_js'] = add_includes ( 'js', $includeJs, 'footer' );
		$data ['includes_for_layout_css'] = add_includes ( 'css', $includeCss );
		$this->load->library ( 'form_validation' );
		$this->form_validation->set_rules ( 'arr_ids', 'arr_ids', 'trim|required' );
		if ($this->form_validation->run () == false) {
			
			$data ['main_content'] = 'searchlist';
		} else {
			
			// $this->invite();
			$data ['main_content'] = 'searchlist';
		}
		
		$this->setData ( $data );
	}
	function selectJobs($data = '') {
		error_reporting ( 0 );
		if (isset ( $_POST ['submit2'] )) {
			
			if ($this->input->post ( 'arr_ids' )) {
				
				$email = $this->session->userdata ( 'email' );
				
				$this->load->library ( 'email' );
				foreach ( $email as $email => $address ) {
					$this->email->clear ();
					$this->email->to ( $address );
					$this->email->from ( 'your@example.com' );
					$this->email->subject ( 'Here is your info' );
					$this->email->message ( 'Hi Here is the info you requested.' );
					if ($this->email->send ()) {
						$this->session->set_flashdata ( 'message', 'Email has been sent successfully' );
					} 

					else {
						$this->session->set_flashdata ( 'error', 'Something Went Wrong' );
					}
				}
				$this->getJoblisting ();
			} 

			else {
				$this->session->set_flashdata ( 'error', 'Please select at least one Job' );
				$this->getJoblisting ();
			}
		} 

		else {
			
			$this->getJoblisting ();
		}
	}
	function getJoblisting() {
		if (! $this->session->userdata ( 'logged_in' )) {
			redirect ( base_url ( 'admin' ) );
		}
		
		$config = array ();
		$config ['total_rows'] = $this->search_model->count_all ();
		$config ['per_page'] = (site_limit ()) ? site_limit () : DEFAULT_LIMIT_PER_PAGE;
		$this->pagination->initialize ( $config );
		$page = ($this->uri->segment ( 4 )) ? $this->uri->segment ( 4 ) : 0;
		$data ["jobs"] = $this->search_model->selectjobs ( $config ["per_page"], $page );
		$data ["links"] = $this->pagination->create_links ();
		$includeJs = array (
				'assets/js/libs/jquery/jquery-1.11.2.min.js',
				'assets/js/libs/DataTables/jquery.dataTables.min.js',
				'assets/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js',
				'assets/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js',
				'assets/js/libs/nanoscroller/jquery.nanoscroller.min.js',
				'assets/js/core/source/App.js',
				'assets/js/core/demo/DemoTableDynamic.js' 
		);
		$includeCss = array (
				'assets/css/theme-default/libs/DataTables/jquery.dataTables.css?1423553989',
				'assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css?1423553990',
				'assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css?1423553990' 
		);
		$data ['includes_for_layout_js'] = add_includes ( 'js', $includeJs, 'footer' );
		$data ['includes_for_layout_css'] = add_includes ( 'css', $includeCss );
		$data ['main_content'] = 'selectjobs';
		$this->setData ( $data );
	}
	
	
	function getlocations()
	{
		
	 $searchTerm = $_GET['term'];
	 
	 
	 
    
    //get matched data from skills table
    $query = $db->query("SELECT * FROM locations WHERE location_name LIKE '%".$searchTerm."%' ORDER BY location_name ASC");
    while ($row = $query->fetch_assoc()) {
        $data[] = $row['location_name'];
    }
    
    //return json data
    echo json_encode($data);
		
		
		
		
	}
	
}

?>