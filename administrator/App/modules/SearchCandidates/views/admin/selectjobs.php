

<section class="style-default-bright">
    <div class="section-header">
        <div class="col-md-12">
            <div class="col-sm-6 col-md-6">
               <h2 class="text-primary">Select Jobs to Invite candidates.</h2>
            </div>
        </div><!--end .col -->
    </div>
    <?php if ($this->session->flashdata('message')) { ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('message'); ?>
        </div>
    <?php } ?>
    <?php if ($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php } ?>
    <div class="section-body">

        <!-- BEGIN DATATABLE 1 -->
        <div class="row">
            <div class="col-md-12">
                <div class="col-sm-6 col-md-12">
                    <h4>Jobs</h4>
                </div>
            </div><!--end .col -->
            <form method="post" name="form1" id="form1" action="">
                <div class="col-lg-12">
                    <div class="table-responsive">

                        <table id="datatable1" class="table table-striped table-hover">
                            <thead>
                                <tr>
                            <th><div class="checkbox checkbox-styled"><label><input name="check_all" type="checkbox" id="check_all" value="1" onclick="checkall(this.form)" ></label></div></th>
                            <th>Title</th>
                            <th>Location</th>
                            <th>Qualification</th>
                            <th>Experience</th>
                            <th>End Date</th>
                            <th>FJ Code</th>
                           
                         
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($jobs as $job): ?>
                                    <tr class="<?php echo ($job->status == 'active' ? 'gradeX' : 'gradeA'); ?>">
                                        <td>
                                            <div class="checkbox checkbox-styled"><label><input name="arr_ids[]" type="checkbox" id="arr_ids[]" value="<?php echo $job->id; ?>" ></label></div>
                                        </td>
                                        <td><a href="<?php echo base_url('admin/Jobs/details/' . $job->id); ?>"><?php echo $job->job_title ;?></a></td>
                                        <td><?php echo $job->job_location; ?></td>
                                        <td><?php echo $job->qualification_name; ?></td>
                                        <td><?php echo $job->experience; ?></td>
                                        <td><?php echo $job->job_last_date;?></td>
                                        <td><?php echo $job->ft_code;?></td>
                                        
                                       
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                    </div><!--end .table-responsive -->
                </div><!--end .col -->

                  <div class="col-sm-12 col-md-12">
                    <div class="col-sm-6 col-md-6">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="left" style="padding:2px">
                                  <button type="submit" id="submit2" name="submit2"
										class="btn ink-reaction btn-raised btn-primary btn-loading-state"
										data-loading-text="<i  class='fa fa-spinner fa-spin'>
										</i> Loading...">Email
										<div style="top: 26px; left: 32.5px;" class="ink"></div>
									</button>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-6 col-md-6 pull-right">
                        <?php echo $links; ?>
                    </div>
                </div>
            </form>

        </div><!--end .row -->
        <!-- END DATATABLE 1 -->
        <hr class="ruler-xxl"/>


    </div><!--end .section-body -->
</section>
