<?php

/*
 *
 */
class Job_model extends CI_Model {
	var $table = "jobs";
	var $qualification = "qualifications";
	var $usersFeilds = "user_fields";
	var $table_fields_values = "user_fields_values";
	var $original_path;
	var $resized_path;
	var $thumbs_path;
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'session' );
		$this->load->library ( 'pagination' );
	}
	function read($showPerpage, $offset) {
		$this->db->limit ( $showPerpage, $offset );
		$this->db->select ( 'qualifications.qualification_name,jobs.*' );
		$this->db->from ( $this->table );
		$this->db->join ( 'qualifications', 'qualifications.qualification_id = jobs.qualification_id', 'left' );
		$query = $this->db->get ();
		// echo $this->db->last_query();die;
		return $query->result ();
	}
	function usersFeilds($showPerpage, $offset) {
		$this->db->limit ( $showPerpage, $offset );
		$this->db->from ( $this->usersFeilds );
		$query = $this->db->get ();
		return $query->result ();
	}
	function count_all() {
		$query = $this->db->get ( $this->table );
		return $query->num_rows ();
	}
	function jobs_by_id($jobid) {
		$this->db->where ( 'id', $jobid );
		$query = $this->db->get ( $this->table );
		if ($query->num_rows () > 0) {
			return $query->row ();
		} else {
			return false;
		}
	}
	function job_update($id) {
		$data = array ();
		$data ['job_id'] = $this->input->post ( 'jobid' );
		$data ['job_title'] = $this->input->post ( 'title' );
		$data ['job_description'] = $this->input->post ( 'jobdescription' );
		$data ['no_of_vacancies'] = $this->input->post ( 'vacancies' );
		$data ['age_limit'] = $this->input->post ( 'age' );
		$data ['qualification_id'] = $this->input->post ( 'qualification' );
		$data ['experience'] = $this->input->post ( 'experience' );
		$data ['salary'] = $this->input->post ( 'salary' );
		$data ['job_location'] = $this->input->post ( 'location' );
		$data ['job_last_date'] = date ( 'Y-m-d', strtotime ( $this->input->post ( 'lastdate' ) ) );
		$data ['avaliability'] = $this->input->post ( 'notice' );
		
		$this->db->where ( 'id', $id );
		$query = $this->db->update ( $this->table, $data );
		if ($query) {
			return true;
		} else {
			return false;
		}
	}
	function create_job() {
		$data ['job_id'] = $this->input->post ( 'jobid', true );
		$data ['job_title'] = $this->input->post ( 'title', true );
		$data ['job_description'] = $this->input->post ( 'jobdescription', true );
		$data ['no_of_vacancies'] = $this->input->post ( 'vacancies', true );
		$data ['age_limit'] = $this->input->post ( 'age', true );
		$data ['qualification_id'] = $this->input->post ( 'qualification' );
		$data ['avaliability'] = $this->input->post('notice');
		$data ['ft_code'] = '#'. mt_rand ( 100000, 999999 );
		$data ['experience'] = $this->input->post ( 'experience' );
		$data ['salary'] = $this->input->post ( 'salary' );
		$data ['job_location'] = $this->input->post ( 'locations' );
		$data ['job_last_date'] = date ( 'Y-m-d', strtotime ( $this->input->post ( 'lastdate' ) ) );
		$data ['status'] = 'active';
		
		$this->db->set ( 'job_created_date', 'NOW()', false );
		$query = $this->db->insert ( $this->table, $data );
		if ($query) {
			$user_id = $this->db->insert_id ();
			// $user_value = $this->input->post('role_id') ? $this->input->post('role_id') : '3';
			// $this->update_role($user_id, $user_value);
			return true;
		} else {
			return false;
		}
	}
	function updateStatus() {
		if ($this->input->post ( 'Delete' ) != null) {
			$arr_ids = $this->input->post ( 'arr_ids' );
			if (! empty ( $arr_ids )) {
				$this->db->where_in ( 'id', $arr_ids );
				$this->db->delete ( $this->table );
			}
		} else {
			if ($this->input->post ( 'Deactivate' ) != null) {
				$status = "inactive";
			} else if ($this->input->post ( 'Activate' ) != null) {
				$status = "active";
			}
			$data = array (
					'status' => $status 
			);
			
			$arr_ids = $this->input->post ( 'arr_ids' );
			if (! empty ( $arr_ids )) {
				$this->db->where_in ( 'id', $arr_ids );
				$this->db->update ( $this->table, $data );
			}
		}
	}
	function get_qualifications() {
		$this->db->from ( $this->qualification );
		$result = $this->db->get ();
		return $result->result ();
	}
}

?>