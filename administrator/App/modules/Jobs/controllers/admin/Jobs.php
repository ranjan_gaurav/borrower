<?php

/*

 */

class Jobs extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Job_model');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    function index() {
    	
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('admin'));
        }
        if ($this->input->post() != null) {
            $this->Job_model->updateStatus();
        }
        $config = array();
        $config['total_rows'] = $this->Job_model->count_all();
        $config['per_page'] = (site_limit()) ? site_limit() : DEFAULT_LIMIT_PER_PAGE;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["jobs"] = $this->Job_model->read($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $includeJs = array(
            'assets/js/libs/jquery/jquery-1.11.2.min.js',
            'assets/js/libs/DataTables/jquery.dataTables.min.js',
            'assets/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js',
            'assets/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js',
            'assets/js/libs/nanoscroller/jquery.nanoscroller.min.js',
            'assets/js/core/source/App.js',
            'assets/js/core/demo/DemoTableDynamic.js',
        );
        $includeCss = array(
            'assets/css/theme-default/libs/DataTables/jquery.dataTables.css?1423553989',
            'assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css?1423553990',
            'assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css?1423553990',
        );
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['main_content'] = 'jobs';
        $this->setData($data);
    }

    function editjob($id = null) {
    	check_auth();
    	if (!$this->session->userdata('logged_in')) {
    		redirect(base_url('signin'));
    	}
    	$includeJs = array(
    			'assets/js/libs/jquery-validation/dist/jquery.validate.min.js',
    			'assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js',
    			'assets/js/core/demo/DemoFormComponents.js'
    	);
    	$includeCss = array('assets/css/theme-default/libs/bootstrap-datepicker/datepicker3.css');
    	$data['includes_for_layout_css'] = add_includes('css', $includeCss);
    	$data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
    	$data['qualification'] = $this->Job_model->get_qualifications();
    	$this->load->library('form_validation');
    	$this->form_validation->set_rules('title', 'title', 'trim|required');
    	$data['job'] = $this->Job_model->jobs_by_id($id);
    	if ($this->form_validation->run() == false) {
    		$data['error'] = validation_errors();
    		$data['main_content'] = 'edit';
    	} else {
    		$Update = $this->Job_model->job_update($id);
    		if ($Update) {
    			$this->session->set_flashdata('message', 'Jobs updated Successfully.');
    		} else {
    			$this->session->set_flashdata('error', 'Something Went Wrong');
    		}
    		redirect('admin/jobs');
    	}
    	$this->setData($data);
    }

    function details($jobid) {
        $data['job'] = $this->Job_model->jobs_by_id($jobid);
        if ($data["job"]) {

            $data['main_content'] = 'jobs';
            $this->setData($data);
        } else {
            show_404();
        }
    }

   
 




    function create() {
        check_auth();
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        $includeJs = array(
            'assets/js/libs/jquery-validation/dist/jquery.validate.min.js',
            'assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js',
            'assets/js/core/demo/DemoFormComponents.js'
        );
        $includeCss = array('assets/css/theme-default/libs/bootstrap-datepicker/datepicker3.css');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'title', 'trim|required');
        $data['qualification'] = $this->Job_model->get_qualifications();
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
            $data['main_content'] = 'create';
        } else {
            $insert = $this->Job_model->create_job();
            if ($insert) {
                $this->session->set_flashdata('message', 'Jobs Added Successfully.');
            } else {
                $this->session->set_flashdata('error', 'Something Went Wrong');
            }
            redirect('admin/jobs');
        }
        $this->setData($data);
    }

   

}

?>