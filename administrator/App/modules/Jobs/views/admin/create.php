<script
	src="<?php echo base_url('assets/plugins/ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript"
	src="<?php echo base_url('assets/plugins/ckfinder/ckfinder.js'); ?>"></script>
	<script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>
<section style="padding: 0px;">
	<div class="contain-lg">
		<!-- BEGIN BASIC VALIDATION -->
		<div class="row">
			<div class="col-md-12">
				<div class="card-body form-inverse">
					<form class="form form-validate floating-label"
						novalidate="novalidate" method="post">
						<div class="card-head style-primary">
							<div class="tools pull-left">
								<header>Add New job</header>
							</div>
							<div class="tools">
								<a class="btn btn-flat hidden-xs"
									href="<?php echo base_url('admin/Jobs'); ?>"><span
									class="glyphicon glyphicon-arrow-left"></span> &nbsp;Back</a>
							</div>
						</div>
						<div class="card">
							<div class="card-body">
                                <?php if (@$error): ?>
                                    <div
									class="alert alert-callout alert-warning" role="alert">
									<strong>Warning!</strong> <?php echo $error; ?>
                                    </div>
                                <?php endif; ?>
<!--                                 <div class="form-group"> -->
								<!--                                     <textarea name="title" id="textarea1" class="form-control" rows="3" required></textarea> -->
								<!--                                     <label for="title">Blog Title</label> -->
								<!--                                 </div> -->

								<!--                                 <div class="form-group"> -->
								<!--                                     <textarea id="editor1" class="form-control control-5-rows" placeholder="Enter text ..." name="description"> -->
								<!--                                     </textarea> -->

								<div class="form-group">
									<input type="text" class="form-control" id="jobid" name="jobid"
										required data-rule-minlength="2"> <label for="meta_title">JobID</label>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" id="title" name="title"
										required data-rule-minlength="2"> <label
										for="meta_description">Title of job</label>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" id="locations"
										name="locations" required data-rule-minlength="2"> <label
										for="meta_keyword">Location</label>
								</div>

								<div class="form-group">
									<input type="text" class="form-control" id="vacancies"
										name="vacancies" required> <label for="meta_keyword">Number of
										vacancies</label>
								</div>

								<div class="form-group">
									<input type="text" class="form-control" id="age" name="age"
										required data-rule-minlength="2"> <label for="meta_keyword">Age</label>
								</div>

								<div class="form-group">
									<input type="text" class="form-control" id="salary"
										name="salary" required data-rule-minlength="2"> <label
										for="meta_keyword">Salary</label>
								</div>

								<div class="form-group">
									<input type="text" class="form-control" id="notice"
										name="notice" required data-rule-minlength="2"> <label
										for="meta_keyword">Avalibility</label>
								</div>


								<div class="form-group">

									<select class="form-control" name="qualification" required
										id="qualification" required>
										<option value="">Select</option>
                            <?php foreach ($qualification as $val) { ?>
                                <option
											value="<?php echo $val->qualification_id; ?>"><?php echo $val->qualification_name; ?></option>
                            <?php } ?>
                                                        
                                                    </select> <label
										for="meta_keyword">Qualification</label>
								</div>

								<div class="form-group">
									<!--                                     <input type="text" class="form-control" id="experience" name="experience" required data-rule-minlength="2"> -->
									<!--                                     <label for="meta_keyword">Experience</label> -->


									<select class="form-control" name="experience" required
										id="experience" required>
										<option value="">Select</option>
										<option value="Fresher">Fresher</option>

									</select> <label for="experience">Experience</label>
								</div>

								<div class="form-group">

									<div class="input-group date" id="demo-date">
										<div class="input-group-content">
											<input type="text" class="form-control" name="lastdate"
												required> <label>Open till</label>
										</div>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>

								</div>

								<div class="form-group">
									<textarea name="jobdescription" id="jobdescription"
										class="form-control" rows="5" columns="4"></textarea>
									<label for="title">Job Description</label>
								</div>





							</div>
							<!--end .card-body -->
							<div class="card-actionbar">
								<div class="card-actionbar-row">
									<button type="submit" id="submit"
										class="btn ink-reaction btn-raised btn-primary btn-loading-state"
										data-loading-text="<i  class='fa fa-spinner fa-spin'>
										</i> Loading...">Create New Job
										<div style="top: 26px; left: 32.5px;" class="ink"></div>
									</button>
								</div>
							</div>
							<!--end .card-actionbar -->
						</div>
						<!--end .card -->
					</form>
				</div>
			</div>
			<!--end .col -->
		</div>
		<!--end .row -->
		<!-- END BASIC VALIDATION -->

	</div>
	<!--end .section-body -->
</section>

<script type="text/javascript">
function initialize() {
    var input = document.getElementById('locations');
    var options = {componentRestrictions: {country: 'in'}};
                 
    new google.maps.places.Autocomplete(input, options);
}
             
google.maps.event.addDomListener(window, 'load', initialize);
</script>


