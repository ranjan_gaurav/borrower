<section>
    <div class="section-body">
        <div class="card">

            <!-- BEGIN CONTACT DETAILS HEADER -->
            <div class="card-head style-primary">
                <div class="tools pull-left">
                    <form class="navbar-search" role="search">
                        <div class="form-group">
                            <input type="text" class="form-control" name="contactSearch" placeholder="Enter your keyword">
                        </div>
                        <button type="submit" class="btn btn-icon-toggle ink-reaction"><i class="fa fa-search"></i></button>
                    </form>
                </div><!--end .tools -->
                <div class="tools">
                    <a class="btn btn-flat hidden-xs" href="<?php echo base_url('admin/home'); ?>"><span class="glyphicon glyphicon-arrow-left"></span> &nbsp;Dashboard</a>
                    <a class="btn btn-flat hidden-xs" href="<?php echo base_url('admin/jobs/editjob/' . $job->id); ?>"><i class="fa fa-edit"></i></span> &nbsp;Edit</a>
                </div><!--end .tools -->
            </div><!--end .card-head -->
            <!-- END CONTACT DETAILS HEADER -->

            <!-- BEGIN CONTACT DETAILS -->
            <div class="card-tiles">
                <div class="hbox-md col-md-12">
                    <div class="hbox-column col-md-9">
                        <div class="row">


                            <!-- END CONTACTS NAV -->

                            <!-- BEGIN CONTACTS MAIN CONTENT -->
                            <div class="col-sm-7 col-md-8 col-lg-9">
                               
                                <ul class="nav nav-tabs" data-toggle="tabs">
                                    <li class="active"><a href="#details">DETAILS</a></li>
                                    <li><a href="#notes">INTERVIEW ATTEMPTED</a></li>
                                </ul>
                                <div class="tab-content">

                                    <!-- BEGIN CONTACTS NOTES -->
                                    <div class="tab-pane" id="notes">

                                        <ul class="timeline collapse-lg timeline-hairline no-shadow">
                                            <li class="timeline-inverted">
                                                <div class="timeline-circ style-accent"></div>
                                                <div class="timeline-entry">
                                                    <div class="card style-default-bright">
                                                        <div class="card-body small-padding">
                                                           <table id="datatable1" class="table table-striped table-hover">
                            <thead>
                                <tr>
                            <th><div class="checkbox checkbox-styled"><label><input name="check_all" type="checkbox" id="check_all" value="1" onclick="checkall(this.form)" ></label></div></th>
                            <th>Name</th>
                            <th>ID</th>
                            <th>Assesment Score</th>
                            <th>L1 Status</th>
                            <th>L2 Status</th>
                            <th>View interview</th>
                            <th>comments</th>
                            </tr>
                            </thead>
                            <tbody>
                             
                            </tbody>
                        </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul><!--end .timeline -->
                                    </div><!--end #notes -->
                                    <!-- END CONTACTS NOTES -->

                                    <!-- END CONTACTS ACTIVITY -->

                                    <!-- BEGIN CONTACTS DETAILS -->
                                    <div class="tab-pane active" id="details">
                                        <h3 class="opacity-50">Summary</h3>
                                        <br/>
                                        <dl class="dl-horizontal dl-icon">
                                            <dt><span class="fa fa-fw fa-mobile fa-lg opacity-50"></span></dt>
                                            <dd>
                                                <span class="opacity-50">Experience</span><br/>
                                                <span class="text-medium"><?php echo $job->experience; ?></span> &nbsp;<span class="opacity-50"></span><br/>
                                            </dd>
                                            <dt><span class="fa fa-fw fa-birthday-cake fa-lg opacity-50"></span></dt>
                                            <dd>
                                                <span class="opacity-50">Location</span><br/>
                                                <span class="text-medium"><?php echo $job->job_location; ?></span> <br/>
                                            </dd>
                                            <dt><span class="fa fa-fw fa-envelope-square fa-lg opacity-50"></span></dt>
                                            <dd>
                                                <span class="opacity-50">No of vacancies</span><br/>
                                                <a class="text-medium" href="mailto:<?php echo $job->no_of_vacancies; ?>"><?php echo $job->no_of_vacancies; ?></a>
                                            </dd>
                                            <dt><span class="fa fa-fw fa-location-arrow fa-lg opacity-50"></span></dt>
                                            <dd>
                                                <span class="opacity-50">End date</span><br/>
                                                <span class="text-medium">
                                                    <?php echo $job->job_last_date; ?>
                                                </span>
                                            </dd>
                                            <dt><span class="fa fa-fw fa-calendar fa-lg opacity-50"></span></dt>
                                            <dd>
                                                <span class="opacity-50">Salary</span><br/>
                                                <span class="text-medium">
                                                    <?php echo $job->salary; ?>
                                                </span>
                                            </dd>
                                             <dt><span class="fa fa-fw fa-calendar fa-lg opacity-50"></span></dt>
                                             <dd>
                                                <span class="opacity-50">Job description</span><br/>
                                                <span class="text-medium">
                                                    <?php echo $job->job_description; ?>
                                                </span>
                                            </dd>
                                            <dd class="full-width"><div id="map-canvas" class="border-white border-xl height-5"></div></dd>
                                        </dl><!--end .dl-horizontal -->
                                    </div><!--end #details -->
                                    <!-- END CONTACTS DETAILS -->

                                </div><!--end .tab-content -->
                            </div><!--end .col -->
                            <!-- END CONTACTS MAIN CONTENT -->

                        </div><!--end .row -->
                    </div><!--end .hbox-column -->

                    <!-- BEGIN CONTACTS COMMON DETAILS -->
                    <!-- END CONTACTS COMMON DETAILS -->

                </div><!--end .hbox-md -->
            </div><!--end .card-tiles -->
            <!-- END CONTACT DETAILS -->

        </div><!--end .card -->
    </div><!--end .section-body -->
</section>
