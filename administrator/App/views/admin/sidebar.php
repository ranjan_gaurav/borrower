<div id="menubar" class="menubar-inverse ">
    <div class="menubar-fixed-panel">
        <div>
            <a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                <i class="fa fa-bars"></i>
            </a>
        </div>
        <div class="expanded">
            <a href="#">
                <span class="text-lg text-bold text-primary ">MATERIAL&nbsp;ADMIN</span>
            </a>
        </div>
    </div>
    <div class="menubar-scroll-panel">

        <!-- BEGIN MAIN MENU -->
        <ul id="main-menu" class="gui-controls">

            <!-- BEGIN DASHBOARD -->
            <li>
                <a href="<?php echo base_url('admin/home'); ?>" class="<?php echo getActiveMenu('home'); ?>">
                    <div class="gui-icon"><i class="md md-home"></i></div>
                    <span class="title">Dashboard </span>
                </a>
            </li><!--end /menu-li -->
            <!-- END DASHBOARD -->

            <!-- BEGIN EMAIL -->
            <li class="gui-folder">
                <a href="javascript:void(0);" class="<?php echo getActiveMenu('users'); ?>">
                    <div class="gui-icon"><i class="fa fa-users"></i></div>
                    <span class="title">Users</span>
                </a>
                <!--start submenu -->
                <ul>
                    <li><a href="<?php echo base_url('admin/users'); ?>" ><span class="title">User List</span></a></li>
                    <li><a href="<?php echo base_url('admin/users/create'); ?>" ><span class="title">Add New User</span></a></li>
                </ul><!--end /submenu -->
            </li><!--end /menu-li -->
            <!-- END EMAIL -->
            <li class="gui-folder">
                <a href="javascript:void(0);" class="<?php echo getActiveMenu('blog'); ?>">
                    <div class="gui-icon"><i class="fa fa-rss"></i></div>
                    <span class="title">Privacy policy</span>
                </a>
                <!--start submenu -->
                <ul>
                    <li><a href="<?php echo base_url('admin/users/privacy_policy'); ?>" ><span class="title">Privacy policy</span></a></li>
                    <li><a href="<?php echo base_url('admin/users/term_of_use'); ?>" ><span class="title">Terms & Condition</span></a></li>
                   
                </ul><!--end /submenu -->
            </li><!--end /menu-li -->

            
            
            
             <li class="gui-folder">
                <a href="javascript:void(0);" class="<?php echo getActiveMenu('ManageInterviews'); ?>">
                    <div class="gui-icon"><i class="fa fa-rss"></i></div>
                    <span class="title">Loan Status</span>
                </a>
                <!--start submenu -->
                <ul>
                     <li><a href="<?php echo base_url('admin/LoanStatus/'); ?>" ><span class="title">loan status list</span></a></li>
                     <li><a href="<?php echo base_url('admin/LoanStatus/create/'); ?>" ><span class="title">Add new loan status</span></a></li>
                    
                    
                   
                </ul><!--end /submenu -->
            </li>
            
            
            <!-- END LEVELS -->

        </ul><!--end .main-menu -->
        <!-- END MAIN MENU -->

        <div class="menubar-foot-panel">
            <small class="no-linebreak hidden-folded">
                <span class="opacity-75">Copyright &copy; 2016</span> <a href="http://orangemantra.com/" target="_blank"><strong>Orangemanta</strong></a>
            </small>
        </div>
    </div><!--end .menubar-scroll-panel-->
</div><!--end #menubar-->